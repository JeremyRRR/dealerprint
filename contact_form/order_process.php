<?php
error_reporting(0);
include("../config/db.php");
include("../config/config.php");
include("../config/keys.php");
include_once "../swift/lib/swift_required.php";
//require_once('../app/assets/Shippo.php');


$post = (!empty($_POST)) ? true : false;
if ($post) {
    include 'email_validation.php';
    $dealer_name = htmlspecialchars($_POST['dealer_name'], ENT_QUOTES);
    $dealer_logo = $_POST['dealer_logo'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $zip = $_POST['zip'];
    $country = $_POST['country'];
    $name = $_POST['name'];
    $name_array = explode(' ', trim($name));
    //$name_array2 = explode(' ', trim($name));
    $first_name = $name_array[0];
    $last_name = $name_array[1];
    $email = trim($_POST['email']);
    $user_phone = trim($_POST['user_phone']);
    $username = $_POST['username'];
    $password = $_POST['password'];
    $subject = " Welcome " . $dealer_name . " to " . DOMAIN;
    $owner = $_POST['owner'];
    $dealer_group = $_POST['dealer_group'];
    $dealer_phone = $_POST['dealer_phone'];
    $subscription = "Trial";
    $referred_by = $_POST['owner'];
    $error = '';

    // Check name
    if (!$dealer_name) {
        $error .= 'Please enter the dealership name.<br />';
    }
    // Check address
    if (!$address) {
        $error .= 'Please enter the dealership street address.<br />';
    }
    // Check city
    if (!$city) {
        $error .= 'Please enter the dealership city.<br />';
    }
    // Check state
    if (!$state) {
        $error .= 'Please enter the dealership state.<br />';
    }
    // Check zip
    if (!$zip) {
        $error .= 'Please enter the dealership zip code.<br />';
    }
    // Check name
    if (!$name) {
        $error .= 'Please enter your name.<br />';
    }
    // Check email

    if (!$email) {
        $error .= 'Please enter your e-mail address.<br />';
    }
    // Check phone
    if (!$dealer_phone) {
        $error .= 'Please enter your dealerships phone number.<br />';
    }
    // Check phone
    if (!$user_phone) {
        $error .= 'Please enter your personal phone number.<br />';
    }

    if ($email && !ValidateEmail($email)) {
        $error .= 'Please enter a valid e-mail address.<br />';
    }
    // Check username
    if (!$username) {
        $error .= 'Please enter a username of your choice.<br />';
    }
    // Check for unique username
    $uquery = mysqli_query($db, "SELECT * FROM users WHERE USERNAME = '$username'");
    $rows = mysqli_num_rows($uquery);
    if ($rows >= 1) {
        $error .= 'Please enter a unique username.<br />';
    }

    // Check password (length)
    if (!$password || strlen($password) < 8) {
        $error .= "Please enter a password. It should have at least 8 characters.<br />";
    }


    if (!$error) {
        // If no form errors insert user into database here

        //create dealer user

        $uid = time();
        $date = date("Y-m-d");
        mysqli_query($db, "INSERT INTO users (
				`USER_ID` ,
				`USER_TYPE` ,
				`NAME` ,
				`DEALER_ID` ,
				`EMAIL` ,
				`USER_PHONE` ,
				`USERNAME` ,
				`PASSWORD` ,
				`CREATOR_ID` ,
				`CREATE_DATE`,
				`NEW_USED_BOTH`
			)
			VALUES (
			'" . $uid . "', 'DealerAdmin', '" . $name . "', '" . $uid . "', '" . $email . "', '" . $user_phone . "','" . $username . "', '" . $password . "', '" . $owner . "', '" . $date . "', 'Both'
			);");
        mysqli_query($db, "INSERT INTO `infosheet` (`INFO_ID`, `DEALER_ID`, `USER_ID`, `INFO_TYPE`, `INFO_STYLE`) VALUES (NULL, '" . $uid . "', '" . $uid . "', 'standard', 'KBB');");
        mysqli_query($db, "INSERT INTO `infosheet` (`INFO_ID`, `DEALER_ID`, `USER_ID`, `INFO_TYPE`, `INFO_STYLE`) VALUES (NULL, '" . $uid . "', '" . $uid . "', 'certified', 'KBB');");
        // use $dealerlogo for MAKE1 by removing .jpg and saving in new variable
        $make1 = ucfirst(preg_replace("/\.jpg$/i", "", $dealer_logo));

        $address1 = $address . ", " . $city . ", " . $state . " " . $zip . ", USA";
        //$address = $row['DEALER_ADDRESS']+", "+$row['DEALER_CITY']+", "+$row['DEALER_STATE']+" "+$row['DEALER_ZIP']+", USA"; // Google HQ
        $prepAddr = str_replace(' ', '+', $address1);
        $prepAddr = str_replace('Road', '# ', $prepAddr);
        $prepAddr = str_replace('Road ', '#', $prepAddr);
        $geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address=' . $prepAddr . '&sensor=false');
        $output = json_decode($geocode);
        $latitude = $output->results[0]->geometry->location->lat;
        $longitude = $output->results[0]->geometry->location->lng;

        //create dealer
        mysqli_query($db, "INSERT INTO dealer_dim (
				`ID` ,
				`Active` ,
				`DEALER_ID` ,
				`DEALER_NAME` ,
				`PRIMARY_CONTACT` ,
				`PRIMARY_CONTACT_EMAIL` ,
				`DEALER_LOGO` ,
				`DEALER_ADDRESS` ,
				`DEALER_CITY` ,
				`DEALER_STATE` ,
				`DEALER_ZIP` ,
				`DEALER_COUNTRY` ,
				`DEALERGROUP` ,
				`PHONE` ,
				`BILLING_STREET` ,
				`BILLING_CITY` ,
				`BILLING_STATE` ,
				`BILLING_ZIP` ,
				`BILLING_COUNTRY` ,
				`OWNER` ,
				`REFERRED_BY` ,
				`MAKE1` ,
				`ACCOUNT_TYPE` ,
				`ADDENDUM_STYLE`, 
				`LAT1` ,
				`LNG1`
				
			)
			VALUES (
			'" . $uid . "', 'Yes', '" . $uid . "', '" . $dealer_name . "', '" . $name . "', '" . $email . "', '" . $dealer_logo . "', '" . $address . "', '" . $city . "', '" . $state . "', '" . $zip . "', '" . $country . "', '" . $dealer_group . "', '" . $dealer_phone . "','" . $address . "', '" . $city . "', '" . $state . "', '" . $zip . "', '" . $country . "', '" . $owner . "', '" . $referred_by . "', '" . $make1 . "', 'Free', 'EPA Template', '" . $latitude . "', '" . $longitude . "');");


        mysqli_query($db, "
				INSERT INTO dealer_inventory(
					`id` ,
					`DEALER_ID` ,
					`VIN_NUMBER` ,
					`STOCK_NUMBER` ,
					`YEAR` ,
					`MAKE` ,
					`MODEL` ,
					`BODYSTYLE` ,
					`DOORS` ,
					`TRIM` ,
					`EXT_COLOR` ,
					`INT_COLOR` ,
					`ENGINE` ,
					`FUEL` ,
					`DRIVETRAIN` ,
					`TRANSMISSION` ,
					`MILEAGE` ,
					`DATE_IN_STOCK` ,
					`STATUS` ,
					`PRINT_STATUS` ,
					`MSRP`,
					`INPUT_DATE`,
					`NEW_USED`,
					`DESCRIPTION`,
					`OPTIONS`,
					`HMPG`,
					`CMPG`,
					`CREATED_BY`,
					`UPDATE_DATE`
					)
					VALUES (
						'', '" . $uid . "', '2HGFC3B96HH362096', 'STOCK_TEST1', '2017', 'Honda', 'Civic', 'Coupe', '2 door', 'Touring', 'White', 'BLACK&IVORY', 'I-4 cyl', 'Regular Unleaded' , 'Front-wheel Drive', 'continuously variable automatic', '10', '" . date("Y-m-d") . "', '1', '0', '27100','" . date("Y-m-d") . "', 'New', 'Description', 'Air Filtration,Airbag Occupancy Sensor,Back-Up Camera,Body-Colored Front Bumper,Body-Colored Power Heated Side Mirrors','100','200', 'VIN API','" . date("Y-m-d") . "');");

        mysqli_query($db, "
				INSERT INTO dealer_inventory(
					`id` ,
					`DEALER_ID` ,
					`VIN_NUMBER` ,
					`STOCK_NUMBER` ,
					`YEAR` ,
					`MAKE` ,
					`MODEL` ,
					`BODYSTYLE` ,
					`DOORS` ,
					`TRIM` ,
					`EXT_COLOR` ,
					`INT_COLOR` ,
					`ENGINE` ,
					`FUEL` ,
					`DRIVETRAIN` ,
					`TRANSMISSION` ,
					`MILEAGE` ,
					`DATE_IN_STOCK` ,
					`STATUS` ,
					`PRINT_STATUS` ,
					`MSRP`,
					`INPUT_DATE`,
					`NEW_USED`,
					`DESCRIPTION`,
					`OPTIONS`,
					`HMPG`,
					`CMPG`,
					`CREATED_BY`,
					`UPDATE_DATE`
					)
					VALUES (
						'', '" . $uid . "', '2HGFC2F50GH562035', 'STOCK_TEST2', '2016', 'Honda', 'Civic', 'Sedan', '2 door', 'Touring', 'White', 'BLACK&IVORY', 'I-4 cyl', 'Regular Unleaded' , 'Front-wheel Drive', 'continuously variable automatic', '10', '" . date("Y-m-d") . "', '1', '0', '20275','" . date("Y-m-d") . "', 'Used', 'Description', 'All pricing includes $790.00 for Honda cars and $830 for Honda Trucks destination/delivery charge. Pricing does not include any add on accessories','10000','20000', 'VIN API','" . date("Y-m-d") . "');");
        mysqli_query($db, "INSERT INTO addendum_defaults(
					`AD_ID` ,
					`DEALER_ID` ,
					`ITEM_NAME` ,
					`ITEM_DESCRIPTION` ,
					`PRICE` ,
					`MODEL_1` ,
					`MODEL_2` ,
					`MODEL_3` ,
					`MODEL_4` ,
					`BODY_STYLE_1` ,
					`BODY_STYLE_2` ,
					`BODY_STYLE_3` ,
					`BODY_STYLE_4` ,
					`BODY_STYLE_5` ,
					`BODY_STYLE_6` ,
					`BODY_STYLE_7` ,
					`BODY_STYLE_8` ,
					`BODY_STYLE_9` ,
					`BODY_STYLE_10` ,
					`AD_TYPE`,
					`MODEL_5` ,
					`MODEL_6` ,
					`MODEL_7` ,
					`MODEL_8` ,
					`SEPARATOR_BELOW` ,
					`SEPARATOR_ABOVE`,
					`OG_OR_AD`
					)
				VALUES (
					'' , '" . $uid . "', 'Performance Package', 'Nitrogen Tire Fill<br />Clear Bra<br />Wheel Locks', '499', 'ALL', '', '', '', 'ALL', '', '', '', '', '', '', '', '', '', 'Both','','','','', '0', '0', '1');");
        mysqli_query($db, "INSERT INTO addendum_defaults(
					`AD_ID` ,
					`DEALER_ID` ,
					`ITEM_NAME` ,
					`ITEM_DESCRIPTION` ,
					`PRICE` ,
					`MODEL_1` ,
					`MODEL_2` ,
					`MODEL_3` ,
					`MODEL_4` ,
					`BODY_STYLE_1` ,
					`BODY_STYLE_2` ,
					`BODY_STYLE_3` ,
					`BODY_STYLE_4` ,
					`BODY_STYLE_5` ,
					`BODY_STYLE_6` ,
					`BODY_STYLE_7` ,
					`BODY_STYLE_8` ,
					`BODY_STYLE_9` ,
					`BODY_STYLE_10` ,
					`AD_TYPE`,
					`MODEL_5` ,
					`MODEL_6` ,
					`MODEL_7` ,
					`MODEL_8` ,
					`SEPARATOR_BELOW` ,
					`SEPARATOR_ABOVE`,
					`OG_OR_AD`
					)
				VALUES (
					'' , '" . $uid . "', 'Window Tint', '3M Scotchguard window tint with lifetime scratch/peel/fade warranty', '299', 'ALL', '', '', '', 'ALL', '', '', '', '', '', '', '', '', '', 'Both','','','','', '0', '0', '1');");


        mysqli_query($db, "update dealer_dim set vehicles_new=1, need_new=1, plus_today_n=1 where ID='" . $uid . "'");
        mysqli_query($db, "update dealer_dim set vehicles_used=1, need_used=1, plus_today_u=1 where ID='" . $uid . "'");


        // if no insertian errors then Send Mail
        $from = array(SYSTEM_EMAIL => DOMAIN);
        $to = array(ADMIN_EMAIL => SITE_AUTHOR,
            $email => $name);

        $text = "Hello " . $first_name . ",\n
			Thanks for signing up with" . DOMAIN . " where creating, printing and managing addendums is now easier than ever! We are thrilled to have you onboard. Your new 30 day trial account has been created and is ready to go.\n
			Here are the login details:\n\n  Login Here: http://dealeraddendums.com/app  \nUsername: " . $username . " \nPassword: " . $password . "\n\n
			You have signed up for our Free Account:  \nThis is a full featured 30 day free account that allows you to manually enter as many vehicles as you want, and to print up to 25 addendums. Once that limit is reached you'll have to upgrade your account to continue using the system.\n\n
			Upgrade Options:  We have three types of upgrade options - Manual and Automatic WEB and Automatic DMS.\n 
			Automatic accounts automatically populate your dealer inventory every night directly from your WEBsite provider or directly from your DMS.\n
			Manual accounts allow you to enter vehicles one at a time or by importing your inventory with an Excel spreadsheet.\n\n
			Addendum Labels:  Just for signing up and giving our service a try we'll send you 25 free printer labels. When you run out, just place an order for more. We'll send them to you Priority Mail and shipping is always free! \n
			We hope you enjoy our service! If you have any questions, comments, or suggestions, please let us know.\n\n
			" . SITE_AUTHOR . " \n" . COMPANY_NAME . " \n" . SUPPORT_PHONE . " ";

        $html = "<strong>Hello " . $first_name . ",</strong><br>
			Thanks for signing up with " . DOMAIN . " where creating, printing and managing addendums is now easier than ever! We are thrilled to have you onboard. Your new 30 day trial account has been created and is ready to go.  <br><br>
			<strong>Here are the login details:</strong><br>Login Here: " . SITE_URL . "<br>Username: " . $username . "<br>Password: " . $password . "<br><br>
			<strong>Free Account:</strong><br>You have signed up for our Free Account. This is a full featured 30 day free account that allows you to manually enter as many vehicles as you want, and to print up to 25 addendums. Once that limit is reached you'll have to upgrade your account to continue using the system.<br><br>
			<strong>Upgrade Options:</strong><br>We have three types of upgrade accounts -  Manual Load, Automatic WEB and Automatic DMS. 
				<ul>
					<li><strong>Automatic DMS</strong> ($175/Mo) accounts automatically populate your dealer inventory every night, or hourly for CDK, directly from your DMS. (Reynolds, Autosoft or CDK)</li>
					<li><strong>Automatic Web</strong> ($125/Mo) accounts automatically populate your dealer inventory every night directly from your WEBsite provider or inventory syndication company. (Dealer.com, VIN Solutions, Homenet, etc.)</li>
					<li><strong>Manual Load</strong> ($75/Mo) accounts allow you to enter vehicles one at a time using our VIN decoder or by importing your inventory in bulk from an Excel spreadsheet.</li>
				</ul>
			
			<strong>Training:</strong><br>Click here to <a href='" . WEBSITE . "tutorial' target='_blank'>watch a tutorial</a> video on how to set up and use the system...it's easy!<br><br>
			<strong>Addendum Labels:</strong><br>Just for signing up and giving our service a try we'll send you 25 free printer labels. When you run out, just place an order for more. We'll send them to you Priority Mail and shipping is always free!<br><br>

			We hope you enjoy our service! If you have any questions, comments, or suggestions, please let us know.<br><br>
			<strong><em>" . SITE_AUTHOR . "</em></strong><br>" . COMPANY_NAME . "<br>" . SUPPORT_PHONE . "";

        $transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 587);
        $transport->setUsername($row['MANDRILL_USERNAME']);
        $transport->setPassword($row['MANDRILL_PASSWORD']);
        $swift = Swift_Mailer::newInstance($transport);

        $message = new Swift_Message($subject);
        $message->setFrom($from);
        $message->setBody($html, 'text/html');
        $message->setTo($to);
        $message->addPart($text, 'text/plain');

        if ($recipients = $swift->send($message, $failures)) {
            echo 'OK';
            $crmaddress = $address . ", " . $city . ", " . $state . " - " . $zip;
          /*  include("../app/assets/crm/CurlLib/curlwrap_v2.php");
            $address2 = array(
                "address"=>$address,
                "city"=>$city,
                "state"=>$state,
                "zip"=>$zip,
                "country"=>$country
            );
            $company_json = array(
                "type" => "COMPANY",
                "properties" => array(
                    array(
                        "name" => "name",
                        "value" => $dealer_name,
                        "type" => "SYSTEM"
                    ),
                    array(
                        "name" => "Dealer Group",
                        "value" => $dealer_group,
                        "type" => "CUSTOM"
                    ),
                    array(
                        "name" => "Phone Number",
                        "value" => $user_phone,
                        "type" => "CUSTOM"
                    ),
                    array(
                        "name" => "Referred By",
                        "value" => $referred_by,
                        "type" => "CUSTOM"
                    ),
                    array(
                        "name" => "Account Type",
                        "value" => "Free",
                        "type" => "CUSTOM"
                    ),
                    array(
                        "name" => "Brands",
                        "value" => $dealer_logo,
                        "type" => "CUSTOM"
                    ),
                    array(
                        "name"=>"address",
                        "value"=>json_encode($address2),
                        "type"=>"SYSTEM"
                    ),
                    array(
                        "name" => "DealerID",
                        "value" => $uid,
                        "type" => "CUSTOM"
                    )
                )
            );

            $company_json = json_encode($company_json);
            $result1=curl_wrap("contacts", $company_json, "POST", "application/json");
			$result1 = json_decode($result1, false, 512, JSON_BIGINT_AS_STRING);
			$company_id = $result1->id;
			 mysqli_query($db, "update dealer_dim set CRM_ID=".$company_id." where ID='".$uid."'");
*/


            $contact_email = $email;
          /*  $contact_json = array(
                "lead_score"=>"0",
                "star_value"=>"0",
                "tags"=>array($owner),
                "properties"=>array(
                    array(
                        "name"=>"first_name",
                        "value"=>$first_name,
                        "type"=>"SYSTEM"
                    ),
                    array(
                        "name"=>"last_name",
                        "value"=>$last_name,
                        "type"=>"SYSTEM"
                    ),
                    array(
                        "name"=>"email",
                        "value"=>$email,
                        "type"=>"SYSTEM"
                    ),
                    array(
                        "name"=>"company",
                        "value"=>$dealer_name,
                        "type"=>"SYSTEM"
                    ),
                    array(
                        "name"=>"phone",
                        "value"=>$user_phone,
                        "type"=>"SYSTEM"
                    ),
                    array(
                        "name"=>"Username",  //This is custom field which you should first define in custom field region.
                        //Example - created custom field : http://snag.gy/kLeQ0.jpg
                        "value"=>$username,
                        "type"=>"CUSTOM"
                    ),
                    array(
                        "name"=>"Password",  //This is custom field which you should first define in custom field region.
                        //Example - created custom field : http://snag.gy/kLeQ0.jpg
                        "value"=>$password,
                        "type"=>"CUSTOM"
                    ),
                    array(
                        "name"=>"User Type",  //This is custom field which you should first define in custom field region.
                        //Example - created custom field : http://snag.gy/kLeQ0.jpg
                        "value"=>"DealerAdmin",
                        "type"=>"CUSTOM"
                    ),
                    array(
                        "name"=>"Trial",  //This is custom field which you should first define in custom field region.
                        //Example - created custom field : http://snag.gy/kLeQ0.jpg
                        "value"=>"Yes",
                        "type"=>"CUSTOM"
                    ),
                    array(
                        "name"=>"address",
                        "value"=>json_encode($address2),
                        "type"=>"SYSTEM"
                    )

                )
            );

            $contact_json = json_encode($contact_json);
            $result=curl_wrap("contacts", $contact_json, "POST", "application/json");
			$result = json_decode($result, false, 512, JSON_BIGINT_AS_STRING);
			$contact_id = $result->id;
			mysqli_query($db, "update users set CRM_ID=".$contact_id." where USER_ID='".$uid."'");


                 // send new customer to Capsul CRM
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,"https://service.capsulecrm.com/service/newlead");
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                "FORM_ID=31a74079-a113-4b60-971f-27900588e23a&SEND=FALSE&ORGANISATION_NAME=".$dealer_name."&PERSON_NAME=".$name."&PHONE=".$user_phone."&EMAIL=".$email."&STREET=".$address."&CITY=".$city."&STATE=".$state."&ZIP=".$zip."&CUSTOMFIELD[Dealer Group]=".$dealer_group."&CUSTOMFIELD[Dealership Phone]=".$dealer_phone."&CUSTOMFIELD[Username]=".$username."&CUSTOMFIELD[Membership Type]=".$subscription."&CUSTOMFIELD[Referred By]=".$referred_by."&CUSTOMFIELD[Password]=".$password."&TAG=".$owner."");

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $server_output = curl_exec ($ch);

                curl_close ($ch);
            */
    $data_string='{
    "party": {
    "type": "organisation",
    "name": "'.$dealer_name.'",
    "emailAddresses": [
      {
        "type": "Work",
        "address": "'.$email.'"
      }
    ],
    "addresses": [
      {
        "type": null,
        "city": "'.$city.'",
        "country": "'.$country.'",
        "street": "'.$address.'",
        "state": "'.$state.'",
        "zip": "'.$zip.'"
      }
    ],
    "phoneNumbers": [
      {
        "type": null,
        "number": "'.$dealer_phone.'"
      }
    ],
    "fields": [
      {
         "value": "'.$subscription.'",
         "definition": { "id": 207029 }
      },
      {
        "definition": { "id": 207032 } ,
        "value": "'.$name.'"
      },
      {
        "definition": { "id": 207030 } ,
        "value": "'.$username.'"
      },
      {
        "definition": { "id": 207031 } ,
        "value": "'.$password.'"
      },
      {
        "definition": { "id": 233138 } ,
        "value": "'.$referred_by.'"
      },
      {
        "definition": { "id": 361027 } ,
        "value": "'.$dealer_group.'"
      },
      {
        "definition": { "id": 361028 } ,
        "value": "'.$dealer_phone.'"
      },
      {
        "definition": { "id": 361029 } ,
        "value": "'.$subscription.'"
      }
    ]

  }
}';
            $ch = curl_init('https://api.capsulecrm.com/api/v2/parties');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: Bearer '.$capsule_key.'',
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'Content-Length: ' . strlen($data_string))
            );

            $result = curl_exec($ch);
            $data=json_decode($result);
            mysqli_query($db, "update dealer_dim set CRM_ID=".$data->party->id." where DEALER_ID='".$uid."'");



    $data_string3='{
  "task" : {
    "description" : "Follow up with new dealership",
    "party" : {
      "id" : '.$data->party->id.'
    },
	"category": {
                "id": 595601
            },
	"owner": {
                "id": 319756
            },
    "detail" : null,
    "dueOn" : "'.date("Y-m-d").'",
    "dueTime" : "00:00:00"
  }
}';
            $ch3 = curl_init('https://api.capsulecrm.com/api/v2/tasks');
            curl_setopt($ch3, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch3, CURLOPT_POSTFIELDS, $data_string3);
            curl_setopt($ch3, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch3, CURLOPT_HTTPHEADER, array(
                    'Authorization: Bearer '.$capsule_key.'',
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'Content-Length: ' . strlen($data_string3))
            );

            $result3 = curl_exec($ch3);
           // $data=json_decode($result);

            $data_string1='{
    "party": {
    "type": "person",
    "firstName": "'.$first_name.'",
    "lastName": "'.$last_name.'",
    "organisation": '.$data->party->id.',
    "emailAddresses": [
      {
        "type": "Work",
        "address": "'.$email.'"
      }
    ],
    "addresses": [
      {
        "type": null,
        "city": "'.$city.'",
        "country": "'.$country.'",
        "street": "'.$address.'",
        "state": "'.$state.'",
        "zip": "'.$zip.'"
      }
    ],
    "phoneNumbers": [
      {
        "type": null,
        "number": "'.$user_phone.'"
      }
    ],
    "fields": [
      {
        "definition": { "id": 207030 } ,
        "value": "'.$username.'"
      },
      {
        "definition": { "id": 207031 } ,
        "value": "'.$password.'"
      },
      {
        "definition": { "id": 233138 } ,
        "value": "'.$referred_by.'"
      },
      {
        "definition": { "id": 361027 } ,
        "value": "'.$dealer_group.'"
      }
    ]

  }
}';
            $ch = curl_init('https://api.capsulecrm.com/api/v2/parties');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: Bearer '.$capsule_key.'',
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'Content-Length: ' . strlen($data_string))
            );

            $result = curl_exec($ch);
            $data=json_decode($result);
            mysqli_query($db, "update users set CRM_ID=".$data->party->id." where DEALER_ID='".$uid."'");
        } else {
            echo "There was an error:\n";
            print_r($failures);
        }
    } else {
        echo '<div class="notification_error">' . $error . '</div>';
    }

}
?>