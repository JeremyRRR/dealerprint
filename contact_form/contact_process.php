<?php
	include("../config/db.php");
	include("../config/config.php");
	include("../config/keys.php");
	include_once "../swift/lib/swift_required.php";
	include dirname(dirname(__FILE__)).'/mail.php';
	include dirname(dirname(__FILE__)).'/mail.php';

	error_reporting (E_ALL ^ E_NOTICE);

	$post = (!empty($_POST)) ? true : false;

	if($post){
		include 'email_validation.php';
		$name = stripslashes($_POST['name']);
		$email = trim($_POST['email']);
		$subject = stripslashes($_POST['subject']);
		$message = stripslashes($_POST['message']);
		$error = '';

		// Check name
		if(!$name){
			$error .= 'Please enter your name.<br />';
		}

		// Check email

		if(!$email){
			$error .= 'Please enter an e-mail address.<br />';
		}

		if($email && !ValidateEmail($email)){
			$error .= 'Please enter a valid e-mail address.<br />';
		}

		// Check message (length)

		if(!$message || strlen($message) < 10){
			$error .= "Please enter your message. It should have at least 10 characters.<br />";
		}


		if(!$error){

			$from = array($email => $name);
			$to = array(ADMIN_EMAIL => SITE_AUTHOR);

			$text = $message;
			$html = $message;

			$transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 587);
			$transport->setUsername($row['MANDRILL_USERNAME']);
			$transport->setPassword($row['MANDRILL_PASSWORD']);
			$swift = Swift_Mailer::newInstance($transport);

			$message = new Swift_Message($subject);
			$message->setFrom($from);
			$message->setBody($html, 'text/html');
			$message->setTo($to);
			$message->addPart($text, 'text/plain');

			if ($recipients = $swift->send($message, $failures))
			{
			 echo 'OK';
			} else {
			 echo "There was an error:\n";
			 print_r($failures);
			}
		}
		else{
			echo '<div class="notification_error">'.$error.'</div>';
		}

	}
?>