<?php
	include("../config/db.php");
	include("../config/config.php");
	include("../config/keys.php");
	include_once "../swift/lib/swift_required.php";
	require_once('../Shippo.php');

	error_reporting (E_ALL ^ E_NOTICE);

	$post = (!empty($_POST)) ? true : false;

	if($post){
		include 'email_validation.php';
		$reseller_company_name =htmlspecialchars($_POST['reseller_company_name'], ENT_QUOTES);
		$reseller_address = $_POST['reseller_address'];
		$reseller_city = $_POST['reseller_city'];
		$reseller_state = $_POST['reseller_state'];
		$reseller_zip = $_POST['reseller_zip'];
		$reseller_phone = $_POST['reseller_phone'];
		$reseller_name = $_POST['reseller_name'];
		$reseller_name_array = explode(' ',trim($reseller_name));
		$reseller_first_name = $reseller_name_array[0];
		$reseller_last_name = $reseller_name_array[1];
		$reseller_email = trim($_POST['reseller_email']);
		$reseller_username = $_POST['reseller_username'];
		$reseller_password = $_POST['reseller_password'];
		$subject = " Welcome ".$reseller_name." to ".DOMAIN. "Reseller Program";
		$owner = $_POST['owner'];
		$reseller__phone = $_POST['dealer_phone'];
		$subscription = $_POST['subscription'];
		$error = '';



		// Check name
		if(!$reseller_name){
			$error .= 'Please enter your name.<br />';
		}
		// Check address
		if(!$reseller_address){
			$error .= 'Please enter your street address.<br />';
		}
		// Check city
		if(!$reseller_city){
			$error .= 'Please enter your city.<br />';
		}
		// Check state
		if(!$reseller_state){
			$error .= 'Please enter your state.<br />';
		}
		// Check zip
		if(!$reseller_zip){
			$error .= 'Please enter your zip code.<br />';
		}
		
		// Check email
		if(!$reseller_email){
			$error .= 'Please enter your e-mail address.<br />';
		}
		// Check phone
		if(!$reseller_phone){
			$error .= 'Please enter your phone number.<br />';
		}

		if($reseller_email && !ValidateEmail($reseller_email)){
			$error .= 'Please enter a valid e-mail address.<br />';
		}
		// Check username
		if(!$reseller_username){
			$error .= 'Please enter a username of your choice.<br />';
		}
		// Check for unique username
		$uquery = mysqli_query($db,"SELECT * FROM users WHERE USERNAME = '$reseller_username'");
		$rows  = mysqli_num_rows($uquery);
		if($rows >= 1){
			$error .= 'Please enter a unique username.<br />';
		}

		// Check password (length)
		if(!$reseller_password || strlen($reseller_password) < 8){
			$error .= "Please enter a password. It should have at least 8 characters.<br />";
		}


		if(!$error){
			// If no form errors insert user into database here

			//create sales rep user
		
			$uid=time();
			$date=date("Y-m-d");
			mysqli_query($db,"INSERT INTO users (
				`USER_ID` ,
				`USER_TYPE` ,
				`NAME` ,
				`DEALER_ID` ,
				`EMAIL` ,
				`USERNAME` ,
				`PASSWORD` ,
				`CREATOR_ID` ,
				`CREATE_DATE`
			)
			VALUES (
			'".$uid."', 'ResellerAdmin', '".$reseller_name."', '".$uid."', '".$reseller_email."', '".$reseller_username."', '".$reseller_password."', '".$owner."', '".$date."'
			);");
			
		
			//create sales rep
			mysqli_query($db,"INSERT INTO sales_reps (
				`REP_ID` ,
				`REP_FULLNAME` ,
				`REP_FIRSTNAME` ,
				`REP_LASTNAME` ,
				`REP_PHONE` ,
				`REP_EMAIL` ,
				`REP_STREET` ,
				`REP_CITY` ,
				`REP_STATE` ,
				`REP_ZIP` ,
				`REP_USERNAME` ,
				`REP_PASSWORD` 	
			)
			VALUES (
			'".$uid."', '".$reseller_name."', '".$reseller_first_name."', '".$reseller_last_name."', '".$reseller_phone."', '".$reseller_email."', '".$reseller_address."', '".$reseller_city."', '".$reseller_state."', '".$reseller_zip."', '".$reseller_username."', '".$reseller_password."');");

			// if no insertian errors then Send Mail
			$from = array(SYSTEM_EMAIL => DOMAIN);
			$to = array(ADMIN_EMAIL => SITE_AUTHOR, $reseller_email => $reseller_name);

			$text = "Hello ".$reseller_first_name.",\n
			Thanks for signing up to be a ".DOMAIN." reseller. Your account is set up and ready to go.\n
			Here are the login details:\n\n  Login Here: http://dealeraddendums.com/app/reseller  \nUsername: ".$reseller_username." \nPassword: ".$reseller_password."\n\n
			All of our accounts start out as free accounts. Once a dealer prints 5 on a free account you will get Finders Fee. When they upgrade you get a commission equal to their first month's bill.\n\n
			Upgrade Options:  We have three types of upgrade options - Manual and Automatic WEB and Automatic DMS.\n 
			Automatic accounts automatically populate the dealers inventory every night directly from their WEBsite provider or directly from their DMS.\n
			Manual accounts allow dealers to enter vehicles one at a time or by importing your inventory with an Excel spreadsheet.\n\n
			Addendum Labels:  Just for signing up and giving our service a try we'll send your new dealer 50 free printer labels. When they run out, just place an order for more. We'll send them Priority Mail and shipping is always free! \n
			If you have any questions, comments, or suggestions, please let us know.\n\n
			".SITE_AUTHOR." \n".COMPANY_NAME." \n".SUPPORT_PHONE." ";
			
			$html = "<strong>Hello ".$reseller_first_name.",</strong><br>
			Thanks for signing up to be a ".DOMAIN." reseller. Your account is set up and ready to go.  <br><br>
			<strong>Here are the login details:</strong><br>Login Here: http://dealeraddendums.com/reseller.php<br>Username: ".$reseller_username."<br>Password: ".$reseller_password."<br><br>
			All of our accounts start out as free accounts. Once a dealer prints 5 on a free account you will get Finders Fee. When they upgrade you get a commission equal to their first month's bill.<br>
			<strong>Free Account:</strong><br> This is a full featured 30 day free account that allows he dealer to manually enter as many vehicles as they want, and to print up to 50 addendums. Once that limit is reached they'll have to upgrade your account to continue using the system.<br><br>
			<strong>Upgrade Options:</strong><br>We have three types of upgrade accounts -  Manual Load, Automatic WEB and Automatic DMS. 
				<ul>
					<li><strong>Automatic DMS</strong> ($175/Mo) accounts automatically populate the dealers inventory every night directly from their DMS. (Reynolds or ADP)</li>
					<li><strong>Automatic Web</strong> ($125/Mo) accounts automatically populate dealer inventory every night directly from their WEBsite provider or inventory syndication company. (Dealer.com, VIN Solutions, Homenet, etc.)</li>
					<li><strong>Manual Load</strong> ($75/Mo) accounts allow the dealer to enter vehicles one at a time using our VIN decoder or by importing inventory in bulk from an Excel spreadsheet.</li>
				</ul>
			
			<strong>Training:</strong><br>Click here to <a href='".WEBSITE."tutorial' target='_blank'>watch a tutorial</a> video on how to set up and use the system...it's easy!<br><br>
			<strong>Addendum Labels:</strong><br>Just for signing up and giving our service a try we'll send your new dealer 50 free printer labels. When they run out, just place an order for more. We'll send them Priority Mail and shipping is always free!<br><br>

			If you have any questions, comments, or suggestions, please let us know.<br><br>
			<strong><em>".SITE_AUTHOR."</em></strong><br>".COMPANY_NAME."<br>".SUPPORT_PHONE."";

			$transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 587);
			$transport->setUsername($row['MANDRILL_USERNAME']);
			$transport->setPassword($row['MANDRILL_PASSWORD']);
			$swift = Swift_Mailer::newInstance($transport);

			$message = new Swift_Message($subject);
			$message->setFrom($from);
			$message->setBody($html, 'text/html');
			$message->setTo($to);
			$message->addPart($text, 'text/plain');

			if ($recipients = $swift->send($message, $failures))
			{
			 echo 'OK';


			 // send new customer to Capsul CRM
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://service.capsulecrm.com/service/newlead");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,
	        "FORM_ID=31a74079-a113-4b60-971f-27900588e23a&SEND=FALSE&ORGANISATION_NAME=".$reseller_company_name."&PERSON_NAME=".$name."&PHONE=".$phone."&EMAIL=".$email."&STREET=".$address."&CITY=".$city."&STATE=".$state."&ZIP=".$zip."&CUSTOMFIELD[Dealer Group]=".$dealer_group."&CUSTOMFIELD[Dealership Phone]=".$dealer_phone."&CUSTOMFIELD[Username]=".$username."&CUSTOMFIELD[Membership Type]=".$subscription."&CUSTOMFIELD[Referred By]=".$referred_by."&CUSTOMFIELD[Password]=".$password."&TAG=".$owner."");
	    
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec ($ch);

			curl_close ($ch);
			


			} else {
			 echo "There was an error:\n";
			 print_r($failures);
			}
		}
		else{
			echo '<div class="notification_error">'.$error.'</div>';
		}

	}
?>