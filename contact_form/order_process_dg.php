<?php
	include("../config/db.php");
	include("../config/config.php");
	include("../config/keys.php");
	include_once "../swift/lib/swift_required.php";
	require_once('../Shippo.php');

	error_reporting (E_ALL ^ E_NOTICE);

	$post = (!empty($_POST)) ? true : false;

	if($post){
		include 'email_validation.php';
		$dealer_name =htmlspecialchars($_POST['dealer_name'], ENT_QUOTES);
		$dealer_logo =$_POST['dealer_logo'];
		$address = $_POST['address'];
		$city = $_POST['city'];
		$state = $_POST['state'];
		$zip = $_POST['zip'];
		$phone = $_POST['phone'];
		$name = $_POST['name'];
		$name_array = explode(' ',trim($name));
		$first_name = $name_array[0];
		$email = trim($_POST['email']);
		$username = $_POST['username'];
		$password = $_POST['password'];
		$subject = " Welcome ".$dealer_name." to ".DOMAIN;
		$owner = $_POST['owner'];

		$error = '';

		// Check name
		if(!$dealer_name){
			$error .= 'Please enter your dealerships name.<br />';
		}
		// Check address
		if(!$address){
			$error .= 'Please enter your dealerships street address.<br />';
		}
		// Check city
		if(!$city){
			$error .= 'Please enter your dealerships city.<br />';
		}
		// Check state
		if(!$state){
			$error .= 'Please enter your dealerships state.<br />';
		}
		// Check zip
		if(!$zip){
			$error .= 'Please enter your dealerships zip code.<br />';
		}
		// Check phone
		if(!$phone){
			$error .= 'Please enter your dealerships phone number.<br />';
		}
		// Check name
		if(!$name){
			$error .= 'Please enter your name.<br />';
		}
		// Check email

		if(!$email){
			$error .= 'Please enter your e-mail address.<br />';
		}

		if($email && !ValidateEmail($email)){
			$error .= 'Please enter a valid e-mail address.<br />';
		}
		// Check username
		if(!$username){
			$error .= 'Please enter a username of your choice.<br />';
		}
		// Check for unique username
		$uquery = mysql_query("SELECT * FROM users WHERE USERNAME = '$username'");
		$rows  = mysql_num_rows($uquery);
		if($rows >= 1){
			$error .= 'Please enter a unique username.<br />';
		}

		// Check password (length)
		if(!$password || strlen($password) < 8){
			$error .= "Please enter a password. It should have at least 8 characters.<br />";
		}


		if(!$error){
			// If no form errors insert user into database here

			//create dealer
		
			$uid=time();
			$date=date("Y-m-d");
			mysql_query("INSERT INTO users (
				`USER_ID` ,
				`USER_TYPE` ,
				`NAME` ,
				`DEALER_ID` ,
				`EMAIL` ,
				`USERNAME` ,
				`PASSWORD` ,
				`CREATOR_ID` ,
				`CREATE_DATE`
			)
			VALUES (
			'".$uid."', 'DealerAdmin', '".$name."', '".$uid."', '".$email."', '".$username."', '".$password."', '".$owner."', '".$date."'
			);");
				
				$address1 = $address.", ".$city.", ".$state." ".$zip.", USA";
        //$address = $row['DEALER_ADDRESS']+", "+$row['DEALER_CITY']+", "+$row['DEALER_STATE']+" "+$row['DEALER_ZIP']+", USA"; // Google HQ
        $prepAddr = str_replace(' ','+',$address1);
		$prepAddr = str_replace('Road','# ',$prepAddr);
		$prepAddr = str_replace('Road ','#',$prepAddr);
        $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
        $output= json_decode($geocode);
        $latitude = $output->results[0]->geometry->location->lat;
        $longitude = $output->results[0]->geometry->location->lng;
				
			mysqli_query($db,"INSERT INTO `infosheet` (`INFO_ID`, `DEALER_ID`, `USER_ID`, `INFO_TYPE`, `INFO_STYLE`) VALUES (NULL, '".$uid."', '".$uid."', 'standard', 'KBB');");
		mysqli_query($db,"INSERT INTO `infosheet` (`INFO_ID`, `DEALER_ID`, `USER_ID`, `INFO_TYPE`, `INFO_STYLE`) VALUES (NULL, '".$uid."', '".$uid."', 'certified', 'KBB');");
				mysql_query("INSERT INTO dealer_dim (
`ID` ,
`Active` ,
`DEALER_ID` ,
`DEALER_NAME` ,
`PRIMARY_CONTACT` ,
`PRIMARY_CONTACT_EMAIL` ,
`DEALER_LOGO` ,
`DEALER_ADDRESS` ,
`DEALER_CITY` ,
`DEALER_STATE` ,
`DEALER_ZIP` ,
`PHONE` ,
`BILLING_DATE` ,
`BILLING_STREET` ,
`BILLING_CITY` ,
`BILLING_STATE` ,
`BILLING_ZIP` ,
`OWNER` ,
`ACCOUNT_TYPE` ,
`ADDENDUM_STYLE`, 
`LAT1` ,
`LNG1`
)
VALUES (
'".$uid."', 'Yes', '".$uid."', '".$dealer_name."', '".$name."', '".$email."', '".$dealer_logo."', '".$address."', '".$city."', '".$state."', '".$zip."', '".$phone."', '', '".$address."', '".$city."', '".$state."', '".$zip."', '".$owner."', 'Free', 'Standard', '".$latitude."', '".$longitude."'
);");

			// if no insertian errors then Send Mail
			$from = array(SYSTEM_EMAIL => DOMAIN);
			$to = array(ADMIN_EMAIL => SITE_AUTHOR,
				$email => $name);

			$text = "Hello ".$first_name.",\n
			Thanks for signing up with".DOMAIN." where creating, printing and managing addendums is now easier than ever! We are thrilled to have you onboard. Your new account has been created and is ready to go.\n
			Here are the login details:\n\n  Login Here: http://dealeraddendums.com/app  \nUsername: ".$username." \nPassword: ".$password."\n\n
			You have signed up for our Free Account:  \nThis is a full featured free account that allows you to manually enter as many vehicles as you want, and to print up to 50 addendums. Once that limit is reached you'll have to upgrade your account to continue using the system.\n\n
			Upgrade Options:  We have two types of upgrade options - Manual and Automatic.\n 
			Automatic accounts automatically populate your dealer inventory every night directly from your DMS. Automatic accounts also have several design options for their addendums.\n
			Manual accounts allow you to enter vehicles one at a time or by importing your inventory with an Excel spreadsheet.\n\n
			Addendum Labels:  Just for signing up and giving our service a try we'll send you 50 free printer labels. When you run out, just place an order for more. We'll send them to you Priority Mail and shipping is always free! \n
			We hope you enjoy our service! If you have any questions, comments, or suggestions, please let us know.\n\n
			".SITE_AUTHOR." \n".COMPANY_NAME." \n".SUPPORT_PHONE." ";
			
			$html = "<strong>Hello ".$first_name.",</strong><br>
			Thanks for signing up with ".DOMAIN." where creating, printing and managing addendums is now easier than ever! We are thrilled to have you onboard. Your new account has been created and is ready to go.  <br><br>
			<strong>Here are the login details:</strong><br>Login Here: ".SITE_URL."<br>Username: ".$username."<br>Password: ".$password."<br><br>
			<strong>Free Account:</strong><br>You have signed up for our Free Account. This is a full featured free account that allows you to manually enter as many vehicles as you want, and to print up to 50 addendums. Once that limit is reached you'll have to upgrade your account to continue using the system.<br><br>
			<strong>Upgrade Options:</strong><br>We have two types of upgrade accounts -  Manual and Automatic. 
				<ul>
					<li>Automatic ($125/Mo) accounts automatically populate your dealer inventory every night directly from your DMS. Automatic accounts also have several design options for their addendums.</li>
					<li>Manual ($75/Mo) accounts allow you to enter vehicles one at a time or by importing your inventory with an Excel spreadsheet.</li>
				</ul>
			
			<strong>Training:</strong><br>Click here to <a href='".WEBSITE."tutorial' target='_blank'>watch a tutorial</a> video on how to set up and use the system...it's easy!<br><br>
			<strong>Addendum Labels:</strong><br>Just for signing up and giving our service a try we'll send you 50 free printer labels. When you run out, just place an order for more. We'll send them to you Priority Mail and shipping is always free!<br><br>

			We hope you enjoy our service! If you have any questions, comments, or suggestions, please let us know.<br><br>
			<strong><em>".SITE_AUTHOR."</em></strong><br>".COMPANY_NAME."<br>".SUPPORT_PHONE."";

			$transport = Swift_SmtpTransport::newInstance('smtp.mandrillapp.com', 587);
			$transport->setUsername($row['MANDRILL_USERNAME']);
			$transport->setPassword($row['MANDRILL_PASSWORD']);
			$swift = Swift_Mailer::newInstance($transport);

			$message = new Swift_Message($subject);
			$message->setFrom($from);
			$message->setBody($html, 'text/html');
			$message->setTo($to);
			$message->addPart($text, 'text/plain');

			if ($recipients = $swift->send($message, $failures))
			{
			 echo 'OK';


			 // send new customer to Capsul CRM
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://service.capsulecrm.com/service/newlead");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,
	        "FORM_ID=31a74079-a113-4b60-971f-27900588e23a&SEND=FALSE&ORGANISATION_NAME=".$dealer_name."&PERSON_NAME=".$name."&PHONE=".$phone."&email=".$email."&STREET=".$address."&CITY=".$city."&STATE=".$state."&ZIP=".$zip."&CUSTOMFIELD[Username]=".$username."&CUSTOMFIELD[Password]=".$password."&TAG=".$owner."");
	    
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec ($ch);

			curl_close ($ch);
			 // send new customer to Capsul CRM


			} else {
			 echo "There was an error:\n";
			 print_r($failures);
			}
		}
		else{
			echo '<div class="notification_error">'.$error.'</div>';
		}

	}
?>