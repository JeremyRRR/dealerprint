<?php
include("../app/assets/config/db.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>DealerAddendums.com - Dealers Map</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  
  <!-- style -->
  <link rel="stylesheet" href="../app/assets/plugins/animate.css/animate.min.css" type="text/css" />
  <link rel="stylesheet" href="../app/assets/plugins/glyphicons/glyphicons.css" type="text/css" />
  <link rel="stylesheet" href="../app/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="../app/assets/plugins/material-design-icons/material-design-icons.css" type="text/css" />

  <link rel="stylesheet" href="../app/assets/plugins/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
  <!-- build:css ../assets/styles/app.min.css -->
  <link rel="stylesheet" href="../app/assets/styles/app.css" type="text/css" />
  <!-- endbuild -->
  <link rel="stylesheet" href="../app/assets/styles/font.css" type="text/css" />
  <style type="text/css">
  .list-group-item{
	  cursor:pointer;
  }
  #map{
	  color:#000;
  }
  li{
	list-style-type:none;
}
.box{
	margin-right: 0rem;
margin-left: 0rem;
}
  </style>
</head>
 <script src="../app/assets/libs/jquery/jquery/dist/jquery.js"></script>
<!-- Bootstrap -->
  <script src="../app/assets/libs/jquery/bootstrap/dist/js/bootstrap.js"></script>
      <script type="text/javascript"
      src="https://maps.google.cn/maps/api/js?key=AIzaSyCt37fhI952cxcda9XhpuyccZbKbjCIyyo">
    </script>
<script type="text/javascript">
function initialise() {
		   $.ajax({
  type: "POST",
  url: "get_locations.php",
  dataType: 'json'
	}).success(function( data ) {

	updateLocation(data);

},'json');
function updateLocation(data){
var locations = data;
gmarkers = [];
var map = new google.maps.Map(document.getElementById('map'), {
    zoom:4,
	zoomControl: true,
    zoomControlOptions: {
	  position: google.maps.ControlPosition.LEFT_BOTTOM
    },
	panControl: true,
    panControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
    },
	scaleControl: true,  // fixed to BOTTOM_RIGHT
    streetViewControl: true,
    streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
    },
	mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DEFAULT,
	  position: google.maps.ControlPosition.LEFT_CENTER
    },
    center: new google.maps.LatLng(38.8468548, -98.6433766),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
	styles: [{"featureType":"administrative.locality","elementType":"all","stylers":[{"hue":"#2c2e33"},{"saturation":7},{"lightness":19},{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":-2},{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"hue":"#e9ebed"},{"saturation":-90},{"lightness":-8},{"visibility":"simplified"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":10},{"lightness":69},{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":-78},{"lightness":67},{"visibility":"simplified"}]}]
});

var infowindow = new google.maps.InfoWindow();


function createMarker(latlng, html, icon, dtype) {
    var marker = new google.maps.Marker({
        position: latlng,
		animation: google.maps.Animation.DROP,
        map: map,
		icon: 'images/'+icon,
		serialNumber: dtype
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(html);
        infowindow.open(map, marker);
    });
	function toggleBounce() {
  if (marker.getAnimation() != null) {
    marker.setAnimation(null);
	marker.setIcon('images/'+icon);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
	 marker.setIcon('images/car.png');
	 setTimeout(function () {
    marker.setAnimation(null);
	marker.setIcon('images/'+icon);
}, 10000);
  }
}
	google.maps.event.addListener(marker, 'click', toggleBounce);
    return marker;
}
var icon2="";
for (var i = 0; i < locations.length; i++) {
	if(locations[i].ACCOUNT_TYPE=="Free"){
		icon2="blue.png";
		dtype="free";
	}else{
		icon2="green.png";
		dtype="paid";
	}
    gmarkers[locations[i].ID] =
    createMarker(new google.maps.LatLng(locations[i].LAT1, locations[i].LNG1), "<strong>"+locations[i].DEALER_NAME + "</strong><br>" +locations[i].DEALER_ADDRESS + "<br>" + locations[i].DEALER_CITY + ", " + locations[i].DEALER_STATE+ " " + locations[i].DEALER_ZIP,icon2, dtype );
}
}
}
function free() {
		   $.ajax({
  type: "POST",
  url: "get_locations.php?dtype=free",
  dataType: 'json'
	}).success(function( data ) {

	updateLocation(data);

},'json');
function updateLocation(data){
var locations = data;
gmarkers = [];
var map = new google.maps.Map(document.getElementById('map'), {
    zoom:4,
	zoomControl: true,
    zoomControlOptions: {
	  position: google.maps.ControlPosition.LEFT_BOTTOM
    },
	panControl: true,
    panControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
    },
	scaleControl: true,  // fixed to BOTTOM_RIGHT
    streetViewControl: true,
    streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
    },
	mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DEFAULT,
	  position: google.maps.ControlPosition.LEFT_CENTER
    },
    center: new google.maps.LatLng(38.8468548, -98.6433766),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
	styles: [{"featureType":"administrative.locality","elementType":"all","stylers":[{"hue":"#2c2e33"},{"saturation":7},{"lightness":19},{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":-2},{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"hue":"#e9ebed"},{"saturation":-90},{"lightness":-8},{"visibility":"simplified"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":10},{"lightness":69},{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":-78},{"lightness":67},{"visibility":"simplified"}]}]
});

var infowindow = new google.maps.InfoWindow();


function createMarker(latlng, html, icon, dtype) {
    var marker = new google.maps.Marker({
        position: latlng,
		animation: google.maps.Animation.DROP,
        map: map,
		icon: 'images/'+icon,
		serialNumber: dtype
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(html);
        infowindow.open(map, marker);
    });
	function toggleBounce() {
  if (marker.getAnimation() != null) {
    marker.setAnimation(null);
	marker.setIcon('images/'+icon);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
	 marker.setIcon('images/car.png');
	 setTimeout(function () {
    marker.setAnimation(null);
	marker.setIcon('images/'+icon);
}, 10000);
  }
}
	google.maps.event.addListener(marker, 'click', toggleBounce);
    return marker;
}
var icon2="";
for (var i = 0; i < locations.length; i++) {
	if(locations[i].ACCOUNT_TYPE=="Free"){
		icon2="blue.png";
		dtype="free";
	}else{
		icon2="green.png";
		dtype="paid";
	}
    gmarkers[locations[i].ID] =
    createMarker(new google.maps.LatLng(locations[i].LAT1, locations[i].LNG1), "<strong>"+locations[i].DEALER_NAME + "</strong><br>" +locations[i].DEALER_ADDRESS + "<br>" + locations[i].DEALER_CITY + ", " + locations[i].DEALER_STATE+ " " + locations[i].DEALER_ZIP,icon2, dtype );
}
}
}
function all1() {
		   $.ajax({
  type: "POST",
  url: "get_locations.php",
  dataType: 'json'
	}).success(function( data ) {

	updateLocation(data);

},'json');
function updateLocation(data){
var locations = data;
gmarkers = [];
var map = new google.maps.Map(document.getElementById('map'), {
    zoom:4,
	zoomControl: true,
    zoomControlOptions: {
	  position: google.maps.ControlPosition.LEFT_BOTTOM
    },
	panControl: true,
    panControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
    },
	scaleControl: true,  // fixed to BOTTOM_RIGHT
    streetViewControl: true,
    streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
    },
	mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DEFAULT,
	  position: google.maps.ControlPosition.LEFT_CENTER
    },
    center: new google.maps.LatLng(38.8468548, -98.6433766),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
	styles: [{"featureType":"administrative.locality","elementType":"all","stylers":[{"hue":"#2c2e33"},{"saturation":7},{"lightness":19},{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":-2},{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"hue":"#e9ebed"},{"saturation":-90},{"lightness":-8},{"visibility":"simplified"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":10},{"lightness":69},{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":-78},{"lightness":67},{"visibility":"simplified"}]}]
});

var infowindow = new google.maps.InfoWindow();


function createMarker(latlng, html, icon, dtype) {
    var marker = new google.maps.Marker({
        position: latlng,
		animation: google.maps.Animation.DROP,
        map: map,
		icon: 'images/'+icon,
		serialNumber: dtype
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(html);
        infowindow.open(map, marker);
    });
	function toggleBounce() {
  if (marker.getAnimation() != null) {
    marker.setAnimation(null);
	marker.setIcon('images/'+icon);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
	 marker.setIcon('images/car.png');
	 setTimeout(function () {
    marker.setAnimation(null);
	marker.setIcon('images/'+icon);
}, 10000);
  }
}
	google.maps.event.addListener(marker, 'click', toggleBounce);
    return marker;
}
var icon2="";
for (var i = 0; i < locations.length; i++) {
	if(locations[i].ACCOUNT_TYPE=="Free"){
		icon2="blue.png";
		dtype="free";
	}else{
		icon2="green.png";
		dtype="paid";
	}
    gmarkers[locations[i].ID] =
    createMarker(new google.maps.LatLng(locations[i].LAT1, locations[i].LNG1), "<strong>"+locations[i].DEALER_NAME + "</strong><br>" +locations[i].DEALER_ADDRESS + "<br>" + locations[i].DEALER_CITY + ", " + locations[i].DEALER_STATE+ " " + locations[i].DEALER_ZIP,icon2, dtype );
}
}
}
function paid() {
		   $.ajax({
  type: "POST",
  url: "get_locations.php?dtype=paid",
  dataType: 'json'
	}).success(function( data ) {

	updateLocation(data);

},'json');
function updateLocation(data){
var locations = data;
gmarkers = [];
var map = new google.maps.Map(document.getElementById('map'), {
    zoom:4,
	zoomControl: true,
    zoomControlOptions: {
	  position: google.maps.ControlPosition.LEFT_BOTTOM
    },
	panControl: true,
    panControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
    },
	scaleControl: true,  // fixed to BOTTOM_RIGHT
    streetViewControl: true,
    streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_BOTTOM
    },
	mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DEFAULT,
	  position: google.maps.ControlPosition.LEFT_CENTER
    },
    center: new google.maps.LatLng(38.8468548, -98.6433766),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
	styles: [{"featureType":"administrative.locality","elementType":"all","stylers":[{"hue":"#2c2e33"},{"saturation":7},{"lightness":19},{"visibility":"on"}]},{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ffffff"},{"saturation":-100},{"lightness":100},{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":31},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"labels","stylers":[{"hue":"#bbc0c4"},{"saturation":-93},{"lightness":-2},{"visibility":"simplified"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"hue":"#e9ebed"},{"saturation":-90},{"lightness":-8},{"visibility":"simplified"}]},{"featureType":"transit","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":10},{"lightness":69},{"visibility":"on"}]},{"featureType":"water","elementType":"all","stylers":[{"hue":"#e9ebed"},{"saturation":-78},{"lightness":67},{"visibility":"simplified"}]}]
});

var infowindow = new google.maps.InfoWindow();


function createMarker(latlng, html, icon, dtype) {
    var marker = new google.maps.Marker({
        position: latlng,
		animation: google.maps.Animation.DROP,
        map: map,
		icon: 'images/'+icon,
		serialNumber: dtype
    });
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(html);
        infowindow.open(map, marker);
    });
	function toggleBounce() {
  if (marker.getAnimation() != null) {
    marker.setAnimation(null);
	marker.setIcon('images/'+icon);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
	 marker.setIcon('images/car.png');
	 setTimeout(function () {
    marker.setAnimation(null);
	marker.setIcon('images/'+icon);
}, 10000);
  }
}
	google.maps.event.addListener(marker, 'click', toggleBounce);
    return marker;
}
var icon2="";
for (var i = 0; i < locations.length; i++) {
	if(locations[i].ACCOUNT_TYPE=="Free"){
		icon2="blue.png";
		dtype="free";
	}else{
		icon2="green.png";
		dtype="paid";
	}
    gmarkers[locations[i].ID] =
    createMarker(new google.maps.LatLng(locations[i].LAT1, locations[i].LNG1), "<strong>"+locations[i].DEALER_NAME + "</strong><br>" +locations[i].DEALER_ADDRESS + "<br>" + locations[i].DEALER_CITY + ", " + locations[i].DEALER_STATE+ " " + locations[i].DEALER_ZIP,icon2, dtype );
}
}
}
setInterval(function(){
		$.ajax({
			    	url: "get_values.php",
			    	type: "get",
					dataType: 'json',
					cache:false,
			      	success: function(html){
						$(".cities").html(html.cities);
						$(".addendums").html(html.addendums);
						$(".prints30d").html(html.prints30d);
						$(".atprints").html(html.atprints);
						$(".mostinaday").html(html.mostinaday);
						$("#cities").html(html.cities);
						if(html.addendums!=$("#addendums").val()){
						$("#addendums").val(html.addendums);
							$.ajax({
			    	url: "get_last.php",
			    	type: "get",
					dataType: 'json',
					cache:false,
			      	success: function(myresponse){
						if(myresponse.id!=$("#last_data").val()){
							$(".listrow").prepend($("<li id='w"+myresponse.id+"' onMouseOver=javascript:google.maps.event.trigger(gmarkers['"+myresponse.dealer_id+"'],'click');javascript:google.maps.event.trigger(gmarkers['"+myresponse.dealer_id+"'],'mouseover'); onMouseOut=javascript:google.maps.event.trigger(gmarkers['"+myresponse.dealer_id+"'],'click');javascript:google.maps.event.trigger(gmarkers['"+myresponse.dealer_id+"'],'mouseout'); class='button3 list-group-item active'><div class='clear'><div class='_500 block'>"+myresponse.dealer+"</div><span class='text-muted'>"+myresponse.year+" "+myresponse.make+" "+myresponse.model+"<br /> Stock # "+myresponse.stock+"</span></div></li>").slideDown("slow"));
							$("#last_data").val(myresponse.id);
							setTimeout(function(){ $("#w"+myresponse.id).removeClass("active"); }, 10000);
							 goo(myresponse.dealer_id);
						}
					}
		});
						}
					}
		});
		}, 10000);
		function goo(id){
			javascript:google.maps.event.trigger(gmarkers[id],'click');
		}
		
</script>
<?php
$count_sql=mysqli_query($db,"select*from count_table where id=1");
$count=mysqli_fetch_array($count_sql);
$cu_dealer=mysqli_query($db,"SELECT count(*) as total from dealer_dim where ACTIVE='Yes'");
$count_dealer=mysqli_fetch_array($cu_dealer);
$dealer_cities=mysqli_query($db,"select * from dealer_dim where ACTIVE='Yes' group by DEALER_CITY");
$countc=mysqli_num_rows($dealer_cities);
$print_date=date("Y-m-d");
$count1=$count['print_today'];
$atprints=$count['printed'];
$oldprints=$count['old_addendum'];
$last_30=$count['last_30'];
$most_a_day=$count['most_aday'];
/*
$addendums_sql=mysqli_query($db,"SELECT count(*) as addendums from dealer_inventory where STATUS='1' and PRINT_DATE='".$print_date."'");
$count1=mysqli_fetch_assoc($addendums_sql);
$atprints_sql=mysqli_query($db,"SELECT count(*) as count from dealer_inventory where PRINT_FLAG='1'");
$atprints_old=mysqli_query($db,"SELECT count(*) as count from z_dealer_inventory_inactive where PRINT_FLAG='1'");
$atprints=mysqli_fetch_assoc($atprints_sql) ;
$oldprints=mysqli_fetch_assoc($atprints_old) ;


$today_date=date("Y-m-d");
$date30=date('Y-m-d', strtotime("now -30 days") );
$l_30=mysqli_query($db,"SELECT count(*) as total from dealer_inventory where PRINT_DATE
BETWEEN '".$date30."'
AND '".$today_date."'");
$last_30=mysqli_fetch_assoc($l_30);
*/
?>
<body onLoad="initialise()" class=" dark pace-done">
  <div class="app" id="app">

<!-- ############ LAYOUT START-->

  <!-- aside -->
  
  <!-- / aside -->
  
  <!-- content -->
  <div id="content" class="app-content box-shadow-z0" role="main">
    <div class="app-header white box-shadow navbar-md" style="text-align:center">
        <a class="navbar-brand" style="width:100%">
      <div ui-include="'assets/images/Logo_300x36.svg'"></div>
      <input type="hidden" id="cities" value="<?php echo $countc;?>" />
<input type="hidden" id="addendums" value="<?php echo $count1;?>" />
      <img src="https://d3manp75urydp4.cloudfront.net/Logo_300x36.png" alt="."> </a>
    </div>
    <div ui-view class="app-body" id="view">

<!-- ############ PAGE START-->
<div class="padding">
	
	<div class="row">
		<div class="col-sm-12 col-md-5 col-lg-12">
			<div class="row">
				<div class="col-lg-2 col-sm-6">
			        <div class="box p-a">
			          <div class="pull-left m-r">
			          	<i class="fa fa-spinner text-2x text-success m-y-sm"></i>
			          </div>
			          <div class="clear">
			          	<div class="text-muted">Today</div>
			            <h4 class="m-a-0 text-md _600 addendums"><?php echo $count1;?></h4>
			          </div>
			        </div>
			    </div>
                <div class="col-lg-2 col-sm-6">
			        <div class="box p-a">
			          <div class="pull-left m-r">
			          	<i class="fa fa-bar-chart text-2x text-success m-y-sm"></i>
			          </div>
			          <div class="clear">
			          	<div class="text-muted">Single Day</div>
			            <h4 class="m-a-0 text-md _600 mostinaday"><?php echo $most_a_day;?></h4>
			          </div>
			        </div>
			    </div>
			    <div class="col-lg-2 col-sm-6">
			        <div class="box p-a">
			          <div class="pull-left m-r">
			          	<i class="fa fa-calendar text-2x text-success m-y-sm"></i>
			          </div>
			          <div class="clear">
			          	<div class="text-muted">30 Day</div>
			            <h4 class="m-a-0 text-md _600 prints30d"><?php echo $last_30;?></h4>
			          </div>
			        </div>
			    </div>
			    <div class="col-lg-2 col-sm-6">
			        <div class="box p-a">
			          <div class="pull-left m-r">
			          	<i class="fa fa-database text-2x text-success m-y-sm"></i>
			          </div>
			          <div class="clear">
			          	<div class="text-muted">Total Prints</div>
			            <h4 class="m-a-0 text-md _600 atprints"><?php echo intval($atprints)+460000 + $oldprints;?></h4>
			          </div>
			        </div>
			    </div>
			    <div class="col-lg-2 col-sm-6">
			        <div class="box p-a">
			          <div class="pull-left m-r">
			          	<i class="fa fa-building text-2x text-success m-y-sm"></i>
			          </div>
			          <div class="clear">
			          	<div class="text-muted">Cities</div>
			            <h4 class="m-a-0 text-md _600 cities"><?php echo $countc;?></h4>
			          </div>
			        </div>
			    </div>
                
			    
                <div class="col-lg-2 col-sm-6">
			        <div class="box p-a">
			          <div class="pull-left m-r">
			          	<i class="fa fa-group text-2x text-success m-y-sm"></i>
			          </div>
			          <div class="clear">
			          	<div class="text-muted">Dealers</div>
			            <h4 class="m-a-0 text-md _600"><a href><?php echo $count_dealer['total'];?></a></h4>
			          </div>
			        </div>
			    </div>
		    </div>
            
	    </div>
        
	    <div class="col-sm-12 col-md-7 col-lg-9">
	    	<div class="row-col box dark bg">
            
		        <div class="col-sm-8">
					<button onClick="all1()" class="md-btn md-flat m-b-sm w-xs text-default">All</button> <button onClick="paid()" class="md-btn md-flat m-b-sm w-xs text-success">Paid</button> <button onClick="free()" class="md-btn md-flat m-b-sm w-xs text-primary">Trial</button>
			        <div class="box-body" id="map" style="height:420px">
			            
			        </div>
		        </div>
		        
		    </div>
	    </div>
        <div class="col-md-12 col-xl-3">
			<div class="box" style="max-height:460px; overflow: hidden">
				<div class="box-header">
					<h3>Latest Addendums</h3>
				</div>
				<ul class="list-group listrow no-border">
			         <?php
	$vehicle_sql=mysqli_query($db,"select * from dealer_inventory WHERE PRINT_STATUS='1' and STATUS='1' order by PT DESC LIMIT 0,5");
	$i=0;
	while($vrow=mysqli_fetch_array($vehicle_sql)){
		$i++;
		if($i==1){
			$last_data=$vrow['id'];
			}
		$dsql=mysqli_query($db,"select*from dealer_dim where DEALER_ID='".$vrow['DEALER_ID']."'");
		$drow=mysqli_fetch_array($dsql);
	?> 
        <li onMouseOver="javascript:google.maps.event.trigger(gmarkers['<?php echo $drow['ID'];?>'],'click');javascript:google.maps.event.trigger(gmarkers['<?php echo $drow['ID'];?>'],'mouseover');" onMouseOut="javascript:google.maps.event.trigger(gmarkers['<?php echo $drow['ID'];?>'],'click');javascript:google.maps.event.trigger(gmarkers['<?php echo $drow['ID'];?>'],'mouseout');" class="list-group-item button3">
			          
			          <div class="clear">
			            <div class="_500 block"><?php echo $drow['DEALER_NAME'];?></div>
			            <span class="text-muted"><?php echo $vrow['YEAR'];?> <?php echo $vrow['MAKE'];?> <?php echo $vrow['MODEL'];?><br>Stock # <?php echo $vrow['STOCK_NUMBER'];?></span>
			          </div>
			        </li>
        <?php
	}
	
		?>
			    </ul>
		    </div>
		</div>
	</div>
</div>
<!-- ############ PAGE END-->
    </div>
  </div>
  <!-- / -->
  <!-- theme switcher -->
  <!-- / -->
<!-- ############ LAYOUT END-->
  </div>
  <input type="hidden" value="<?php echo $last_data;?>" id="last_data" />
<!-- build:js scripts/app.html.js -->
</body>
</html>
