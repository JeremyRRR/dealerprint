<?php
define("SITE_URL","http://dealeraddendums.com/app/");
define("WEBSITE","http://dealeraddendums.com/");
define("DOMAIN","DealerAddendums.com");
define("APP_NAME","Dealer Addendum Platform");
define("COMPANY_NAME","DealerAddendums Inc");
define("COPYRIGHT","DealerAddendums Inc.");
define("LOGOURL","https://s3.amazonaws.com/addendum-websiteimages/Logo_WhiteAndOrange_300x36.png");
define("LOGIN_LOGOURL","/img/Logo_BlackAndOrange_300x36.png");

define("SITE_SUPPORT","Allan Tone");
define("SITE_AUTHOR","Allan Tone");
define("SITE_AUTHOR_EMAIL","allan@dealeraddendums.com");
define("SITE_AUTHOR_PHONE","240-475-1116");
define("AUTHOR_URL","#");

define("SITE_RESELLER","Allan Tone");
define("SITE_RESELLER_EMAIL","allan@dealeraddendums.com");
define("SITE_RESELLER_PHONE","800-231-7124");

define("SUPPORT_NAME","Allan Tone");
define("SUPPORT_PHONE","1-800-231-7124");
define("SUPPORT_EMAIL","support@dealeraddendums.com");

define("SALES_SUPPORT_NAME","Allan Tone");
define("SALES_SUPPORT_EMAIL","support@dealeraddendums.com");
define("SALES_SUPPORT_PHONE","1-800-231-7124");

define("SALES_NAME","Allan Tone");
define("SALES_EMAIL","allan@dealeraddendums.com");
define("SALES_PHONE","800-231-7124");

define("ADMIN_NAME","Allan Tone");
define("ADMIN_PHONE","1-800-231-7124");
define("ADMIN_EMAIL","appadmin@dealeraddendums.com");
define("ADMIN_COMPANY","DealerAddendums Inc.");

define("CRM_EMAIL","appadmin@dealeraddendums.com");

define("ORDERS_PHONE","1-800-231-7124");
define("ORDERS_EMAIL","support@dealeraddendums.com");
define("ORDERS_NAME","Label Order Team");

define("SYSTEM_EMAIL","no-reply@dealeraddendums.com");

define("ADDENDUM_LINK","#order"); //includes/header.php line 263
define("SLOGAN" , "is powered by <a href='http://liqueo.com' target='_blank'> DealerAddendums Inc</a>"); //includes/footer.php line 7
define("ADDENDUM_PRICING_TABLE","label_price"); //home.php line 64

define("AUTOMATIC_PRICE","150");
define("MANUAL_PRICE","100");

$copy_year=date('Y');
define("YEAR",$copy_year);


define("IMG_FTP","images.dealeraddendums.com");
define("IMG_USER","images");
define("IMG_PASS","images2626");

define("CDN_URL","http://9fbc328b4af46c38978c-b0f0e9b8d518c7eabf540bcb1200b057.r3.cf1.rackcdn.com/www/print/images")
?>
