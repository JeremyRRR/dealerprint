<?php

// v.3.70.72 - Updated ATT narrow
// v.3.70.73 - Updated Buyers Guide and fixed Iphone log in issue
// v.3.70.74 - Removed Edmunds as the default VIN decoder
// v.3.70.75 - Updated VIN decoder errors
// v.3.70.76 - Fixed user rights to be dynamic
// v.3.70.77 - Millennium Dealer Services
// v.3.70.78 - Added Audi Narrow template
// v.3.70.79 - Added pop up and Google adWorks tracking
// v.3.70.83 - Added new exit pop up
// v.3.70.84 - Fixed bug in template that was keeping deleted options
// v.3.70.85 - Fixed bug that was keeping deleted options
// v.3.70.86 - Update Freshbooks webhoot for recurring profile
// v.3.70.88 - Added CPO buyers Guide
// v.3.70.89 - Fixed bug in CPO buyers Guide
// v.3.70.94 - Fixed order label notification
// v.3.70.96 - Truncate 6 more options from KBB-Carfax infosheet
// v.3.70.97 - Added Smart Pizel
// v.3.70.98 - Updated Search
// v.3.70.99 - maus template fix
// v.3.70.100 - Updated Automax Template and QR encoding
// v.3.70.101 - Updated Automax Template and QR encoding
// v.3.70.102 - Updated QR Encoding from Template Builder
// v.3.70.104 - Created Custom9

define("RELEASE_VERSION","v.3.70.104 - Updated 10/03/18 at 11:30am MST");
define("LATEST_VERSION_NEWS","Created Custom9");
define("LATEST_VERSION_DATE","10/02/18 at 11:30am MST");
define("LATEST_VERSION_NUMBER","v.3.70.104");
define("DASEVER_URL","http://localhost/allon");

?>
