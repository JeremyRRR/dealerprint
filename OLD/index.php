<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		
		<!-- BASIC INFO -->
		<title>Addendum System for Car Dealers | DealerAddendums Inc. </title>
		<META NAME="description" CONTENT="New vehicle addendums, addendum software and addendum templates. Printing addendums has never been easier than with DealerAddendums.com">
		<META NAME="keywords" CONTENT="dealer addendums, automotive addendum, auto dealer stickers, car dealership labels, addendum templates, window stickers, addendum labels, addendum software, buyers guides, information sheets,  VIN Decoder, CDK, 3PA">
		<META NAME="robot" CONTENT="index,follow">
		<META NAME="copyright" CONTENT="Copyright © 2017 DealerAddendums.com. All Rights Reserved.">
		<META NAME="author" CONTENT="Allan Tone">
		<META NAME="language" CONTENT="English">
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0" />

		<!-- FAVICONS -->
		<link rel="icon" href="http://d14bqw9lzcvxs9.cloudfront.net/favicon.ico">
		<link rel="apple-touch-icon" href="http://d14bqw9lzcvxs9.cloudfront.net/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="http://d14bqw9lzcvxs9.cloudfront.net/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="http://d14bqw9lzcvxs9.cloudfront.net/apple-touch-icon-114x114.png">
		
		<?php 
			//INCLUDE FILES 
			include("../version/version.php");
			
			// CONNECT TO DATABASE 
			include("app/assets/config/db.php");
			$count_sql=mysqli_query($db,"select*from count_table where id=1");
		$count=mysqli_fetch_array($count_sql);
			// COUNTER CALCULATIONS 	
			// DEALER COUNT
			$cu_dealer=mysqli_query($db,"SELECT count(*) as total from dealer_dim where ACTIVE='Yes'");
			$count_dealer=mysqli_fetch_array($cu_dealer);
			
			// TODAYS PRINT COUNT
			$dateta=date("Y-m-d");
			$todays=$count['print_today'];

			// TOTAL PRINT COUNT
			// CURRENT VEHICLES
			//$pr_addendum=mysqli_query($db,"SELECT count(*) as total from dealer_inventory where PRINT_FLAG='1'");
			$printed_addendum=$count['printed'];
			// OLD VEHICLES
			//$pr_addendum_old=mysqli_query($db,"SELECT count(*) as total from z_dealer_inventory_inactive where PRINT_FLAG='1'");
			$printed_addendum_old=$count['old_addendum'];
			// TOTAL VENICLES
			$total_prints = number_format($printed_addendum + $printed_addendum_old + 460000);
		?>
		
		<!-- CSS AND FONTS
		================================= -->

		<!-- GOOGLE FONTS -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">

		<!-- LIBRARIES CSS -->
		<link rel="stylesheet" href="http://css.dealeraddendums.com/bootstrap.min.css">
		<link rel="stylesheet" href="/css/font-awesome.min.css">
		<link rel="stylesheet" href="http://css.dealeraddendums.com/animate.min.css">
		<link rel="stylesheet" href="http://css.dealeraddendums.com/style.css">
        <link rel="stylesheet" href="http://css.dealeraddendums.com/bootstrap-notify.css" >

    	<!-- Custom Styles -->
    	<link href="http://css.dealeraddendums.com/styles/alert-bangtidy.css" rel="stylesheet">
    	<link href="http://css.dealeraddendums.com/styles/alert-blackgloss.css" rel="stylesheet">
		<!-- COLORS CSS -->
		<!-- <link id="color-css" rel="stylesheet" href="http://css.dealeraddendums.com/colors/pink.css"> -->
		<!-- <link id="color-css" rel="stylesheet" href="http://css.dealeraddendums.com/colors/red.css"> -->
		<!-- <link id="color-css" rel="stylesheet" href="http://css.dealeraddendums.com/colors/orange.css">-->
		<!-- <link id="color-css" rel="stylesheet" href="http://css.dealeraddendums.com/colors/yellow.css"> -->
		<!-- <link id="color-css" rel="stylesheet" href="http://css.dealeraddendums.com/colors/green.css"> -->
		<!-- <link id="color-css" rel="stylesheet" href="http://css.dealeraddendums.com/colors/turquoise.css"> -->
		<link id="color-css" rel="stylesheet" href="http://css.dealeraddendums.com/colors/blue.css">

		<!-- Intercom Chat -->
		<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/7cda8fc1bfea25032a2ba95d5d862ecb86f8e976';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()
		</script>
		
	</head>

	<body class="enable-animations enable-preloader">
		<div id="document" class="document">

			<!-- HEADER
			================================= -->
			<header id="header" class="header-section section section-dark navbar navbar-fixed-top">

				<div class="container-fluid">

					<div class="navbar-header navbar-left">

						<!-- RESPONSIVE MENU BUTTON -->
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<!-- HEADER LOGO -->
						<a class="navbar-logo navbar-brand anchor-link" href="#hero">
							<img src="http://d14bqw9lzcvxs9.cloudfront.net/header-logo-long.png" srcset="http://d14bqw9lzcvxs9.cloudfront.net/header-logo-long.png" alt="Addendums for your car dealership">
						</a>

					</div>

					<nav id="navigation" class="navigation navbar-collapse collapse navbar-right">
						
						<!-- NAVIGATION LINKS -->
						<ul id="header-nav" class="nav navbar-nav">
							
							<li><a href="#hero" class="hidden">Top</a></li>

							<li><a href="#hero">Benefits</a></li>
							<li><a href="#video">Video</a></li>
							<li><a href="#features">Features</a></li>
							<li><a href="#how-it-works">How it Works</a></li>
							<li><a href="#pricing">Pricing</a></li>
							<li><a href="#maps">Contact</a></li>
							<li><a href="/app" target="_blank">Log In</a></li>
							
							<!-- HEADER ACTION BUTTON -->
							<li class="header-action-button" ><a href="#signup" class="btn btn-primary">Sign Up Now</a></li>

						</ul>

					</nav>

				</div>

			</header>

			<!-- HERO
			================================= -->
			<section id="hero" class="hero-section hero-layout-classic hero-layout-features-and-form section section-dark">

				<div class="section-background">

					<!-- IMAGE BACKGROUND -->
					<div class="section-background-image parallax" data-stellar-ratio="0.4">
						<img src="http://d14bqw9lzcvxs9.cloudfront.net/dealership_clean.jpg" alt="DealerAddendums Inc." style="opacity: 1.2;">
					</div>

					<!-- VIDEO BACKGROUND
					<div class="section-background-video section-background-dot-overlay parallax" data-stellar-ratio="0.4">
						<video preload="auto" autoplay loop muted poster="http://d14bqw9lzcvxs9.cloudfront.net/video-fallback-bg.jpg">
							<source type="video/mp4" src="https://s3.amazonaws.com/addendum-videos/video-bg.mp4">
							<source type="video/ogg" src="https://s3.amazonaws.com/addendum-videos/video-bg.ogv">
							<source type="video/webm" src="https://s3.amazonaws.com/addendum-videos/video-bg.webm">
						</video>
					</div> -->

				</div>

				<div class="container">

					<div class="hero-content">

						<div class="hero-heading row" data-animation="fadeIn">
							<div class="col-md-10 col-md-offset-1">

								<h1 class="hero-title"><img src="http://d14bqw9lzcvxs9.cloudfront.net/horizontal_stacked_website.png" width=600px></h1> 

								<p class="hero-tagline"><orange>Create, Manage and Print Addendums Online.</orange></p>


							</div>
						</div>

						<div class="hero-features row">

							<div class="hero-features-left col-md-7">

								<p class="lead">DealerAddendums.com is an online subscription service for new and used vehicle dealers. The system allows dealers to print <strong><orange>New Vehicle Addendums</orange></strong>, Buyers Guides, and Info Sheets in seconds. </p>
								<ul class="icon-list">
									<li>
										<span class="icon-list-icon fa fa-code" data-animation="bounceIn"></span>
										<h4 class="icon-list-title">Professional</h4>
										<p>Now you can print professional, accurate, and dealer branded addendums in a snap!</p>
									</li>
									<li>
										<span class="icon-list-icon fa fa-cloud" data-animation="bounceIn"></span>
										<h4 class="icon-list-title">Saves Time and Money</h4>
										<p>Print addendums in less than a minute or your entire lot in under five with our affordable month-to-month subscription service!</p>
									</li>
									<li>
										<span class="icon-list-icon fa fa-print" data-animation="bounceIn"></span>
										<h4 class="icon-list-title">Printing Goodness</h4>
										<p>Our revolutionary system prints your customized labels using your existing laser printer and it prints on inexpensive blank labels at just .14 cents per label.</p>
									</li>
								</ul>

							</div>

							<div class="hero-features-right col-md-5" data-animation="fadeIn">

								<h4 class="form-heading">Print Professional Addendums Today!</h4>
									<!-- Gererate random image number -->
							 		<img src="http://d14bqw9lzcvxs9.cloudfront.net/addendums/<?php echo rand(1, 10); ?>.png">	
							</div>

						</div>

					</div>

				</div>

			</section>

			<!-- VIDEO SECTION
			================================= -->
			<section id="video" class="video-section section-white section">

				<div class="container">

					<h2 class="section-heading text-center">Watch our Videos</h2>

					<div class="row">
						

							<div align ="center"> 
								<div class="col-md-6 col-sm-12">
									<div align="center"><h3>Addendum Explainer Video.</h3></div>
									<!-- VIDEO EMBED FROM Explainer JW Player -->
									<script src="//content.jwplatform.com/players/fzyhnJaP-X7R8yf1M.js"></script>
								</div>
								
								<div class="col-md-6 col-sm-12">
									<div align="center"><h3>Print addendums in less than 30 seconds</h3></div>
									<!-- VIDEO EMBED FROM 30 second JW Player -->
									<script src="//content.jwplatform.com/players/I00yDUPC-X7R8yf1M.js"></script>
								</div>
							</div>

						
					</div>

				</div>
			</section>

			<!-- FEATURES
			================================= -->
			<section id="features" class="benefits-section section-gray section">

				<div class="container">

					<h2 class="section-heading text-center">No Contract, Month-to-Month, and Free for 50 Vehicles</h2>

					<div class="benefits-row row">

						<!-- BENEFIT 1 -->
						<div class="col-md-4 col-sm-6">
							<div class="benefit">
								<span class="benefit-icon fa fa-file-code-o" data-animation="bounceIn"></span>
								<h4 class="benefit-title">Vehicle Addendums</h4>
								<p class="benefit-description">Print beautiful, full color or black and white, addendums in seconds for as little as $0.14 a piece. Add your dealer installed options to increase your profits!</p>
							</div>
						</div>

						<!-- BENEFIT 2 -->
						<div class="col-md-4 col-sm-6">
							<div class="benefit">
								<span class="benefit-icon fa fa-cloud" data-animation="bounceIn"></span>
								<h4 class="benefit-title">Web Inventory Integration</h4>
								<p class="benefit-description">The Automatic Web Plan updates your inventory every night with data from your <a href="#" data-toggle="tooltip" data-placement="top" data-original-title="We can get your data from Dealer.com, Homenet, Cobalt, DealerSpecialties, VinSolutions, Arkona, DealerOn, Vauto, DealerTrack, Autouplink, Ecarlist, Autoshot, and more">website or syndication provider</a>. No more manual data entry! </p>
							</div>
						</div>

						<div class="hidden-md hidden-lg clear"></div>

						<!-- BENEFIT 3 -->
						<div class="col-md-4 col-sm-6">
							<div class="benefit">
								<span class="benefit-icon fa fa-cloud" data-animation="bounceIn"></span><span class="benefit-icon fa fa-plus" data-animation="bounceIn"></span>
								<h4 class="benefit-title">DMS Inventory Integration</h4>
								<p class="benefit-description">The Automatic DMS Plan updates your inventory automatically with your DMS data we receive from DealerVault or direct from <a href="http://www.cdkglobal.com/approvedvendorlist/" target ="_blank" data-toggle="tooltip" data-placement="top" data-original-title="We are an approved CDK 3PA vendor. Through CDK your inventory is updated every hour!">CDK.</a> No manual data entry and your inventory shows up FAST!</p>
							</div>
						</div>

						<div class="hidden-sm clear"></div>

						<!-- BENEFIT 4 -->
						<div class="col-md-4 col-sm-6">
							<div class="benefit">
								<span class="benefit-icon fa fa-plus" data-animation="bounceIn"></span>
								<h4 class="benefit-title">Vehicle Options</h4>
								<p class="benefit-description">Apply options to a vehicle addendum based on preset defaults. Options can be manually added or automatically added based on model and/or bodystyle.</p>
							</div>
						</div>

						<div class="hidden-md hidden-lg clear"></div>

						<!-- BENEFIT 5 -->
						<div class="col-md-4 col-sm-6">
							<div class="benefit">
								<span class="benefit-icon fa fa-file-text" data-animation="bounceIn"></span>
								<h4 class="benefit-title">Info Sheets</h4>
								<p class="benefit-description">Easily print Pre-Owned Vehicle Information Sheets, with your logo and vehicle details, description, options, QR code, and optional PureCars integration.</p>
							</div>
						</div>

						<!-- BENEFIT 6 -->
						<div class="col-md-4 col-sm-6">
							<div class="benefit">
								<span class="benefit-icon fa fa-file-text-o" data-animation="bounceIn"></span>
								<h4 class="benefit-title">Buyers Guides</h4>
								<p class="benefit-description">As-Is, Implied, or Warranty Buyers Guides in English and Spanish can be printed in seconds with the click of your mouse for as little as $0.23 a piece.</p>
							</div>
						</div>

					</div>

				</div>

			</section>

			<!-- HOW IT WORKS
			================================= -->
			<section id="how-it-works" class="how-it-works-section section">

				<div class="container-fluid">
					
					<h2 class="section-heading text-center">How it Works</h2>

					<div class="hiw-row row">

						<!-- HOW IT WORKS - ITEM 1 -->
						<div class="col-md-3 col-sm-6" data-animation="fadeIn">
							<div class="hiw-item">
								<img class="hiw-item-picture" src="http://d14bqw9lzcvxs9.cloudfront.net/DA-1.png" alt="">
								<div class="hiw-item-text">
									<span class="hiw-item-icon">1</span>
									<h4 class="hiw-item-title">One time set up</h4>
									<p class="hiw-item-description">Choose your addendum style, upload your logo, add your default options and load your blank labels. </p>
								</div>
							</div>
						</div>

						<!-- HOW IT WORKS - ITEM 2 -->
						<div class="col-md-3 col-sm-6" data-animation="fadeIn">
							<div class="hiw-item even">
								<img class="hiw-item-picture" src="http://d14bqw9lzcvxs9.cloudfront.net/DA-22.png" alt="">
								<div class="hiw-item-text">
									<span class="hiw-item-icon">2</span>
									<h4 class="hiw-item-title">Select Vehicle(s)</h4>
									<p class="hiw-item-description">Choose the vehicle, or vehicles you want to print addendums for. You can manually enter vehicles into the system, or upgrade to Automatic and we'll import them nightly. </p>
								</div>
							</div>
						</div>

						<div class="hidden-md hidden-lg clear"></div>

						<!-- HOW IT WORKS - ITEM 3 -->
						<div class="col-md-3 col-sm-6" data-animation="fadeIn">
							<div class="hiw-item">
								<img class="hiw-item-picture" src="http://d14bqw9lzcvxs9.cloudfront.net/DA-3.png" alt="">
								<div class="hiw-item-text">
									<span class="hiw-item-icon">3</span>
									<h4 class="hiw-item-title">Add options</h4>
									<p class="hiw-item-description">Options are added to vehicles based on the defaults you entered earlier, they can be choosen from a list, or you can enter options manually</p>
								</div>
							</div>
						</div>

						<!-- HOW IT WORKS - ITEM 4 -->
						<div class="col-md-3 col-sm-6" data-animation="fadeIn">
							<div class="hiw-item even">
								<img class="hiw-item-picture" src="http://d14bqw9lzcvxs9.cloudfront.net/DA-4.png" alt="">
								<div class="hiw-item-text">
									<span class="hiw-item-icon">4</span>
									<h4 class="hiw-item-title">Print</h4>
									<p class="hiw-item-description">Put labels in the printer, and hit the big green "Create..." button. It's that easy. Have doubts? Just watch the video above.</p>
								</div>
							</div>
						</div>

					</div>

				</div>

			</section>

			<!-- NUMBERS
			================================= -->
			<section id="numbers" class="numbers-section section-dark section">

				<div class="section-background">

					<!-- IMAGE BACKGROUND -->
					<div class="section-background-image parallax-background" data-stellar-background-ratio="0.4"></div>
					
				</div>

				<div class="container">

					<h2 class="section-heading text-center">Realtime Numbers <a href="/map" target="_blank">(Map)</a></h2>

					<div class="numbers-row row">

						<!-- NUMBERS - ITEM 1 -->
						<div class="col-md-3 col-sm-6">
							<div class="numbers-item">
								<div class="numbers-item-counter"><span class="counter-up"><?php echo $total_prints?></span></div>
								<div class="numbers-item-caption">Addendums Printed</div>
							</div>
						</div>

						<!-- NUMBERS - ITEM 2 -->
						<div class="col-md-3 col-sm-6">
							<div class="numbers-item">
								<div class="numbers-item-counter"><span class="counter-up"><?php echo $todays;?></span></div>
								<div class="numbers-item-caption">Addendums Printed Today</div>
							</div>
						</div>

						<!-- NUMBERS - ITEM 3 -->
						<div class="col-md-3 col-sm-6">
							<div class="numbers-item">
								<div class="numbers-item-counter"><span class="counter-up">33.1</span>Sec.</div>
								<div class="numbers-item-caption">Average Print Time</div>
							</div>
						</div>

						<!-- NUMBERS - ITEM 4 -->
						<div class="col-md-3 col-sm-6">
							<div class="numbers-item">
								<div class="numbers-item-counter"><span class="counter-up"><?php echo $count_dealer['total']; ?></span> <span class="fa fa-coffee"></span></div>
								<div class="numbers-item-caption">Number of Dealers</div>
							</div>
						</div>

					</div>

				</div>

			</section>

			

			
			<!-- PRICING SECTION
			================================= -->
			<section id="pricing" class="pricing-section section">

				<div class="container">

					<h2 class="section-heading text-center">All accounts start out as a 30 Day Free Trial</h2>

					<div class="row text-center">
						<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
							<p>Print 50 labels then choose to upgrade. <br>Never a contract, always month to month.</p>
						</div>
					</div>

					<!-- PRICING TABLE -->
					<div class="pricing-table row">

						<!-- PRICING PACKAGE 1 -->
						<div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3" data-animation="fadeInUp">
							<div class="pricing-package pricing-package-featured">
								<div class="pricing-package-header">
									<h4 class="price-title">Trial/Manual <orange>Load</orange></h4>
									<div class="price">
										<span class="price-currency">$</span>
										<span class="price-number">75</span>

									</div>
									<div class="price-description">Billed monthly</div>
									<div class="price-featured">Manually Load Inventory</div>
								</div>
								<ul class="pricing-package-items">
									<li><strong><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="This plan starts out as the Free trial plan, except that you can print just 50 Addendums, Info Sheets and Buyers Guides">30 day trial gets 50 free prints</a></strong></li>
									<li>Print Addendums</li>
									<li>Print Buyers Guides</li>
									<li>Print QR Stickers</li>
									<li><strong><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="When you sign up for the free trial, we will mail you 50 free balnk labels so you can try out the system.">50 Free Labels</a></strong></li>
									<li>Single Vehicle Import</li>
									<li>Excel Vehicle Import</li>
									<li>Labels as Low as $.14 ea</li>
									<li>4 Hour Email Support</li>
								</ul>
							</div>
						</div>


						<!-- PRICING PACKAGE 3 -->
						<div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3" data-animation="fadeInUp">
							<div class="pricing-package pricing-package-featured">
								<div class="pricing-package-header">
									<h4 class="price-title">Automatic <orange>Web</orange></h4>
									<div class="price">
										<span class="price-currency">$</span>
										<span class="price-number">125</span>

									</div>
									<div class="price-description">Billed monthly</div>
									<div class="price-featured">Automatic Inventory Import</div>
								</div>
								<ul class="pricing-package-items">
									<li><strong><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Call us to discuss special group pricing">Multi-Rooftop Discounts</a></strong></li>
									<li>Print Addendums</li>
									<li>Print Buyers Guides</li>
									<li>Print QR Stickers</li>
									<li><strong><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="We can get your data from Dealer.com, Homenet, Cobalt, DealerSpecialties, VinSolutions, Arkona, DealerOn, Vauto, DealerTrack, Autouplink, Ecarlist, Autoshot, and more">Automatic Inventory Import</a></strong></li>
									<li>Single Vehicle Import</li>
									<li>Excel Vehicle Import</li>
									<li>Labels as Low as $.14 ea</li>
									<li><strong><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="As an Automatic subscriber you have unlimited 24x7 phone, text or email support">24/7 Customer Support</a></strong></li>
								</ul>
							</div>
						</div>
						<!-- PRICING PACKAGE 3 -->
						<div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3" data-animation="fadeInUp">
							<div class="pricing-package pricing-package-featured">
								<div class="pricing-package-header">
									<h4 class="price-title">Automatic <orange>DMS</orange></h4>
									<div class="price">
										<span class="price-currency">$</span>
										<span class="price-number">175</span>

									</div>
									<div class="price-description">Billed monthly</div>
									<div class="price-featured">Sync with RR, CDK</div>
								</div>
								<ul class="pricing-package-items">
									<li>Unlimited Prints</li>
									<li>Print Addendums</li>
									<li>Print Buyers Guides</li>
									<li>Print QR Stickers</li>
									<li><strong><a href="http://www.cdkglobal.com/approvedvendorlist/" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Using DealerVault or CDK's 3PA program to access to your inventory faster and getting it directly from your DMS">Direct DMS Import</a></strong></li>
									<li>Single Vehicle Import</li>
									<li>Excel Vehicle Import</li>
									<li>Labels as Low as $.14 ea</li>
									<li><strong><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="As an Automatic subscriber you have unlimited 24x7 phone, text or email support">24/7 Customer Support</a></strong></li>
								</ul>
							</div>
						</div>

					</div>

				</div>

			</section>

			

						<!-- TESTIMONIAL SLIDER
			================================= -->
			<section id="testimonial" class="testimonial-slider-section section-dark section">

				<div class="section-background">

					<!-- IMAGE BACKGROUND -->
					<div class="section-background-image parallax" data-stellar-ratio="0.4">
						
					</div>

				</div>

				<div class="container">

					<h2 class="section-heading text-center">Hundreds of thousands printed in over 400 dealerships!</h2>

					<div class="testimonial-slider-row row">

						<div class="col-md-8 col-md-offset-2">

							<ul class="testimonial-slider rslides" data-speed="800" data-timeout="4000" data-auto="false">

								<!-- TESTIMONIAL ITEM 1 -->
								<li class="testimonial-slide">
									<span class="testimonial-ratings">
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</span>
									<blockquote class="testimonial-quote">
										"Ok, we’re signed up.  Placed order for labels as well.  Try to hurry those over!  You win, your product rocks..super easy to use and intuitive.  Feel free to cut and paste that to whatever testimonials you have."" <cite class="testimonial-cite">Jason T Falbo, Finnegan DCJR</cite>
									</blockquote>
									
								</li>

								<!-- TESTIMONIAL ITEM 2 -->
								<div class="testimonial-slide">
									<span class="testimonial-ratings">
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</span>
									<blockquote class="testimonial-quote">
										“I’m recommending the site and your tools to everyone.  Printing QR codes and addendums have never been easier.  The  support team is fantastic! <cite class="testimonial-cite">Samone Kopca, Front Range Honda</cite>
									</blockquote>
									
								</div>

								<!-- TESTIMONIAL ITEM 3 -->
								<li class="testimonial-slide">
									<span class="testimonial-ratings">
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</span>
									<blockquote class="testimonial-quote">
										“Dealer Addendums has truly made our addendum process so much easier! The efficiency and quick response from Allan is outstanding! Here at Crown BMW we give Dealer Addendums two thumbs up” <cite class="testimonial-cite">Yazmin Garcia, Crown BMW</cite>
									</blockquote>
									
								</li>

								<!-- TESTIMONIAL ITEM 4 -->
								<div class="testimonial-slide">
									<span class="testimonial-ratings">
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</span>
									<blockquote class="testimonial-quote">
										"Does everything I need it to do. Very easy to set up and manage as vehicles come online." <cite class="testimonial-cite">Paul Brockwell, Lynch Toyota of Auburn</cite>
									</blockquote>
									
								</div>

								<!-- TESTIMONIAL ITEM 5 -->
								<div class="testimonial-slide">
									<span class="testimonial-ratings">
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</span>
									<blockquote class="testimonial-quote">
										DealerAddendums makes addendums quick and easy for my entire inventory. Customer service is outstanding and always helpful" <cite class="testimonial-cite">Ian, Virginia BMW Dealer</cite>
									</blockquote>
									
								</div>

								<!-- TESTIMONIAL ITEM 6 -->
								<div class="testimonial-slide">
									<span class="testimonial-ratings">
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
										<span class="fa fa-star"></span>
									</span>
									<blockquote class="testimonial-quote">
										"We got tired of wrestling with Reynolds' aniquated addendum system, now we have 8 rooftops using DealerAddendums. It couldn't be easier to use, or an easier decision to make!" <cite class="testimonial-cite">Phillip Taylor, Stevenson Auto Group</cite>
									</blockquote>
									
								</div>

							</ul>

						</div>

					</div>

					<div class="sponsors-row" data-animation="bounceIn">
						<img src="http://d14bqw9lzcvxs9.cloudfront.net/groups.png" alt="">
					</div>

				</div>

			</section>

						<!-- GOOGLE MAPS
			================================= -->
			<section id="maps" class="maps-section section">

				<div class="container-fluid maps-row">

					<!-- MAPS IMAGE -->
					<div class="maps-image" data-animation="fadeIn">
						<div id="gmap"></div>
				
						<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
						<!-- GOGGLE MAPS CONFIGURATION -->
						<!-- ref: http://maplacejs.com/ -->
						<script>
						var gmap_options = {
							generate_controls : false,
							locations : [{
								lat : 40.75,
								lon : -111.89,
								animation : google.maps.Animation.DROP,
								html : "World Headquarters",
							}, 
							// {
							// 	lat : -8.71595,
							// 	lon : 115.17398,
							// 	animation : google.maps.Animation.DROP,
							// 	html : "Workshop & Store",
							// }
							],
							map_options : {
								scrollwheel : false,
								mapTypeControl : false,
								streetViewControl : false,
								zoomControlOptions : {
									style : google.maps.ZoomControlStyle.SMALL,
								},
								zoom : 10,
							}
						};
						</script>
					</div>

					<!-- MAPS TEXT -->
					<div class="maps-text" data-animation="fadeIn">

						<div class="maps-text-inner">

							<h2 class="section-heading text-left">Made with style in Salt Lake City, Utah</h2>

							<p>We moved our business to Salt Lake City about 4 years ago from the DC area. When we're not at the office we are skiing, hiking, paddle boarding, or backpacking.</p>
							

							<div class="row">
								<address class="col-sm-6">
									<strong>World Headquarters</strong>
									<ul class="fa-ul">
										<li><i class="fa-li fa fa-home"></i>Salt Lake City<br>Utah, USA</li>
										<li><i class="fa-li fa fa-phone"></i>Phone: +1-801-415-9435</li>
										<li><i class="fa-li fa fa-phone"></i>Free: +1-800-231-7124</li>
										<li><i class="fa-li fa fa-fax"></i>Fax: +1-801-349-2538</li>
										
										<li><i class="fa-li fa fa-envelope-o"></i>Support@DealerAddendums.com</li>
									</ul>										
								</address>
								
							</div>
							
						</div>

					</div>

				</div>

			</section>

			<!-- CLOSING
			================================= -->
			<section id="signup" class="closing-section section-dark section">

				<div class="section-background">

					<!-- IMAGE BACKGROUND -->
					<div class="section-background-image parallax" data-stellar-ratio="0.4">
						<img src="http://d14bqw9lzcvxs9.cloudfront.net/video-fallback-bg.jpg" style="opacity: 0.15;">
					</div>

				</div>
				<div class="container">




					<h3 class="closing-shout">Sign up for a <orange>free account,</orange> then upgrade at any time ($75/Mo, $125/Mo, or $175/Mo)</h3>
					<div class="hero-features row">
					<div class="hero-features-left col-md-3">
						</br></br>
						<p>Just give us your:<br>

						Dealership Info and choose a username and password, you'll be able to immediatly load some vehicles and start printing addendums. We'll even send you 50 blank addendum labels for free! 
						</p><hr><p>
						You can upgrade at any time to print more. Choose an Automatic plan and we'll import your inventory automatically every night.
						</p><hr>
					</div>
					<div class="hero-features-right col-md-9">
						<div class="contact_form">  
							<div id="note1"></div>
								<div id="fields">
									<form id="ajax-contact-form" class="form" role="form">
										<input type="hidden" name="subscription" value="Trial"/>
										<input type="hidden" name="isleadstatusonly" value="yes"/>

										<p class="form_info">
										<hr>
										<div class="form-group">
											<label for="dealer_name">Dealership Information</label>
											<input class="form-control"  type="text" name="dealer_name" value="" placeholder="Dealership Name *"/>
										</div>
										
										<div class="form-inline">
											<!-- <label for="dealer_logo">Franchise Brand</label> -->
											<select name="dealer_logo" class="form-control" >
											  	<option value="" selected >Franchise Brand</option>
											  	<option value="mylogo.jpg">Multiple/Custom</option>
											  	<option value="Abarth.jpg">Abarth</option>
												<option value="Acura.jpg">Acura</option>
												<option value="Alfa-romeo.jpg">Alfa Romeo</option>
												<option value="Aston-marton.jpg">Aston Marton</option>
												<option value="Audi.jpg">Audi</option>
												<option value="Bently.jpg<">Bently</option>
												<option value="BMW.jpg">BMW</option>
												<option value="Buick.jpg">Buick</option>
												<option value="Cadillac.jpg">Cadillac</option>
												<option value="Chevrolet.jpg">Chevrolet</option>
												<option value="Chrysler.jpg">Chrysler</option>
												<option value="Dodge.jpg">Dodge</option>
												<option value="Ferrari.jpg">Ferrari</option>
												<option value="Fiat.jpg">Fiat</option>
												<option value="Fisker.jpg">Fisker</option>
												<option value="Ford.jpg">Ford</option>
												<option value="GMC.jpg">GMC</option>
												<option value="Honda.jpg">Honda</option>
												<option value="Hyundai.jpg">Hyundai</option>
												<option value="Infinity.jpg">Infinity</option>
												<option value="Jaguar.jpg">Jaguar</option> 
												<option value="Jeep.jpg">Jeep</option>
												<option value="Kia.jpg">Kia</option>
												<option value="Lamborghini.jpg">Lamborghini</option>
												<option value="Land-rover.jpg">Land Rover</option>
												<option value="Lexus.jpg">Lexus</option>
												<option value="Lincoln.jpg">Lincoln</option>
												<option value="Lotus.jpg">Lotus</option>
												<option value="Maserati.jpg">Maserati</option>
												<option value="Maybach.jpg">Maybach</option>
												<option value="Mazda.jpg">Mazda</option>
												<option value="Mercedes-benz.jpg">Mercedes Benz</option>
												<option value="Mercury.jpg">Mercury</option>
												<option value="Mini.jpg">Mini</option>
												<option value="Mitsubishi.jpg">Mitsubishi</option>
												<option value="Nissan.jpg">Nissan</option>
												<option value="Pontiac.jpg">Pontiac</option>
												<option value="Porsche.jpg">Porsche</option>
												<option value="Rolls-royce.jpg">Rolls Royce</option>
												<option value="Rover.jpg">Rover</option>
												<option value="Saab.jpg">Saab</option> 
												<option value="Seat.jpg">Seat</option>
												<option value="Smart.jpg">Smart</option>
												<option value="Subaru.jpg">Subaru</option>
												<option value="Suzuki.jpg">Suzuki</option>
												<option value="Tesla.jpg">Tesla</option>
												<option value="Toyota.jpg">Toyota</option>
												<option value="Used.jpg">Used Only</option>
												<option value="Volkswagon.jpg">Volskwagon</option>
												<option value="Volvo">Volvo</option>
											</select> 
											<input class="form-control" type="text" name="dealer_group" value="" placeholder="Dealer Group"/>
											<input class="form-control" type="text" name="dealer_phone" value="" placeholder="Dealership Phone"/>
										</div>
										<div class="form-group">
											<!-- <label for="address">Street *</label> -->
											<input class="form-control" type="text" name="address" value="" placeholder="Address *"/>
										</div>
										<div class="form-inline">
											<!-- <label for="city">City *</label> -->
											<input class="form-control" type="text" name="city" value="" placeholder="City *"/>
									
											<!-- <label for="state">State *</label> -->
											<!-- <input class="form-control" type="text" name="state" value="" placeholder="State *"/> -->

											<select name="state" id="form-location" class="form-control" >
												<option value="" selected >State/Province *</option>
												<option value="AL">Alabama</option>
												<option value="AK">Alaska</option>
												<option value="AZ">Arizona</option>
												<option value="AR">Arkansas</option>
												<option value="CA">California</option>
												<option value="CO">Colorado</option>
												<option value="CT">Connecticut</option>
												<option value="DE">Delaware</option>
												<option value="FL">Florida</option>
												<option value="GA">Georgia</option>
												<option value="HI">Hawaii</option>
												<option value="ID">Idaho</option>
												<option value="IL">Illinois</option>
												<option value="IN">Indiana</option>
												<option value="IA">Iowa</option>
												<option value="KS">Kansas</option>
												<option value="KY">Kentucky</option>
												<option value="LA">Louisiana</option>
												<option value="ME">Maine</option>
												<option value="MD">Maryland</option>
												<option value="MA">Massachusetts</option>
												<option value="MI">Michigan</option>
												<option value="MM">Minnesota</option>
												<option value="MS">Mississippi</option>
												<option value="MO">Missouri</option>
												<option value="MT">Montana</option>
												<option value="NE">Nebraska</option>
												<option value="NV">Nevada</option>
												<option value="NH">New Hampshire</option>
												<option value="NJ">New Jersey</option>
												<option value="NM">New Mexico</option>
												<option value="NY">New York</option>
												<option value="NC">North Carolina</option>
												<option value="ND">North Dakota</option>
												<option value="OH">Ohio</option>
												<option value="OK">Oklahoma</option>
												<option value="OR">Oregon</option>
												<option value="PA">Pennsylvania</option>
												<option value="RI">Rhode Island</option>
												<option value="SC">South Carolina</option>
												<option value="SD">South Dakota</option>
												<option value="TN">Tennessee</option>
												<option value="TX">Texas</option>
												<option value="UT">Utah</option>
												<option value="VT">Vermont</option>
												<option value="VA">Virginia</option>
												<option value="WA">Washington</option>
												<option value="WV">West Virginia</option>
												<option value="WI">Wisconsin</option>
												<option value="WY">Wyoming</option>
												<option value="">***Canada***</option>
												<option value="AB">AB - Alberta</option>
												<option value="BC">BC - British Columbia</option>
												<option value="MB">MB - Manitoba</option>
												<option value="NB">NB - New Brunswick</option>
												<option value="NL">NL - Newfoundland and Labrador</option>
												<option value="NT">NT - Northwest Territories</option>
												<option value="NS">NS - Nova Scotia</option>
												<option value="NU">NU - Nunavut</option>
												<option value="ON">ON - Ontario</option>
												<option value="PE">PE - Prince Edward Island</option>
												<option value="QC">QC - Quebec</option>
												<option value="SK">SK - Saskatchewan</option>
												<option value="YT">YT - Yukon</option>
											</select>
										
											<!-- <label for="zip">Zip *</label> -->
											<input class="form-control" type="text" name="zip" value="" placeholder="Zip *"/>
										</div>
										<div class="form-group">
											<select name="country" id="form-location" class="form-control" >
												<option value=""></option><option value="AF">AFGHANISTAN</option><option value="AX">ALAND ISLANDS</option><option value="AL">ALBANIA</option><option value="DZ">ALGERIA</option><option value="AS">AMERICAN SAMOA</option><option value="AD">ANDORRA</option><option value="AO">ANGOLA</option><option value="AI">ANGUILLA</option><option value="AQ">ANTARCTICA</option><option value="AG">ANTIGUA / BARBUDA</option><option value="AR">ARGENTINA</option><option value="AM">ARMENIA</option><option value="AW">ARUBA</option><option value="AU">AUSTRALIA</option><option value="CX">AUSTRALIA (CHRISTMAS ISLANDS)</option><option value="CC">AUSTRALIA (COCOS KEELING ISLANDS)</option><option value="NF">AUSTRALIA (NORFOLK ISLANDS)</option><option value="AT">AUSTRIA</option><option value="AZ">AZERBAIJAN</option><option value="BS">BAHAMAS</option><option value="BH">BAHRAIN</option><option value="BD">BANGLADESH</option><option value="BB">BARBADOS</option><option value="BY">BELARUS</option><option value="BE">BELGIUM</option><option value="BZ">BELIZE</option><option value="BJ">BENIN</option><option value="BM">BERMUDA</option><option value="BT">BHUTAN</option><option value="BO">BOLIVIA</option><option value="XB">BONAIRE (NETHERLANDS ANTILLES)</option><option value="BA">BOSNIA / HERZEGOVINA</option><option value="BW">BOTSWANA</option><option value="BV">BOUVET ISLAND</option><option value="BR">BRAZIL</option><option value="IO">BRITISH INDIAN OCEAN TERRITORY</option><option value="BN">BRUNEI DARUSSALAM</option><option value="BG">BULGARIA</option><option value="BF">BURKINA FASO</option><option value="BI">BURUNDI</option><option value="KH">CAMBODIA</option><option value="CM">CAMEROON</option><option value="CA">CANADA</option><option value="IC">CANARY ISLANDS</option><option value="CV">CAPE VERDE</option><option value="KY">CAYMAN ISLANDS</option><option value="CF">CENTRAL AFRICAN REPUBLIC</option><option value="TD">CHAD</option><option value="CL">CHILE</option><option value="CN">CHINA</option><option value="CO">COLOMBIA</option><option value="KM">COMOROS</option><option value="CG">CONGO</option><option value="CD">CONGO, DEMOCRATIC REPUBLIC OF THE</option><option value="CK">COOK ISLANDS</option><option value="CR">COSTA RICA</option><option value="HR">CROATIA</option><option value="CU">CUBA</option><option value="XC">CURACAO</option><option value="CY">CYPRUS</option><option value="CZ">CZECH REPUBLIC</option><option value="DK">DENMARK</option><option value="DJ">DJIBOUTI</option><option value="DM">DOMINICA</option><option value="DO">DOMINICAN REPUBLIC</option><option value="TL">EAST TIMOR</option><option value="EC">ECUADOR</option><option value="EG">EGYPT</option><option value="SV">EL SALVADOR</option><option value="ER">ERITREA</option><option value="EE">ESTONIA</option><option value="ET">ETHIOPIA</option><option value="FK">FALKLAND ISLANDS (MALVINAS)</option><option value="FO">FAROE ISLANDS</option><option value="FJ">FIJI</option><option value="WF">FIJI (WALLIS / FUTUNA ISLANDS)</option><option value="FI">FINLAND</option><option value="FR">FRANCE</option><option value="GF">FRENCH GUIANA</option><option value="PF">FRENCH POLYNESIA</option><option value="TF">FRENCH SOUTHERN TERRITORIES</option><option value="GA">GABON</option><option value="GM">GAMBIA</option><option value="GE">GEORGIA</option><option value="DE">GERMANY</option><option value="GH">GHANA</option><option value="GI">GIBRALTAR</option><option value="GR">GREECE</option><option value="GL">GREENLAND</option><option value="GD">GRENADA</option><option value="GP">GUADELOUPE</option><option value="GU">GUAM</option><option value="GT">GUATEMALA</option><option value="GG">GUERNSEY</option><option value="GQ">GUINEA (EQUATORIAL GUINEA)</option><option value="GW">GUINEA BISSAU</option><option value="GN">GUINEA REPUBLIC (GUINEA)</option><option value="GY">GUYANA (BRITISH)</option><option value="HT">HAITI</option><option value="HM">HEARD ISLAND AND MCDONALD ISLANDS</option><option value="HN">HONDURAS</option><option value="HK">HONG KONG</option><option value="HU">HUNGARY</option><option value="IS">ICELAND</option><option value="IN">INDIA</option><option value="ID">INDONESIA</option><option value="IR">IRAN (ISLAMIC REPUBLIC OF)</option><option value="IQ">IRAQ</option><option value="IE">IRELAND, REPUBLIC OF (IRELAND)</option><option value="IL">ISRAEL</option><option value="IT">ITALY</option><option value="SM">ITALY (SAN MARINO)</option><option value="VA">ITALY (VATICAN CITY)</option><option value="CI">IVORY COAST</option><option value="JM">JAMAICA</option><option value="JP">JAPAN</option><option value="JE">JERSEY</option><option value="JO">JORDAN</option><option value="KZ">KAZAKHSTAN</option><option value="KE">KENYA</option><option value="KI">KIRIBATI</option><option value="KR">KOREA, REPUBLIC OF (KOREA SOUTH)</option><option value="KP">KOREA, THE D.P.R. OF (KOREA NORTH)</option><option value="KV">KOSOVO</option><option value="KW">KUWAIT</option><option value="KG">KYRGYZSTAN</option><option value="LA">LAOS</option><option value="LV">LATVIA</option><option value="LB">LEBANON</option><option value="LS">LESOTHO</option><option value="LR">LIBERIA</option><option value="LY">LIBYAN ARAB JAMAHIRIYA</option><option value="LI">LIECHTENSTEIN</option><option value="LT">LITHUANIA</option><option value="LU">LUXEMBOURG</option><option value="MO">MACAO</option><option value="MK">MACEDONIA, REPUBLIC OF (FYROM)</option><option value="MG">MADAGASCAR</option><option value="MW">MALAWI</option><option value="MY">MALAYSIA</option><option value="MV">MALDIVES</option><option value="ML">MALI</option><option value="MT">MALTA</option><option value="MH">MARSHALL ISLANDS</option><option value="MQ">MARTINIQUE</option><option value="MR">MAURITANIA</option><option value="MU">MAURITIUS</option><option value="YT">MAYOTTE</option><option value="MX">MEXICO</option><option value="FM">MICRONESIA, FEDERATED STATES OF</option><option value="MD">MOLDOVA, REPUBLIC OF</option><option value="MC">MONACO</option><option value="MN">MONGOLIA</option><option value="ME">MONTENEGRO</option><option value="MS">MONTSERRAT</option><option value="MA">MOROCCO</option><option value="MZ">MOZAMBIQUE</option><option value="MM">MYANMAR</option><option value="NA">NAMIBIA</option><option value="NR">NAURU, REPUBLIC OF</option><option value="NP">NEPAL</option><option value="NL">NETHERLANDS</option><option value="AN">NETHERLANDS ANTILLES</option><option value="XN">NEVIS</option><option value="NC">NEW CALEDONIA</option><option value="NZ">NEW ZEALAND</option><option value="NI">NICARAGUA</option><option value="NE">NIGER</option><option value="NG">NIGERIA</option><option value="NU">NIUE</option><option value="MP">NORTHERN MARIANA ISLANDS</option><option value="NO">NORWAY</option><option value="OM">OMAN</option><option value="PK">PAKISTAN</option><option value="PW">PALAU</option><option value="PS">PALESTINIAN TERRITORY, OCCUPIED</option><option value="PA">PANAMA</option><option value="PG">PAPUA NEW GUINEA</option><option value="PY">PARAGUAY</option><option value="PE">PERU</option><option value="PH">PHILIPPINES</option><option value="PN">PITCAIRN</option><option value="PL">POLAND</option><option value="PT">PORTUGAL</option><option value="PR">PUERTO RICO</option><option value="QA">QATAR</option><option value="RE">REUNION ISLANDS</option><option value="RO">ROMANIA</option><option value="RU">RUSSIAN FEDERATION (RUSSIA)</option><option value="RW">RWANDA</option><option value="PM">SAINT PIERRE AND MIQUELON</option><option value="WS">SAMOA</option><option value="ST">SAO TOME / PRINCIPE</option><option value="SA">SAUDI ARABIA</option><option value="SN">SENEGAL</option><option value="RS">SERBIA</option><option value="SC">SEYCHELLES</option><option value="SL">SIERRA LEONE</option><option value="SG">SINGAPORE</option><option value="SK">SLOVAKIA</option><option value="SI">SLOVENIA</option><option value="SB">SOLOMON ISLANDS</option><option value="SO">SOMALIA</option><option value="XS">Somaliland, Rep of (North Somalia)</option><option value="ZA">SOUTH AFRICA</option><option value="SH">SOUTH AFRICA (ST HELENA)</option><option value="GS">SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS</option><option value="SS">SOUTH SUDAN</option><option value="ES">SPAIN</option><option value="LK">SRI LANKA</option><option value="XY">ST BARTHELEMY</option><option value="XE">ST EUSTATIUS (NETHERLANDS ANTILLES)</option><option value="KN">ST KITTS</option><option value="LC">ST LUCIA</option><option value="XM">ST MAARTEN (NETHERLANDS ANTILLES)</option><option value="VC">ST VINCENT / GRENADINE</option><option value="SD">SUDAN</option><option value="SR">SURINAME (SURINAM)</option><option value="SJ">SVALBARD AND JAN MAYEN</option><option value="SZ">SWAZILAND</option><option value="SE">SWEDEN</option><option value="CH">SWITZERLAND</option><option value="SY">SYRIAN ARAB REPUBLIC</option><option value="TW">TAIWAN</option><option value="TJ">TAJIKISTAN</option><option value="TZ">TANZANIA, UNITED REPUBLIC OF</option><option value="TH">THAILAND</option><option value="TG">TOGO</option><option value="TK">TOKELAU</option><option value="TO">TONGA</option><option value="TT">TRINIDAD / TOBAGO</option><option value="TN">TUNISIA</option><option value="TR">TURKEY</option><option value="TM">TURKMENISTAN</option><option value="TC">TURKS / CAICOS ISLANDS</option><option value="TV">TUVALU</option><option value="UG">UGANDA</option><option value="UA">UKRAINE</option><option value="AE">UNITED ARAB EMIRATES</option><option value="GB">UNITED KINGDOM</option><option value="US" selected >UNITED STATES</option><option value="UM">UNITED STATES MINOR OUTLYING ISLANDS</option><option value="UY">URUGUAY</option><option value="UZ">UZBEKISTAN</option><option value="VU">VANUATU</option><option value="VE">VENEZUELA</option><option value="VN">VIETNAM</option><option value="VG">VIRGIN ISLANDS (BRITISH)</option><option value="VI">VIRGIN ISLANDS (USA)</option><option value="EH">WESTERN SAHARA</option><option value="YE">YEMEN</option><option value="ZM">ZAMBIA</option><option value="ZW">ZIMBABWE</option>
											</select>

										</div>
										<hr>
										<div class="form-group">
											<label for="name">User Information</label>
											<input class="form-control" type="text" name="name" value="" placeholder="Your Name *"/>
										</div>
										<div class="form-inline">
											<!-- <label for="email">Contact Email *</label> -->
											<input class="form-control" type="text" name="email" value="" placeholder="Your Email *" />
											<!-- <label for="phone">Phone *</label> -->
											<input class="form-control" type="text" name="user_phone" value="" placeholder="Your Phone *"/>
										</div>
										<div class="form-group">
											<div class="form-inline">
												<select name="owner" id="form-location" class="form-control">
													<option value="" selected >Referred By *</option>
													<option value="Google">Google</option>
													<option value="Bing">Bing</option>
													<option value="Other Search">Other Search</option>
													<option value="Email">Email</option>
													<option value="Blog">Blog</option>
													<option value="A friend">A friend</option>
													<option value="Dealer Referral">Dealer Referral</option>
													<option value="1482180260">Derek Lewis</option>
													<option value="7">Justin Bone</option>
													<option value="6">Your AER Rep</option>
												</select>
												<input class="form-control" type="text" name="repname" value="" placeholder="Referal Details"/>
											</div>
										</div>
										<div class="form-inline">
											<label for="username">Username *</label>
											<input class="form-control" type="text" name="username" value=""  />
											<label for="password">Password *</label>
											<input class="form-control" type="text" name="password" value="" placeholder="At least 8 characters"  />
										</div>

										<div class="clear"></div>
										<hr>
										<input type="submit" class="btn  btn-primary btn-form marg-right5" value="Free Sign up" />
										<input type="reset"  class="btn  btn-warning btn-form" value="reset" />
										<!-- Button trigger modal -->
										<button type="button" class="btn btn-default" data-toggle="modal" data-target="#terms">
  											Terms of Use
										</button>
										<button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">
  											Privacy Policy
										</button>
										<div id="note"></div>
										<div class="clear"></div>
									</form>
								</div>
							</div> 
						</div>    
					</div>
				</div>

			</section>
			<!-- PARTNERS
			================================= -->
			<section id="partners" class="benefits-section section-gray section">
				<div class="container">
				<a href="https://www.autosoftdms.com/partners/" target ="_blank" alt="We are an Autosoft DMS partner"><img src="http://d14bqw9lzcvxs9.cloudfront.net/Autosoft_website.png" ></a>
				<img src="http://d14bqw9lzcvxs9.cloudfront.net/spacer.png" >
				<a href="https://aws.amazon.com/" target="_blank"> <img src="http://d14bqw9lzcvxs9.cloudfront.net/AWS.png" > </a>
				<img src="http://d14bqw9lzcvxs9.cloudfront.net/spacer.png" >
				<a href="https://freshbooks.com/" target="_blank"> <img  src="http://d14bqw9lzcvxs9.cloudfront.net/freshbooks.png" > </a>
				<img src="http://d14bqw9lzcvxs9.cloudfront.net/spacer.png" >
				<a href="http://www.cdkglobal.com/approvedvendorlist/" target="_blank"> <img  src="http://d14bqw9lzcvxs9.cloudfront.net/CDK_3PA.png" > </a>
				<img src="http://d14bqw9lzcvxs9.cloudfront.net/spacer.png" >
				<a href="http://www.edmunds.com/?id=apis" target="_blank"> <img  src="http://d14bqw9lzcvxs9.cloudfront.net/Edmunds.png" > </a>
				<img src="http://d14bqw9lzcvxs9.cloudfront.net/spacer.png" >
				<a href="https://stripe.com/" target="_blank"> <img  src="http://d14bqw9lzcvxs9.cloudfront.net/Stripe.png" > </a>
				</div>
			</section>



			<!-- FOOTER
			================================= -->
			<section id="footer" class="footer-section section">

				<div class="container">

					<h3 class="footer-logo">
						<img src="http://d14bqw9lzcvxs9.cloudfront.net/footer-logo.png" srcset="http://d14bqw9lzcvxs9.cloudfront.net/footer-logo@2x.png 2x" alt="Dealer Addendums Inc">
					</h3>

					<div class="footer-socmed">
						<a href="https://www.facebook.com/Addendums" target="_blank"><span class="fa fa-facebook"></span></a>
						<a href="https://twitter.com/dealeraddendums" target="_blank"><span class="fa fa-twitter"></span></a>
					<!-- 	<a href="#" target="_blank"><span class="fa fa-instagram"></span></a>
						<a href="#" target="_blank"><span class="fa fa-pinterest"></span></a> -->
					</div>

					<div class="footer-copyright">
						&copy; 2014 - 2017 DealerAddendums Inc<br>
						** Patent Pending **<br>
						<?php echo RELEASE_VERSION; ?>
					</div>

				</div>

			</section>
		
		</div>

		<!-- Modals - PRIVACY
		================================= -->

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  			<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">DealerAddendums Inc.</h4>
      				</div>
      				<div class="modal-body">
						<h1>Privacy Policy</h1>
						<p>Last updated: June 02, 2015</p>
						<p>DealerAddendums Inc. ("us", "we", or "our") operates the http://dealeraddendums.com website (the "Service").</p>
						<p>This page informs you of our policies regarding the collection, use and disclosure of Personal and Business Information when you use our Service.</p>
						<p>We will not use or share your information with anyone except as described in this Privacy Policy.</p>
						<p><strong>We use your Personal Information for providing and improving the Service. By using the Service, you agree to the collection and use of information in accordance with this policy.</strong> Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms of Use</p>
						<p><strong>Information Collection And Use</strong></p>
						<p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to, your email address, name, phone number, postal address ("Personal Information").</p>
						<p>We collect this information for the purpose of providing the Service, identifying and communicating with you, responding to your requests/inquiries, servicing your purchase orders, and improving our services.</p>
						<p><strong>Log Data</strong></p>
						<p>We collect information that your browser sends whenever you visit our Service ("Log Data"). This Log Data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages and other statistics.</p>
						<p><strong>Cookies</strong></p>
						<p>Cookies are files with a small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and transferred to your device. We use cookies to collect information in order to improve our services for you.</p>
						<p>You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. The Help feature on most browsers provide information on how to accept cookies, disable cookies or to notify you when receiving a new cookie.</p>
						<p>If you do not accept cookies, you may not be able to use some features of our Service and we recommend that you leave them turned on.</p>
						<p><strong>Do Not Track Disclosure</strong></p>
						<p>We support Do Not Track ("DNT"). Do Not Track is a preference you can set in your web browser to inform websites that you do not want to be tracked.</p>
						<p>You can enable or disable Do Not Track by visiting the Preferences or Settings page of your web browser.</p>
						<p><strong>Service Providers</strong></p>
						<p>We may employ third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform Service-related services and/or to assist us in analyzing how our Service is used.</p>
						<p>These third parties have access to your Personal Information only to perform specific tasks on our behalf and are obligated not to disclose or use your information for any other purpose.</p>
						<p><strong>Compliance With Laws</strong></p>
						<p>We will disclose your Personal Information where required to do so by law or subpoena or if we believe that such action is necessary to comply with the law and the reasonable requests of law enforcement or to protect the security or integrity of our Service.</p>
						<p><strong>Security</strong></p>
						<p>The security of your Personal Information is important to us, and we strive to implement and maintain reasonable, commercially acceptable security procedures and practices appropriate to the nature of the information we store, in order to protect it from unauthorized access, destruction, use, modification, or disclosure.</p>
						<p>However, please be aware that no method of transmission over the internet, or method of electronic storage is 100% secure and we are unable to guarantee the absolute security of the Personal Information we have collected from you.</p>
						<p><strong>International Transfer</strong></p>
						<p>Your information, including Personal Information, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>
						<p>If you are located outside United States and choose to provide information to us, please note that we transfer the information, including Personal Information, to United States and process it there.</p>
						<p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>
						<p><strong>Links To Other Sites</strong></p>
						<p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>
						<p>We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>
						<p><strong>Children's Privacy</strong></p>
						<p>Only persons age 18 or older have permission to access our Service. Our Service does not address anyone under the age of 13 ("Children").</p>
						<p>We do not knowingly collect personally identifiable information from children under 13. If you are a parent or guardian and you learn that your Children have provided us with Personal Information, please contact us. If we become aware that we have collected Personal Information from a children under age 13 without verification of parental consent, we take steps to remove that information from our servers.</p>
						<p><strong>Changes To This Privacy Policy</strong></p>
						<p>This Privacy Policy is effective as of June 02, 2015 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page.</p>
						<p>We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Your continued use of the Service after we post any modifications to the Privacy Policy on this page will constitute your acknowledgment of the modifications and your consent to abide and be bound by the modified Privacy Policy.</p>
						<p>If we make any material changes to this Privacy Policy, we will notify you either through the email address you have provided us, or by placing a prominent notice on our website.</p>
						<p><strong>Contact Us</strong></p>
						<p>If you have any questions about this Privacy Policy, please contact us.</p>
      				</div>
      				<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      				</div>
    			</div>
  			</div>
		</div>



		<!-- Modals - Terms
		================================= -->
		<div class="modal fade" id="terms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  			<div class="modal-dialog">
   		 		<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">DealerAddendums Inc.</h4>
      				</div>
      			<div class="modal-body">
					<h1>Terms of Use ("Terms")</h1>
					<p>Last updated: Jume 20, 2017</p>
					<p>
					Please read these Terms of Use ("Terms", "Terms of Use") carefully before using the http://dealeraddendums.com website (the "Service") operated by DealerAddendums Inc. ("us", "we", or "our").<br><br>
					Your access to and use of the Service is conditioned upon your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who wish to access or use the Service.<br><br>
					<strong>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you do not have permission to access the Service.</strong><br><br>
					</p><p>
					<Strong>Accounts</Strong><br>
					All accounts with DealerAddendums Inc are month-to-month and there is no contract, or set up fee.
					<ul>
						<li>Free trial accounts are full featured accounts with manual data import and management and are limited to 50 vehicle prints.</li>
						<li>Manual Accounts are full featured accounts with manual data import and management and unlimited vehicle printing.</li>
						<li>Automatic DMS and Automatic Web accounts have all the Manual account features but add automatic inventory management through DMS or Web integration.</li></ul>
					</p><p>
					Manual and Automatic accounts are recurring subscription accounts (paid) and are billed on the monthly account creation anniversary. Bills are due on receipt, and are late at 37 days. Late accounts are automatically suspended until account is brought current.<br><br>
					Labels ordered through our system are shipped the next business day via USPS Priority Flat Rate and usually arrive in two days. The shipping and any taxes are included in the pricing and the cost of the ordered labels is added to your next invoice.<br><br>
					When you create an account, you guarantee that you are above the age of 18, and that the information you provide us is accurate, complete, and current at all times. Inaccurate, incomplete, or obsolete information may result in the immediate termination of your account on the Service.<br><br>
					You are responsible for maintaining the confidentiality of your account and password, including but not limited to the restriction of access to your computer and/or account. You agree to accept responsibility for any and all activities or actions that occur under your account and/or password, whether your password is with our Service or a third-party Single Sign On service. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.<br><br>
					You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trademark that is subject to any rights of another person or entity other than you, without appropriate authorization. You may not use as a username any name that is offensive, vulgar or obscene.
					We reserve the right to refuse service, terminate accounts, remove or edit content in our sole discretion.
					</p><p>
					<Strong>Communications</Strong><br>

					By creating an Account on our service, you agree to subscribe to newsletters, marketing or promotional materials and other information we may send. However, you may opt out of receiving any, or all, of these communications from us by following the unsubscribe link or instructions provided in any email we send.
					</p><p>
					<Strong>Content</Strong><br>

					Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material ("Content"). You are responsible for the Content that you post on or through the Service, including its legality, reliability, and appropriateness.<br><br>
					By posting Content on or through the Service, You represent and warrant that: (i) the Content is yours (you own it) and/or you have the right to use it and the right to grant us the rights and license as provided in these Terms, and (ii) that the posting of your Content on or through the Service does not violate the privacy rights, publicity rights, copyrights, contract rights or any other rights of any person or entity. We reserve the right to terminate the account of anyone found to be infringing on a copyright.<br><br>
					You retain any and all of your rights to any Content you submit, post or display on or through the Service and you are responsible for protecting those rights. We take no responsibility and assume no liability for Content you or any third party posts on or through the Service. However, by posting Content using the Service you grant us the right and license to use, modify, publicly perform, publicly display, reproduce, and distribute such Content on and through the Service.<br><br>
					DealerAddendums Inc has the right but not the obligation to monitor and edit all Content provided by users.<br><br>
					In addition, Content found on or through this Service are the property of DealerAddendums Inc or used with permission. You may not distribute, modify, transmit, reuse, download, repost, copy, or use said Content, whether in whole or in part, for commercial purposes or for personal gain, without express advance written permission from us.
					</p><p>
					<Strong>Links To Other Web Sites</Strong><br>

					Our Service may utilize third party web sites or services that are not owned or controlled by DealerAddendums Inc.<br><br>
					DealerAddendums Inc has no control over, and assumes no responsibility for the content, privacy policies, or practices of any third party web sites or services. We do not warrant the offerings of any of these entities/individuals or their websites.<br><br>
					You acknowledge and agree that DealerAddendums Inc shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such third party web sites or services.<br><br>
					We strongly advise you to read the terms and conditions and privacy policies of any third party web sites or services that you visit.
					</p><p>
					<Strong>Termination</Strong><br><br>

					<strong>DealerAddendums does NOT prorate the current invoice for terminated accounts.</strong><br><br>
					If you wish to terminate your account, you may simply log in and convert your account to the "Free" plan and pay any outstanding invoices. You can also send an email with your termination request to allan@dealeraddendums.com. <br><br>
					We may terminate or suspend your account and bar access to the Service immediately, without prior notice or liability, under our sole discretion, for any reason whatsoever and without limitation, including but not limited to a breach of the Terms.<br><br>
					All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.
					</p><p>
					<Strong>Indemnification</Strong><br>

					You agree to defend, indemnify and hold harmless DealerAddendums Inc and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees), resulting from or arising out of a) your use and access of the Service, by you or any person using your account and password; b) a breach of these Terms, or c) Content posted on the Service.
					</p><p>
					<strong>Limitation Of Liability</strong><br>

					In no event shall DealerAddendums Inc, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.
					</p><p>
					<Strong>Disclaimer</Strong><br>

					Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and "AS AVAILABLE" basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance.<br><br>
					DealerAddendums Inc its subsidiaries, affiliates, and its licensors do not warrant that a) the Service will function uninterrupted, secure or available at any particular time or location; b) any errors or defects will be corrected; c) the Service is free of viruses or other harmful components; or d) the results of using the Service will meet your requirements.
					</p><p>
					<Strong>Exclusions</Strong><br>

					Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or limitation of liability for consequential or incidental damages, so the limitations above may not apply to you.
					</p><p>
					<Strong>Governing Law </Strong><br>

					These Terms shall be governed and construed in accordance with the laws of Utah, United States, without regard to its conflict of law provisions.<br><br>
					Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have had between us regarding the Service.
					</p><p>
					</Strong>Changes</Strong><br>

					We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material, we will provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.<br><br>
					By continuing to access or use our Service after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Service.
					</p><p>
					</Strong>Contact Us</strong><br>

					If you have any questions about these Terms, please contact us.
					</p>
      			</div>
      			<div class="modal-footer">
                <input type="hidden" id="addendums" value="" />
                <input type="hidden" value="" id="last_data" />
                <input type="hidden" value="" id="new_link" />
        			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      			</div>
    		</div>
  		</div>
	</div>
<div class='notifications bottom-right'></div>


		<!-- JAVASCRIPT
		================================= -->
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/respimage.min.js"></script>
		<script src="js/jpreloader.min.js"></script>
		<script src="js/smoothscroll.min.js"></script>
		<script src="js/jquery.nav.min.js"></script>
		<script src="js/jquery.inview.min.js"></script>
		<script src="js/jquery.counterup.min.js"></script>
		<script src="js/jquery.stellar.min.js"></script>
		<script src="js/maplace-0.1.3.min.js"></script>
		<script src="js/responsiveslides.min.js"></script>
		<script src="js/script.js"></script>
        <script src="js/bootstrap-notify.js"></script>


		<!-- Order processing
		================================= -->
		<script type="text/javascript">
			$(document).ready(function(){	
				$("#ajax-contact-form").submit(function() {
					var str = $(this).serialize();		
					$.ajax({
						type: "POST",
						url: "contact_form/order_process.php",
						data: str,
						success: function(msg) {
							result = '';
							result1 = '';
							// Message Sent - Show the 'Thank You' message and hide the form
							if(msg == 'OK') {
								result1 = '<div class="notification_ok"><h3>Your Free account is now set up and you should receive a confirmation email in a minute or two. <br><br>Thank you!<br>Allan Tone - Founder & CEO</h3></div>';
								$("#fields").hide();
							} else {
								result = msg;
							}
							$('#note').html(result);
							$('#note1').html(result1);

						}
					});
					return false;
				});															
			});
		//]]>		
		</script>

		
		<!-- Open Modal
		================================= -->
		<script>
			$('#myModal').on('shown.bs.modal', function () {
			  	$('#myInput').focus()
			})
		</script>


		<!-- Intercom
		================================= -->
		<script>
			window.Intercom("boot", {
  				app_id: "7cda8fc1bfea25032a2ba95d5d862ecb86f8e976"
			});
			window.Intercom("update");
		</script>
		
		<!-- Google
		================================= -->
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-46873878-1', 'auto');
		  ga('send', 'pageview');
		</script>

		<!-- Print Notification
		================================= -->
		<script>
			setInterval(function(){
				$.ajax({
		    		url: "map/get_values.php",
		    		type: "get",
					dataType: 'json',
					cache:false,
		      		success: function(html){
						if(html.addendums!=$("#addendums").val()){
							$("#addendums").val(html.addendums);
							$.ajax({
		    					url: "map/get_last_home.php",
		    					type: "get",
								dataType: 'json',
								cache:false,
		      					success: function(myresponse){
									if(myresponse.id!=$("#last_data").val()){
										var text1=myresponse.dealer+' just printed an addendum. <br>Click to see the addendum they printed for a:<br> <a href="app/assets/print/single-download.php?vid='+myresponse.id+'&printstart=1&viewpdf=yes" target="new"><strong>'+myresponse.make+' '+myresponse.model+'</strong></a>';
										$('.bottom-right').notify({
											message: { html: text1 },
											fadeOut: { enabled: true, delay: 5000 },
											type:'blackgloss'
										}).show();
										$("#new_link").val("app/assets/print/single-download.php?vid="+myresponse.id+"&printstart=1&viewpdf=yes");
										$("#last_data").val(myresponse.id);						
									}
								}
							});
						}
					}
				});
			}, 10000);
			$( ".bottom-right" ).click(function() {
					window.open($("#new_link").val(), '_new');
			});
		</script>


	</body>

</html>