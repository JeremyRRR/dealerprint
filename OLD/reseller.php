<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		
		<!-- BASIC INFO -->
		<title>Addendum System for Car Dealers | Resellers Portal | DealerAddendums Inc. </title>
		<META NAME="description" CONTENT="New vehicle addendum, Infosheet and Buyers Guide printing made easy by DealerAddendums.com">
		<META NAME="keywords" CONTENT="dealer addendums, automotive addendum, auto dealer, car dealership, new vehicle, window stickers, addendum labels, FTC Buyers guides, spaniah buyers guides, VIN Decoder">
		<META NAME="robot" CONTENT="index,follow">
		<META NAME="copyright" CONTENT="Copyright © 2017 DealerAddendums.com. All Rights Reserved.">
		<META NAME="author" CONTENT="Allan Tone">
		<META NAME="language" CONTENT="English">
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0" />

		<!-- FAVICONS -->
		<link rel="icon" href="http://d14bqw9lzcvxs9.cloudfront.net/favicon.ico">
		<link rel="apple-touch-icon" href="http://d14bqw9lzcvxs9.cloudfront.net/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="http://d14bqw9lzcvxs9.cloudfront.net/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="http://d14bqw9lzcvxs9.cloudfront.net/apple-touch-icon-114x114.png">

		
		<?php 
		//INCLUDE FILES 
		// include("config/config.php"); 
		include("../version/version.php");
		

		// CONNECT TO DATABASE 
		include("app/assets/config/db.php");
		
		// COUNTER CALCULATIONS 	
		// DEALER COUNT
		$cu_dealer=mysqli_query($db,"SELECT count(*) as total from dealer_dim where ACTIVE='Yes'");
		$count_dealer=mysqli_fetch_array($cu_dealer);
		
		// TODAYS PRINT COUNT
		$dateta=date("Y-m-d");
		$todays_add=mysqli_query($db,"SELECT count(*) as total from dealer_inventory where  PRINT_DATE='".$dateta."'  and STATUS='1'");
		$todays=mysqli_fetch_array($todays_add);

		// TOTAL PRINT COUNT
		// CURRENT VEHICLES
		$pr_addendum=mysqli_query($db,"SELECT count(*) as total from dealer_inventory where PRINT_STATUS='1' or PRINT_INFO='1' or PRINT_GUIDE='1'");
		$printed_addendum=mysqli_fetch_array($pr_addendum);
		// OLD VEHICLES
		$pr_addendum_old=mysqli_query($db,"SELECT count(*) as total from z_dealer_inventory_inactive where PRINT_STATUS='1' or PRINT_INFO='1' or PRINT_GUIDE='1'");
		$printed_addendum_old=mysqli_fetch_array($pr_addendum_old);

		$total_prints = $printed_addendum['total'] + $printed_addendum_old['total'] + 160000;	
		?>
		
		<!-- CSS
		================================= -->

		<!-- GOOGLE FONTS -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800">

		<!-- LIBRARIES CSS -->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/animate.min.css">

		<!-- JQUERY LIBRARY -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

		<!-- SPECIFIC CSS -->
		<link rel="stylesheet" href="css/style.css">


		<!-- JS FOR VIDEO -->
		<script src="tutorial/media/scripts/embedded-smart-player.min.js"></script>

		<!-- CSS FOR VIDEO -->
		<style>
			.smart-player-embed-container-iframe{
				width:960px;height:587px}.smart-player-full-frame-mode{position:absolute;top:0;left:0;z-index:10;width:100%;height:100%;padding:0;margin:0
			}
		orange {
    		color: orange;
		}
		.placeholder{
			 color: #aaa;
		}

		.sponsors-row{
			background-color: #fff;
		}
		</style>

		<!-- COLORS -->
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/pink.css"> -->
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/red.css"> -->
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/orange.css">-->
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/yellow.css"> -->
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/green.css"> -->
		<!-- <link id="color-css" rel="stylesheet" href="css/colors/turquoise.css"> -->
		<link id="color-css" rel="stylesheet" href="css/colors/blue.css">


		<script type="text/javascript">
			$(document).ready(function(){
    			$('[data-toggle="tooltip"]').tooltip({
        			placement : 'top'
    			});
			});
		</script>
		
	</head>

	<body class="enable-animations enable-preloader">

		<div id="document" class="document">

			<!-- HEADER
			================================= -->
			<header id="header" class="header-section section section-dark navbar navbar-fixed-top">

				<div class="container-fluid">

					<div class="navbar-header navbar-left">

						<!-- RESPONSIVE MENU BUTTON -->
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<!-- HEADER LOGO -->
						<a class="navbar-logo navbar-brand anchor-link" href="#hero">
							<img src="http://d14bqw9lzcvxs9.cloudfront.net/header-logo-long.png" srcset="http://d14bqw9lzcvxs9.cloudfront.net/header-logo-long.png" alt="Addendums for your car dealership">
						</a>

					</div>

					<nav id="navigation" class="navigation navbar-collapse collapse navbar-right">
						
						<!-- NAVIGATION LINKS -->
						<ul id="header-nav" class="nav navbar-nav">
							
							<li><a href="#hero" class="hidden">Top</a></li>
							<li><a href="#hero">About</a></li>
							<li><a href="#benefits">Reseller Benefits</a></li>
							<li><a href="#video">Watch Video</a></li>
							<li><a href="#pricing">Pricing</a></li>
							<li><a href="#maps">Contact Us</a></li>
							<li><a href="/app/reseller" target="_blank">Log In</a></li>
							
							<!-- HEADER ACTION BUTTON -->
							<li class="header-action-button" ><a href="#signup" class="btn btn-primary">Become a Reseller</a></li>

						</ul>

					</nav>

				</div>

			</header>

			<!-- HERO
			================================= -->
			<section id="hero" class="hero-section hero-layout-classic hero-layout-features-and-form section section-dark">

				<div class="section-background">

					<!-- IMAGE BACKGROUND 
					<div class="section-background-image parallax-background" data-stellar-background-ratio="0.4"></div>-->

					<!-- VIDEO BACKGROUND -->
					<div class="section-background-video section-background-dot-overlay parallax" data-stellar-ratio="0.4">
						<video preload="auto" autoplay loop muted poster="http://d14bqw9lzcvxs9.cloudfront.net/video-fallback-bg.jpg">
							<source type="video/mp4" src="https://s3.amazonaws.com/addendum-videos/video-bg.mp4">
							<source type="video/ogg" src="https://s3.amazonaws.com/addendum-videos/video-bg.ogv">
							<source type="video/webm" src="https://s3.amazonaws.com/addendum-videos/video-bg.webm">
						</video>
					</div>

				</div>

				<div class="container">

					<div class="hero-content">

						<div class="hero-heading row" data-animation="fadeIn">
							<div class="col-md-10 col-md-offset-1">

								<h1 class="hero-title"><img src="https://s3.amazonaws.com/addendum-websiteimages/horizontal_stacked_AS.png" width=600px></h1> 

								<p class="hero-tagline"><orange>Resellers Portal</orange></p>


							</div>
						</div>

						<div class="hero-features row">

							<div class="hero-features-left col-md-7">

								<p class="lead">DealerAddendums.com is an online subscription service for new and used vehicle dealers. The system allows dealers to print both New Vehicle Addendums and Used Vehicle Buyers Guides in seconds. </p>
								<ul class="icon-list">
									<li>
										<span class="icon-list-icon fa fa-code" data-animation="bounceIn"></span>
										<h4 class="icon-list-title">Software as a Service (SaaS)</h4>
										<p>With no software to install our system can be used from any Internet connected computer. Just log in, choose a vehicle, label type and any options. It’s that easy!</p>
									</li>
									<li>
										<span class="icon-list-icon fa fa-cloud" data-animation="bounceIn"></span>
										<h4 class="icon-list-title">Inventory Import</h4>
										<p>Upgrade to the Automatic Web or Automatic DMS plan and we'll get your inventory direct from your <a href="#" data-toggle="tooltip" data-placement="top" data-original-title="We can get your data from your web solutions provider like Dealer.com, Homenet, VinSolutions, etc. or we can use DealerVault to get your inventory directly from your DMS like Reynolds, CDK, etc.">website provider or DMS </a>, never type a stock or VIN number again.</p>
									</li>
									<li>
										<span class="icon-list-icon fa fa-print" data-animation="bounceIn"></span>
										<h4 class="icon-list-title">Printing Goodness</h4>
										<p>The system prints using your existing laser printer, and it prints on inexpensive blank labels.</p>
									</li>
								</ul>

							</div>

							<div class="hero-features-right col-md-5" data-animation="fadeIn">

								<h4 class="form-heading">Print Professional Addendums Today!</h4>

								<img src="http://d14bqw9lzcvxs9.cloudfront.net/addendums.png" alt="Addendum templates">								
							
							</div>

						</div>

					</div>

				</div>

			</section>


			<!-- BENEFITS
			================================= -->
			<section id="benefits" class="benefits-section section-gray section">

				<div class="container">

					<h2 class="section-heading text-center">No Contract, Month-to-Month, and Free for 50 Vehicles</h2>

					<div class="benefits-row row">

						<!-- BENEFIT 1 -->
						<div class="col-md-4 col-sm-6">
							<div class="benefit">
								<span class="benefit-icon fa fa-file-code-o" data-animation="bounceIn"></span>
								<h4 class="benefit-title">Vehicle Addendums</h4>
								<p class="benefit-description">Print beautiful, full color or black and white, addendums in seconds for as little as $0.14 a piece. Add your dealer installed options to increase your profits!</p>
							</div>
						</div>

						<!-- BENEFIT 2 -->
						<div class="col-md-4 col-sm-6">
							<div class="benefit">
								<span class="benefit-icon fa fa-cloud" data-animation="bounceIn"></span>
								<h4 class="benefit-title">Web Inventory Integration</h4>
								<p class="benefit-description">The Automatic Web Plan updates your inventory every night with data from your <a href="#" data-toggle="tooltip" data-placement="top" data-original-title="We can get your data from Dealer.com, Homenet, Cobalt, DealerSpecialties, VinSolutions, Arkona, DealerOn, Vauto, DealerTrack, Autouplink, Ecarlist, Autoshot, and more">website or syndication provider</a>. No more manual data entry! </p>
							</div>
						</div>

						<div class="hidden-md hidden-lg clear"></div>

						<!-- BENEFIT 3 -->
						<div class="col-md-4 col-sm-6">
							<div class="benefit">
								<a href="http://www.dealervault.com?campaign=DealerAddendumsWebsite#7" target="_blank"><img src="http://d14bqw9lzcvxs9.cloudfront.net/DealerVault2.png" width = "175px"></a>
								<!-- <span class="benefit-icon fa fa-cloud" data-animation="bounceIn"></span> -->
								<h4 class="benefit-title">DMS Inventory Integration</h4>
								<p class="benefit-description">The Automatic DMS Plan updates your inventory automatically with your DMS data we receive from <a href="http://www.dealervault.com?campaign=DealerAddendumsWebsite#7" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="DealerVault will connect to your DMS to easily update DealerAddendum’s Automatic DMS plan with the current vehicles in your DMS inventory. DealerVault is a free service for dealers that provides dealerships with a solution to all pressing issues including security, compliance, and the indemnification of a $20 million cyber liability policy. ">DealerVault</a>. No manual data entry and your inventory shows up FAST!</p>
							</div>
						</div>

						<div class="hidden-sm clear"></div>

						<!-- BENEFIT 4 -->
						<div class="col-md-4 col-sm-6">
							<div class="benefit">
								<span class="benefit-icon fa fa-plus" data-animation="bounceIn"></span>
								<h4 class="benefit-title">Vehicle Options</h4>
								<p class="benefit-description">Apply options to a vehicle addendum based on preset defaults. Options can be manually added or automatically added based on model and/or bodystyle.</p>
							</div>
						</div>

						<div class="hidden-md hidden-lg clear"></div>

						<!-- BENEFIT 5 -->
						<div class="col-md-4 col-sm-6">
							<div class="benefit">
								<span class="benefit-icon fa fa-file-text" data-animation="bounceIn"></span>
								<h4 class="benefit-title">Info Sheets</h4>
								<p class="benefit-description">Easily print Pre-Owned Vehicle Information Sheets, with your logo and vehicle details, description, options, QR code. and optional PureCars integration.</p>
							</div>
						</div>

						<!-- BENEFIT 6 -->
						<div class="col-md-4 col-sm-6">
							<div class="benefit">
								<span class="benefit-icon fa fa-file-text-o" data-animation="bounceIn"></span>
								<h4 class="benefit-title">Buyers Guides</h4>
								<p class="benefit-description">As-Is, Implied, or Warranty Buyers Guides in English and Spanish can be printed in seconds with the click of your mouse for as little as $0.23 a piece.</p>
							</div>
						</div>

					</div>

				</div>

			</section>


			

			<!-- VIDEO SECTION
			================================= -->
			<section id="video" class="video-section section-gray section">

				<div class="container">

					<h2 class="section-heading text-center">Watch the Video</h2>

					<div class="row">
						<div class="col-md-10 col-md-offset-1">

							<div class="video-embed">

								<!-- VIDEO EMBED FROM VIMEO -->
								<iframe class="video-async" data-source="vimeo" data-video="183924159" data-color="f3ae73" allowfullscreen></iframe>
								
								<!-- VIDEO EMBED FROM YOUTUBE 
								<iframe class="video-async" data-source="youtube" data-video="pfOqP-i37hM" allowfullscreen></iframe>-->
								
								<!-- VIDEO FROM SCREENCAST 
								<iframe class="smart-player-embed-container-iframe" id="embeddedSmartPlayerInstance" src="tutorial/media/index_player.html?embedIFrameId=embeddedSmartPlayerInstance" scrolling="no"  frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>-->
							</div>

						</div>
					</div>

				</div>
			</section>

			
			<!-- PRICING SECTION
			================================= -->
			<section id="pricing" class="pricing-section section">

				<div class="container">

					<h2 class="section-heading text-center">All accounts start out as a 30 Day Free Trial</h2>

					<div class="row text-center">
						<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
							<p>Print 50 labels then choose to upgrade. <br>Never a contract, always month to month.</p>
						</div>
					</div>

					<!-- PRICING TABLE -->
					<div class="pricing-table row">

						<!-- PRICING PACKAGE 1 -->
						<div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3" data-animation="fadeInUp">
							<div class="pricing-package pricing-package-featured">
								<div class="pricing-package-header">
									<h4 class="price-title">Trial/Manual <orange>Load</orange></h4>
									<div class="price">
										<span class="price-currency">$</span>
										<span class="price-number">75</span>

									</div>
									<div class="price-description">Billed monthly</div>
									<div class="price-featured">Manually Load Inventory</div>
								</div>
								<ul class="pricing-package-items">
									<li><strong><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="This plan starts out as the Free trial plan, except that you can print just 50 Addendums, Info Sheets and Buyers Guides">30 day trial gets 50 free prints</a></strong></li>
									<li>Print Addendums</li>
									<li>Print Buyers Guides</li>
									<li>Print QR Stickers</li>
									<li><strong><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="When you sign up for the free trial, we will mail you 50 free balnk labels so you can try out the system.">50 Free Labels</a></strong></li>
									<li>Single Vehicle Import</li>
									<li>Excel Vehicle Import</li>
									<li>Labels as Low as $.14 ea</li>
									<li>4 Hour Email Support</li>
								</ul>
							</div>
						</div>


						<!-- PRICING PACKAGE 3 -->
						<div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3" data-animation="fadeInUp">
							<div class="pricing-package pricing-package-featured">
								<div class="pricing-package-header">
									<h4 class="price-title">Automatic <orange>Web</orange></h4>
									<div class="price">
										<span class="price-currency">$</span>
										<span class="price-number">125</span>

									</div>
									<div class="price-description">Billed monthly</div>
									<div class="price-featured">Automatic Inventory Import</div>
								</div>
								<ul class="pricing-package-items">
									<li><strong><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Call us to discuss special group pricing">Multi-Rooftop Discounts</a></strong></li>
									<li>Print Addendums</li>
									<li>Print Buyers Guides</li>
									<li>Print QR Stickers</li>
									<li><strong><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="We can get your data from Dealer.com, Homenet, Cobalt, DealerSpecialties, VinSolutions, Arkona, DealerOn, Vauto, DealerTrack, Autouplink, Ecarlist, Autoshot, and more">Automatic Inventory Import</a></strong></li>
									<li>Single Vehicle Import</li>
									<li>Excel Vehicle Import</li>
									<li>Labels as Low as $.14 ea</li>
									<li><strong><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="As an Automatic subscriber you have unlimited 24x7 phone, text or email support">24/7 Customer Support</a></strong></li>
								</ul>
							</div>
						</div>
						<!-- PRICING PACKAGE 3 -->
						<div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3" data-animation="fadeInUp">
							<div class="pricing-package pricing-package-featured">
								<div class="pricing-package-header">
									<h4 class="price-title">Automatic <orange>DMS</orange></h4>
									<div class="price">
										<span class="price-currency">$</span>
										<span class="price-number">175</span>

									</div>
									<div class="price-description">Billed monthly</div>
									<div class="price-featured">Synch with RR, ADP</div>
								</div>
								<ul class="pricing-package-items">
									<li>Unlimited Prints</li>
									<li>Print Addendums</li>
									<li>Print Buyers Guides</li>
									<li>Print QR Stickers</li>
									<li><strong><a href="http://www.dealervault.com?campaign=DealerAddendumsWebsite#7" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Using DealerVault to access to your inventory faster and getting it directly from your DMS">Direct DMS Import</a></strong></li>
									<li>Single Vehicle Import</li>
									<li>Excel Vehicle Import</li>
									<li>Labels as Low as $.14 ea</li>
									<li><strong><a href="#" data-toggle="tooltip" data-placement="top" data-original-title="As an Automatic subscriber you have unlimited 24x7 phone, text or email support">24/7 Customer Support</a></strong></li>
								</ul>
							</div>
						</div>

					</div>

				</div>

			</section>

			<!-- NUMBERS
			================================= -->
			<section id="numbers" class="numbers-section section-dark section">

				<div class="section-background">

					<!-- IMAGE BACKGROUND -->
					<div class="section-background-image parallax-background" data-stellar-background-ratio="0.4"></div>

				</div>

				<div class="container">

					<h2 class="section-heading text-center">Realtime Numbers <a href="/map" target="_blank">(Map)</a></h2>

					<div class="numbers-row row">

						<!-- NUMBERS - ITEM 1 -->
						<div class="col-md-3 col-sm-6">
							<div class="numbers-item">
								<div class="numbers-item-counter"><span class="counter-up"><?php echo $total_prints?></span></div>
								<div class="numbers-item-caption">Addendums Printed</div>
							</div>
						</div>

						<!-- NUMBERS - ITEM 2 -->
						<div class="col-md-3 col-sm-6">
							<div class="numbers-item">
								<div class="numbers-item-counter"><span class="counter-up"><?php echo $todays['total'];?></span></div>
								<div class="numbers-item-caption">Addendums Printed Today</div>
							</div>
						</div>

						<!-- NUMBERS - ITEM 3 -->
						<div class="col-md-3 col-sm-6">
							<div class="numbers-item">
								<div class="numbers-item-counter"><span class="counter-up">33.1</span>Sec.</div>
								<div class="numbers-item-caption">Average Print Time</div>
							</div>
						</div>

						<!-- NUMBERS - ITEM 4 -->
						<div class="col-md-3 col-sm-6">
							<div class="numbers-item">
								<div class="numbers-item-counter"><span class="counter-up"><?php echo $count_dealer['total']; ?></span> <span class="fa fa-coffee"></span></div>
								<div class="numbers-item-caption">Number of Dealers</div>
							</div>
						</div>

					</div>

				</div>

			</section>

			

						<!-- GOOGLE MAPS
			================================= -->
			<section id="maps" class="maps-section section">

				<div class="container-fluid maps-row">

					<!-- MAPS IMAGE -->
					<div class="maps-image" data-animation="fadeIn">
						<div id="gmap"></div>
				
						<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
						<!-- GOGGLE MAPS CONFIGURATION -->
						<!-- ref: http://maplacejs.com/ -->
						<script>
						var gmap_options = {
							generate_controls : false,
							locations : [{
								lat : 40.75,
								lon : -111.89,
								animation : google.maps.Animation.DROP,
								html : "World Headquarters",
							}, 
							// {
							// 	lat : -8.71595,
							// 	lon : 115.17398,
							// 	animation : google.maps.Animation.DROP,
							// 	html : "Workshop & Store",
							// }
							],
							map_options : {
								scrollwheel : false,
								mapTypeControl : false,
								streetViewControl : false,
								zoomControlOptions : {
									style : google.maps.ZoomControlStyle.SMALL,
								},
								zoom : 10,
							}
						};
						</script>
					</div>

					<!-- MAPS TEXT -->
					<div class="maps-text" data-animation="fadeIn">

						<div class="maps-text-inner">

							<h2 class="section-heading text-left">Made with style in Salt Lake City, Utah</h2>

							<p>We moved our business to Salt Lake City about 4 years ago from the DC area. When we're not at the office we are skiing, hiking, paddle boarding, or backpacking.</p>
							

							<div class="row">
								<address class="col-sm-6">
									<strong>World Headquarters</strong>
									<ul class="fa-ul">
										<li><i class="fa-li fa fa-home"></i>Salt Lake City<br>Utah, USA</li>
										<li><i class="fa-li fa fa-phone"></i>Phone: +1-801-415-9435</li>
										<li><i class="fa-li fa fa-phone"></i>Free: +1-800-231-7124</li>
										<li><i class="fa-li fa fa-fax"></i>Fax: +1-801-349-2538</li>
										
										<li><i class="fa-li fa fa-envelope-o"></i>Support@DealerAddendums.com</li>
									</ul>										
								</address>
								
							</div>
							
						</div>

					</div>

				</div>

			</section>

			<!-- CLOSING
			================================= -->
			<section id="signup" class="closing-section section-dark section">

				<div class="section-background">

					<!-- IMAGE BACKGROUND -->
					<div class="section-background-image parallax-background" data-stellar-background-ratio="0.4"></div>

				</div>
				<div class="container">




					<h3 class="closing-shout">Sign up for a <orange>free resellers account,</orange> and receive great finders fees and commissions</h3>
					<div class="hero-features row">
					<div class="hero-features-left col-md-3">
						</br></br>
						<p>Just give us your:<br>

						Your info and choose a username and password, you'll be able to immediatly get credit for any dealership who signs up and lists you as their Rep.
						</p><hr><p>
						Commissions are paid quarterly directly to you. Once you sign up, you can download the commission schedule</a>						
						</p><hr>
					</div>
					<div class="hero-features-right col-md-9">
						<div class="contact_form">  
							<div id="note1"></div>
								<div id="fields">
									<form id="ajax-contact-form" class="form" role="form">
										<input type="hidden" name="owner" value="342453425"/>
										<input type="hidden" name="subscription" value="Trial"/>
										<input type="hidden" name="isleadstatusonly" value="yes"/>

										<p class="form_info">
										<hr>
										<div class="form-group">
											<label for="dealer_name">Your Information</label>
											<input class="form-control"  type="text" name="reseller_name" value="" placeholder="Your Name *"/>
										</div>
										
									
										<div class="form-group">
											<!-- <label for="address">Street *</label> -->
											<input class="form-control" type="text" name="reseller_address" value="" placeholder="Address *"/>
										</div>
										<div class="form-inline">
											<!-- <label for="city">City *</label> -->
											<input class="form-control" type="text" name="reseller_city" value="" placeholder="City *"/>
									
											<!-- <label for="state">State *</label> -->
											<!-- <input class="form-control" type="text" name="state" value="" placeholder="State *"/> -->

											<select name="reseller_state" id="form-location" class="form-control" placeholder="Your Location" autocomplete="off">
												<option value="" selected >State *</option>
												<option value="AL">Alabama</option>
												<option value="AK">Alaska</option>
												<option value="AZ">Arizona</option>
												<option value="AR">Arkansas</option>
												<option value="CA">California</option>
												<option value="CO">Colorado</option>
												<option value="CT">Connecticut</option>
												<option value="DE">Delaware</option>
												<option value="FL">Florida</option>
												<option value="GA">Georgia</option>
												<option value="HI">Hawaii</option>
												<option value="ID">Idaho</option>
												<option value="IL">Illinois</option>
												<option value="IN">Indiana</option>
												<option value="IA">Iowa</option>
												<option value="KS">Kansas</option>
												<option value="KY">Kentucky</option>
												<option value="LA">Louisiana</option>
												<option value="ME">Maine</option>
												<option value="MD">Maryland</option>
												<option value="MA">Massachusetts</option>
												<option value="MI">Michigan</option>
												<option value="MM">Minnesota</option>
												<option value="MS">Mississippi</option>
												<option value="MO">Missouri</option>
												<option value="MT">Montana</option>
												<option value="NE">Nebraska</option>
												<option value="NV">Nevada</option>
												<option value="NH">New Hampshire</option>
												<option value="NJ">New Jersey</option>
												<option value="NM">New Mexico</option>
												<option value="NY">New York</option>
												<option value="NC">North Carolina</option>
												<option value="ND">North Dakota</option>
												<option value="OH">Ohio</option>
												<option value="OK">Oklahoma</option>
												<option value="OR">Oregon</option>
												<option value="PA">Pennsylvania</option>
												<option value="RI">Rhode Island</option>
												<option value="SC">South Carolina</option>
												<option value="SD">South Dakota</option>
												<option value="TN">Tennessee</option>
												<option value="TX">Texas</option>
												<option value="UT">Utah</option>
												<option value="VT">Vermont</option>
												<option value="VA">Virginia</option>
												<option value="WA">Washington</option>
												<option value="WV">West Virginia</option>
												<option value="WI">Wisconsin</option>
												<option value="WY">Wyoming</option>
											</select>
										
											<!-- <label for="zip">Zip *</label> -->
											<input class="form-control" type="text" name="reseller_zip" value="" placeholder="Zip *"/>
										</div>
										<br>
										<div class="form-inline">
											<!-- <label for="email">Contact Email *</label> -->
											<input class="form-control" type="text" name="reseller_email" value="" placeholder="Your Email *" />
											<!-- <label for="phone">Phone *</label> -->
											<input class="form-control" type="text" name="reseller_phone" value="" placeholder="Your Phone *"/>
										</div>
										<div class="form-group">
											<!-- <label for="referral">Referred By</label>
 -->											<input class="form-control" type="text" name="reseller_company_name" value="" placeholder="The company you work for (optional)"/>
										</div>
										<div class="form-inline">
											<label for="username">Username *</label>
											<input class="form-control" type="text" name="reseller_username" value=""  />
											<label for="password">Password *</label>
											<input class="form-control" type="text" name="reseller_password" value="" placeholder="At least 8 characters"  />
										</div>

										<div class="clear"></div>
										<hr>
										<input type="submit" class="btn  btn-primary btn-form marg-right5" value="Free Sign up" />
										<input type="reset"  class="btn  btn-warning btn-form" value="reset" />
										<!-- Button trigger modal -->
										<button type="button" class="btn btn-default" data-toggle="modal" data-target="#terms">
  											Terms of Use
										</button>
										<button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">
  											Privacy Policy
										</button>
										<div id="note"></div>
										<div class="clear"></div>
									</form>
								</div>
							</div> 
						</div>    
					</div>
				</div>

			</section>
			<!-- PARTNERS
			================================= -->
			<section id="partners" class="benefits-section section-gray section">
				<div class="container">
				<a href="http://www.dealervault.com?campaign=DealerAddendumsWebsite#7" target ="_blank"><img src="http://d14bqw9lzcvxs9.cloudfront.net/DealerVault.png" ></a>
				<img src="http://d14bqw9lzcvxs9.cloudfront.net/spacer.png" >
				<a href="https://aws.amazon.com/" target="_blank"> <img src="http://d14bqw9lzcvxs9.cloudfront.net/AWS.png" > </a>
				<img src="http://d14bqw9lzcvxs9.cloudfront.net/spacer.png" >
				<a href="https://freshbooks.com/" target="_blank"> <img  src="http://d14bqw9lzcvxs9.cloudfront.net/freshbooks.png" > </a>
				<img src="http://d14bqw9lzcvxs9.cloudfront.net/spacer.png" >
				<a href="http://dealerhq.com/" target="_blank"> <img  src="http://d14bqw9lzcvxs9.cloudfront.net/DealerHQ.png" > </a>
				<img src="http://d14bqw9lzcvxs9.cloudfront.net/spacer.png" >
				<a href="http://www.edmunds.com/?id=apis" target="_blank"> <img  src="http://d14bqw9lzcvxs9.cloudfront.net/Edmunds.png" > </a>
				<img src="http://d14bqw9lzcvxs9.cloudfront.net/spacer.png" >
				<a href="https://stripe.com/" target="_blank"> <img  src="http://d14bqw9lzcvxs9.cloudfront.net/Stripe.png" > </a>
				</div>
			</section>



			<!-- FOOTER
			================================= -->
			<section id="footer" class="footer-section section">

				<div class="container">

					<h3 class="footer-logo">
						<img src="http://d14bqw9lzcvxs9.cloudfront.net/footer-logo.png" srcset="http://d14bqw9lzcvxs9.cloudfront.net/footer-logo@2x.png 2x" alt="Dealer Addendums Inc">
					</h3>

					<div class="footer-socmed">
						<a href="https://www.facebook.com/Addendums" target="_blank"><span class="fa fa-facebook"></span></a>
						<a href="https://twitter.com/dealeraddendums" target="_blank"><span class="fa fa-twitter"></span></a>
					<!-- 	<a href="#" target="_blank"><span class="fa fa-instagram"></span></a>
						<a href="#" target="_blank"><span class="fa fa-pinterest"></span></a> -->
					</div>

					<div class="footer-copyright">
						&copy; 2014 - 2017 DealerAddendums Inc<br>
						** Patent Pending **<br>
						<?php echo RELEASE_VERSION; ?>
					</div>

				</div>

			</section>
		
		</div>

		<!-- Modals
		================================= -->

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  			<div class="modal-dialog">
    			<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">DealerAddendums Inc.</h4>
      				</div>
      				<div class="modal-body">
						<h1>Privacy Policy</h1>
						<p>Last updated: June 02, 2015</p>
						<p>DealerAddendums Inc. ("us", "we", or "our") operates the http://dealeraddendums.com website (the "Service").</p>
						<p>This page informs you of our policies regarding the collection, use and disclosure of Personal and Business Information when you use our Service.</p>
						<p>We will not use or share your information with anyone except as described in this Privacy Policy.</p>
						<p><strong>We use your Personal Information for providing and improving the Service. By using the Service, you agree to the collection and use of information in accordance with this policy.</strong> Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms of Use</p>
						<p><strong>Information Collection And Use</strong></p>
						<p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to, your email address, name, phone number, postal address ("Personal Information").</p>
						<p>We collect this information for the purpose of providing the Service, identifying and communicating with you, responding to your requests/inquiries, servicing your purchase orders, and improving our services.</p>
						<p><strong>Log Data</strong></p>
						<p>We collect information that your browser sends whenever you visit our Service ("Log Data"). This Log Data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages and other statistics.</p>
						<p><strong>Cookies</strong></p>
						<p>Cookies are files with a small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and transferred to your device. We use cookies to collect information in order to improve our services for you.</p>
						<p>You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. The Help feature on most browsers provide information on how to accept cookies, disable cookies or to notify you when receiving a new cookie.</p>
						<p>If you do not accept cookies, you may not be able to use some features of our Service and we recommend that you leave them turned on.</p>
						<p><strong>Do Not Track Disclosure</strong></p>
						<p>We support Do Not Track ("DNT"). Do Not Track is a preference you can set in your web browser to inform websites that you do not want to be tracked.</p>
						<p>You can enable or disable Do Not Track by visiting the Preferences or Settings page of your web browser.</p>
						<p><strong>Service Providers</strong></p>
						<p>We may employ third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform Service-related services and/or to assist us in analyzing how our Service is used.</p>
						<p>These third parties have access to your Personal Information only to perform specific tasks on our behalf and are obligated not to disclose or use your information for any other purpose.</p>
						<p><strong>Compliance With Laws</strong></p>
						<p>We will disclose your Personal Information where required to do so by law or subpoena or if we believe that such action is necessary to comply with the law and the reasonable requests of law enforcement or to protect the security or integrity of our Service.</p>
						<p><strong>Security</strong></p>
						<p>The security of your Personal Information is important to us, and we strive to implement and maintain reasonable, commercially acceptable security procedures and practices appropriate to the nature of the information we store, in order to protect it from unauthorized access, destruction, use, modification, or disclosure.</p>
						<p>However, please be aware that no method of transmission over the internet, or method of electronic storage is 100% secure and we are unable to guarantee the absolute security of the Personal Information we have collected from you.</p>
						<p><strong>International Transfer</strong></p>
						<p>Your information, including Personal Information, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>
						<p>If you are located outside United States and choose to provide information to us, please note that we transfer the information, including Personal Information, to United States and process it there.</p>
						<p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>
						<p><strong>Links To Other Sites</strong></p>
						<p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>
						<p>We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>
						<p><strong>Children's Privacy</strong></p>
						<p>Only persons age 18 or older have permission to access our Service. Our Service does not address anyone under the age of 13 ("Children").</p>
						<p>We do not knowingly collect personally identifiable information from children under 13. If you are a parent or guardian and you learn that your Children have provided us with Personal Information, please contact us. If we become aware that we have collected Personal Information from a children under age 13 without verification of parental consent, we take steps to remove that information from our servers.</p>
						<p><strong>Changes To This Privacy Policy</strong></p>
						<p>This Privacy Policy is effective as of June 02, 2015 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page.</p>
						<p>We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Your continued use of the Service after we post any modifications to the Privacy Policy on this page will constitute your acknowledgment of the modifications and your consent to abide and be bound by the modified Privacy Policy.</p>
						<p>If we make any material changes to this Privacy Policy, we will notify you either through the email address you have provided us, or by placing a prominent notice on our website.</p>
						<p><strong>Contact Us</strong></p>
						<p>If you have any questions about this Privacy Policy, please contact us.</p>
      				</div>
      				<div class="modal-footer">
        				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      				</div>
    			</div>
  			</div>
		</div>


		<div class="modal fade" id="terms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  			<div class="modal-dialog">
   		 		<div class="modal-content">
      				<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        				<h4 class="modal-title" id="myModalLabel">DealerAddendums Inc.</h4>
      				</div>
      			<div class="modal-body">
					<h1>Terms of Use ("Terms")</h1>
					<p>Last updated: December 09, 2015</p>
					<p>
					Please read these Terms of Use ("Terms", "Terms of Use") carefully before using the http://dealeraddendums.com website (the "Service") operated by DealerAddendums Inc. ("us", "we", or "our").<br><br>
					Your access to and use of the Service is conditioned upon your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who wish to access or use the Service.<br><br>
					<strong>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you do not have permission to access the Service.</strong><br><br>
					</p><p>
					<Strong>Accounts</Strong><br>
					All accounts with DealerAddendums Inc are month-to-month and there is no contract, or set up fee.
					<ul>
						<li>Free trial accounts are full featured accounts with manual data import and management and are limited to 50 vehicle prints.</li>
						<li>Manual Accounts are full featured accounts with manual data import and management and unlimited vehicle printing.</li>
						<li>Automatic DMS and Automatic Web accounts have all the Manual account features but add automatic inventory management through DMS or Web integration.</li></ul>
					</p><p>
					Manual and Automatic accounts are recurring subscription accounts (paid) and are billed on the monthly account creation anniversary. Bills are due on receipt, and are late at 37 days. Late accounts are automatically suspended until account is brought current.<br><br>
					Labels ordered through our system are shipped the next business day via USPS Priority Flat Rate and usually arrive in two days. The shipping and any taxes are included in the pricing and the cost of the ordered labels is added to your next invoice.<br><br>
					When you create an account, you guarantee that you are above the age of 18, and that the information you provide us is accurate, complete, and current at all times. Inaccurate, incomplete, or obsolete information may result in the immediate termination of your account on the Service.<br><br>
					You are responsible for maintaining the confidentiality of your account and password, including but not limited to the restriction of access to your computer and/or account. You agree to accept responsibility for any and all activities or actions that occur under your account and/or password, whether your password is with our Service or a third-party Single Sign On service. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.<br><br>
					You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trademark that is subject to any rights of another person or entity other than you, without appropriate authorization. You may not use as a username any name that is offensive, vulgar or obscene.
					We reserve the right to refuse service, terminate accounts, remove or edit content in our sole discretion.
					</p><p>
					<Strong>Communications</Strong><br>

					By creating an Account on our service, you agree to subscribe to newsletters, marketing or promotional materials and other information we may send. However, you may opt out of receiving any, or all, of these communications from us by following the unsubscribe link or instructions provided in any email we send.
					</p><p>
					<Strong>Content</Strong><br>

					Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material ("Content"). You are responsible for the Content that you post on or through the Service, including its legality, reliability, and appropriateness.<br><br>
					By posting Content on or through the Service, You represent and warrant that: (i) the Content is yours (you own it) and/or you have the right to use it and the right to grant us the rights and license as provided in these Terms, and (ii) that the posting of your Content on or through the Service does not violate the privacy rights, publicity rights, copyrights, contract rights or any other rights of any person or entity. We reserve the right to terminate the account of anyone found to be infringing on a copyright.<br><br>
					You retain any and all of your rights to any Content you submit, post or display on or through the Service and you are responsible for protecting those rights. We take no responsibility and assume no liability for Content you or any third party posts on or through the Service. However, by posting Content using the Service you grant us the right and license to use, modify, publicly perform, publicly display, reproduce, and distribute such Content on and through the Service.<br><br>
					DealerAddendums Inc has the right but not the obligation to monitor and edit all Content provided by users.<br><br>
					In addition, Content found on or through this Service are the property of DealerAddendums Inc or used with permission. You may not distribute, modify, transmit, reuse, download, repost, copy, or use said Content, whether in whole or in part, for commercial purposes or for personal gain, without express advance written permission from us.
					</p><p>
					<Strong>Links To Other Web Sites</Strong><br>

					Our Service may utilize third party web sites or services that are not owned or controlled by DealerAddendums Inc.<br><br>
					DealerAddendums Inc has no control over, and assumes no responsibility for the content, privacy policies, or practices of any third party web sites or services. We do not warrant the offerings of any of these entities/individuals or their websites.<br><br>
					You acknowledge and agree that DealerAddendums Inc shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such third party web sites or services.<br><br>
					We strongly advise you to read the terms and conditions and privacy policies of any third party web sites or services that you visit.
					</p><p>
					<Strong>Termination</Strong><br><br>

					<strong>DealerAddendums does NOT prorate the current invoice for terminated accounts.</strong><br><br>
					If you wish to terminate your account, you may simply log in and convert your account to the "Free" plan and pay any outstanding invoices. You can also send an email with your termination request to allan@dealeraddendums.com. <br><br>
					We may terminate or suspend your account and bar access to the Service immediately, without prior notice or liability, under our sole discretion, for any reason whatsoever and without limitation, including but not limited to a breach of the Terms.<br><br>
					All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.
					</p><p>
					<Strong>Indemnification</Strong><br>

					You agree to defend, indemnify and hold harmless DealerAddendums Inc and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees), resulting from or arising out of a) your use and access of the Service, by you or any person using your account and password; b) a breach of these Terms, or c) Content posted on the Service.
					</p><p>
					<strong>Limitation Of Liability</strong><br>

					In no event shall DealerAddendums Inc, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.
					</p><p>
					<Strong>Disclaimer</Strong><br>

					Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and "AS AVAILABLE" basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance.<br><br>
					DealerAddendums Inc its subsidiaries, affiliates, and its licensors do not warrant that a) the Service will function uninterrupted, secure or available at any particular time or location; b) any errors or defects will be corrected; c) the Service is free of viruses or other harmful components; or d) the results of using the Service will meet your requirements.
					</p><p>
					<Strong>Exclusions</Strong><br>

					Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or limitation of liability for consequential or incidental damages, so the limitations above may not apply to you.
					</p><p>
					<Strong>Governing Law </Strong><br>

					These Terms shall be governed and construed in accordance with the laws of Utah, United States, without regard to its conflict of law provisions.<br><br>
					Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have had between us regarding the Service.
					</p><p>
					</Strong>Changes</Strong><br>

					We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material, we will provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.<br><br>
					By continuing to access or use our Service after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Service.
					</p><p>
					</Strong>Contact Us</strong><br>

					If you have any questions about these Terms, please contact us.
					</p>
      			</div>
      			<div class="modal-footer">
        			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      			</div>
    		</div>
  		</div>
	</div>



		<!-- JAVASCRIPT
		================================= -->
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/respimage.min.js"></script>
		<script src="js/jpreloader.min.js"></script>
		<script src="js/smoothscroll.min.js"></script>
		<script src="js/jquery.nav.min.js"></script>
		<script src="js/jquery.inview.min.js"></script>
		<script src="js/jquery.counterup.min.js"></script>
		<script src="js/jquery.stellar.min.js"></script>
		<script src="js/maplace-0.1.3.min.js"></script>
		<script src="js/responsiveslides.min.js"></script>
		<script src="js/script.js"></script>
		<script src="tutorial/media/scripts/embedded-smart-player.min.js"></script>

		<script type="text/javascript">
		//<![CDATA[
			$(document).ready(function(){	
				$("#ajax-contact-form").submit(function() {
					var str = $(this).serialize();		
					$.ajax({
						type: "POST",
						url: "contact_form/reseller_sign_up.php",
						data: str,
						success: function(msg) {
							result = '';
							result1 = '';
							// Message Sent - Show the 'Thank You' message and hide the form
							if(msg == 'OK') {
								result1 = '<div class="notification_ok"><h3>Your Free Reseller Account is now set up and you should receive a confirmation email in a minute or two. <br><br>Thank you!<br>Allan Tone - Founder & CEO</h3></div>';
								$("#fields").hide();
							} else {
								result = msg;
							}
							$('#note').html(result);
							$('#note1').html(result1);

						}
					});
					return false;
				});															
			});
		//]]>		
		</script>

		<script>
			$('#myModal').on('shown.bs.modal', function () {
			  	$('#myInput').focus()
			})
		</script>


<!-- Intercom new -->

<script>
  window.intercomSettings = {
    app_id: "7cda8fc1bfea25032a2ba95d5d862ecb86f8e976"
  };
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/7cda8fc1bfea25032a2ba95d5d862ecb86f8e976';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>

<!-- Intercom API script -->
		
		<!-- Scripts for fixing placeholders in IE -->
		<script> 
			$('[placeholder]').focus(function() {
			  var input = $(this);
			  if (input.val() == input.attr('placeholder')) {
			    input.val('');
			    input.removeClass('placeholder');
			  }
			}).blur(function() {
			  var input = $(this);
			  if (input.val() == '' || input.val() == input.attr('placeholder')) {
			    input.addClass('placeholder');
			    input.val(input.attr('placeholder'));
			  }
			}).blur();
			$('[placeholder]').parents('form').submit(function() {
			  $(this).find('[placeholder]').each(function() {
			    var input = $(this);
			    if (input.val() == input.attr('placeholder')) {
			      input.val('');
			    }
			  })
			});
		</script>

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-46873878-2', 'auto');
		  ga('send', 'pageview');

		</script>


	</body>

</html>