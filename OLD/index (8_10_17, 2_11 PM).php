
<!DOCTYPE html>
<html lang="en">

<head>
  <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" CONTENT="New vehicle addendums, addendum software and addendum templates. Printing addendums has never been easier than with DealerAddendums.com">
  <meta name="keywords" CONTENT="dealer addendums, automotive addendum, auto dealer stickers, car dealership labels, addendum templates, window stickers, addendum labels, addendum software, buyers guides, information sheets,  VIN Decoder, CDK, 3PA">
  <meta name="copyright" CONTENT="Copyright © 2017 DealerAddendums.com. All Rights Reserved.">
  <meta name="author" CONTENT="Allan Tone">
  <meta name="language" CONTENT="English">

  <meta name="robots" content="index,follow">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="Addendum System for Car Dealers | DealerAddendums Inc.">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="theme-color" content="#3c7cc4">
  <title>Addendum System for Car Dealers | DealerAddendums Inc.</title>

  <!-- Facebook -->
  <meta property="og:locale" content="en_US">
  <meta property="og:url" content="http://www.dealeraddendums.com">
  <meta property="og:title" content="Addendum System for Car Dealers | DealerAddendums Inc.">
  <meta property="og:site_name" content="DealerAddendums Inc">
  <meta property="og:description" content=New vehicle addendums, addendum software and addendum templates. Printing addendums has never been easier than with DealerAddendums.com">
  <meta property="og:image" content="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-72x72.png">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="80">
  <meta property="og:image:height" content="80">
  <meta property="og:type" content="website">

  <!-- Fav and Touch Icons -->
  <meta name="msapplication-TileColor" content="#d9534f">
  <meta name="msapplication-TileImage" content="images/favicons/mstile-144x144.png">
  <meta name="msapplication-config" content="images/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <link rel="apple-touch-icon" sizes="57x57" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="152x152" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="images/favicons/manifest.json">
  <link rel="shortcut icon" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/favicon.ico">

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!--=== FONT ICONS ===-->
    <!-- Font Awesome -->
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- IonIcons -->
    <link href="assets/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- Elegant Icons -->
    <link href="assets/elegant-icons/css/elegant-icons.min.css" rel="stylesheet">
    <!--[if lte IE 7]><script src="assets/elegant-icons/js/elegant-icons-lte-ie7.min.js"></script><![endif]-->

  <!--=== VENDORS ===-->
    <!-- Slick Carousel -->
    <link href="css/vendor/slick.min.css" rel="stylesheet">
    <link href="css/vendor/slick-theme.min.css" rel="stylesheet">

    <!-- Blueimp Gallery Lightbox -->
    <link href="css/vendor/blueimp-gallery.min.css" rel="stylesheet">

  <!-- Custom -->
  <link href="css/main.min.css" rel="stylesheet">
  <link href="css/style.min.css" rel="stylesheet">
  <link href="css/animation.min.css" rel="stylesheet">
  <link href="css/responsive.min.css" rel="stylesheet">
  <link href="css/typography.min.css" rel="stylesheet">
  <link href="css/colors/default.min.css" rel="stylesheet">
  <link href="css/colors/orange.css" rel="stylesheet">

  <!--Inline styles  -->
  <style>
    orange{
      color: orange;
      font-weight: bold;
    }
    .stats {
      position: relative;
      z-index: 0;
      padding: 50px 0 64px;
      background: url(http://d14bqw9lzcvxs9.cloudfront.net/dealership_blurred_dark.jpg) no-repeat center center;
      background-attachment: fixed;
      background-size: cover;
    }
    #rcorners {
      border-radius: 5px;
    }
    #button1 {
      background-color: #555;
    }
    #button2 {
      background-color: #468c00;
    }
    #button3 {
      background-color: #555;
    }
  </style>

  <?php 
      //INCLUDE FILES 
      include("../version/version.php");
      
      // CONNECT TO DATABASE 
      include("app/assets/config/db.php");
      $count_sql=mysqli_query($db,"select*from count_table where id=1");
      $count=mysqli_fetch_array($count_sql);
      // COUNTER CALCULATIONS   
      // DEALER COUNT
      $cu_dealer=mysqli_query($db,"SELECT count(*) as total from dealer_dim where ACTIVE='Yes'");
      $count_dealer=mysqli_fetch_array($cu_dealer);
      
      // TODAYS PRINT COUNT
      $dateta=date("Y-m-d");
      $todays=$count['print_today'];

      // TOTAL PRINT COUNT
      // CURRENT VEHICLES
      //$pr_addendum=mysqli_query($db,"SELECT count(*) as total from dealer_inventory where PRINT_FLAG='1'");
      $printed_addendum=$count['printed'];
      // OLD VEHICLES
      //$pr_addendum_old=mysqli_query($db,"SELECT count(*) as total from z_dealer_inventory_inactive where PRINT_FLAG='1'");
      $printed_addendum_old=$count['old_addendum'];
      // TOTAL VENICLES
      $total_prints = number_format($printed_addendum + $printed_addendum_old + 460000);
    ?>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!--[if gt IE 8]>
    <link href="css/ie9.min.css" rel="stylesheet">
  <![endif]-->

  <!-- Intercom Chat -->
  <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/7cda8fc1bfea25032a2ba95d5d862ecb86f8e976';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()
  </script>

</head>

<body class="btn-circle sld-transition-2" id="top" data-spy="scroll" data-target=".navbar">

  <!-- Preloader -->
  <div class="preloader">
    <div class="status"></div>
  </div>
  <!-- End Preloader -->
          
  <!-- Modal -->
  <div class="modal fade modal-full" id="modal" tabindex="-1" role="dialog" aria-labelledby="title-modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      </div>
    </div>
  </div>
  <!-- End Modal -->


  <!-- LAYOUT -->
  <div class="layout">

    <!-- HEADER -->
    <header>

      <!-- SLIDESHOW -->
      <section class="slideshow">
        <div class="container-fluid">
          <div class="r1 row">
            <div class="c1 col-md-12">
              <div class="carousel slide" id="slideshow-carousel-1" data-ride="carousel">

                <!-- Wrapper for Slides -->
                <div class="carousel-inner" role="listbox">

                  <!-- Slide 3 -->
                  <div class="item item-3 slide-image active">
                    <img class="img-responsive bg-item parallax" data-speed="1" src="http://d14bqw9lzcvxs9.cloudfront.net/dealership_blurred_dark.jpg" alt="DealerAddendums Inc.">
                    <div class="carousel-caption">
                      <div class="c4 col-sm-5 col-md-5">
                        <img class="img-responsive center-block" src="http://d14bqw9lzcvxs9.cloudfront.net/addendums/<?php echo rand(1, 10); ?>.png" alt="Image">
                      </div>
                      <div class="c5 col-xs-12 col-sm-7 col-md-7">
                        <h2 class="title">Create, Manage and Print Addendums Online.
                        <img class="img-responsive center-block" src="http://d14bqw9lzcvxs9.cloudfront.net/horizontal_stacked_website.png"></h2>
                        <a class="btn btn-lg btn-danger btn-download" href="#signup">Free Trial</a><a class="btn btn-lg btn-danger btn-download" href="/app">Log In</a>
                      </div>
                    </div>
                  </div>
                  <!-- End Slide 3 -->

                </div>
                <!-- End Wrapper for Slides -->

              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- END SLIDESHOW -->

      <!-- NAVBAR -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and Toggle -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href=""><img class="logo" src="images/logo.png" alt="Logo"></a>
          </div>
          <!-- End Brand and Toggle -->
          <!-- Menu -->
          <div class="collapse navbar-collapse" id="navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active" data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#top">Home</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#overview">Overview</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#features">Features</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1"> 
                <a href="#numbers">Our Numbers</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#examples">Examples</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#testimonials">Testimonials</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#team">Team</a>
              </li>
               <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#contact">Contact Us</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#pricing">Pricing</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#signup">Free Trial</a>
              </li>
              <li class="dropdown li-more">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">+</a>
                <ul class="dropdown-menu" id="overflow"></ul>
              </li>
            </ul>
          </div>
          <!-- End Menu -->
        </div>
      </nav>
      <!-- END NAVBAR -->

    </header>
    <!-- END HEADER -->


    <!-- MAIN -->
    <div class="main">

      <!-- OVERVIEW -->
      <section class="overview" id="overview">
        <div class="container">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Overview</h2>
             <h3 class="section-subtitle">DealerAddendums.com is an online subscription service for new and used vehicle dealers. The system allows dealers to print New Vehicle Addendums, Buyers Guides, and Info Sheets in seconds.</h3>
          </div>
          <!-- End Section Header -->
          <!-- Section Content -->
          <div class="r1 row">
            <div class="c1 col-md-12">
              <!-- Nav Tabs -->
              <ul class="nav nav-justified nav-tabs" role="tablist">
                <li>
                  <a class="btn-lg" data-toggle="tab" href="#overview-tab-2">Customize</a>
                </li>
                <li class="active">
                  <a class="btn-lg" data-toggle="tab" href="#overview-tab-1">Create in seconds</a>
                </li>
                <li>
                  <a class="btn-lg" data-toggle="tab" href="#overview-tab-3">Explainer Video</a>
                </li>
              </ul>
              <!-- End Nav Tabs -->
              <!-- Tab Panes -->
              <div class="tab-content">
                <!-- Tab Pane 1 -->
                <div class="tab-pane fade in active" id="overview-tab-1">
                  <div class="r1 row">
                    <!-- Block 1 -->
                    <div class="c1 col-sm-6 col-md-6 col-sm-push-6">
                      <div class="img-perspective">
                        <img class="img-responsive center-block img-1" src="images/overview/overview_left_3.png" alt="Image">
                        <img class="img-responsive center-block img-2" src="images/overview/overview_left_2.png" alt="Image">
                        <img class="img-responsive center-block img-3" src="images/overview/overview_left_1.png" alt="Image">
                      </div>
                    </div>
                    <!-- End Block 1 -->
                    <!-- Block 2 -->
                    <div class="c2 col-sm-6 col-md-6 col-sm-pull-6">
                      <div class="overview-wrapper">
                        <div class="overview-content">
                          <h3 class="title">Create Addendums<br><small class="subtitle">Addendums in Seconds</small></h3>
                          <p><orange>1. Select Vehicle(s) :</orange> Select your vehicle, or vehicles. You can search by stock, VIN or model.</p>
                          <p><orange>2. Add Options :</orange> Use the Default options to automatically add options based on Model or Bodystyle, or choose from a dropdown list of common options</p>
                          <p><orange>3. Print :</orange> Print one at a time or use the batch function to print up to 50 at a time. Print using ANY laser printer.</p>
                          
                        </div>
                      </div>
                    </div>
                    <!-- End Block 2 -->
                  </div>
                </div>
                <!-- End Tab Pane 1 -->
                <!-- Tab Pane 2 -->
                <div class="tab-pane fade" id="overview-tab-2">
                  <div class="r1 row">
                    <!-- Block 1 -->
                    <div class="c1 col-sm-4 col-md-4 col-sm-push-4 animateblock size-img speed-2">
                      <div class="img-wireframe">
                        <img class="img-responsive center-block img-4" src="images/overview/overview_center_1.png" alt="Image">
                        <img class="img-responsive center-block img-5" src="images/overview/overview_center_2.png" alt="Image">
                      </div>
                    </div>
                    <!-- End Block 1 -->
                    <!-- Block 2 -->
                    <div class="c2 col-sm-4 col-md-4 col-sm-pull-4 animateblock ltr-2 speed-3">
                      <div class="overview-wrapper">
                        <div class="hidden-sm overview-icon"><span class="ion-social-html5"></span>
                        </div>
                        <div class="overview-content">
                          <h3 class="title">Dealership Logo</h3>
                          <p class="description">Upload your dealership, or group logo in fill high resolution color.</p>
                        </div>
                      </div>
                      <div class="overview-wrapper">
                        <div class="hidden-sm overview-icon"><span class="glyphicon glyphicon-cog"></span>
                        </div>
                        <div class="overview-content">
                          <h3 class="title">Inventory data</h3>
                          <p class="description">We can pull from your website, or DMS and you choose what specific vehicle info to print.</p>
                        </div>
                      </div>
                      <div class="overview-wrapper">
                        <div class="hidden-sm overview-icon"><span class="glyphicon glyphicon-eye-open"></span>
                        </div>
                        <div class="overview-content">
                          <h3 class="title">Custom Footer</h3>
                          <p class="description">Choose from EPA, QR Code, VIN Barcode, product marketing, or upload your own image.</p>
                        </div>
                      </div>
                    </div>
                    <!-- End Block 2 -->
                    <!-- Block 3 -->
                    <div class="c3 col-sm-4 col-md-4 animateblock rtl-2 speed-3">
                      <div class="overview-wrapper">
                        <div class="hidden-sm overview-icon"><span class="ion-social-buffer"></span>
                        </div>
                        <div class="overview-content">
                          <h3 class="title">Vehicle Options</h3>
                          <p class="description">Enter your options once and have them auto applied to specific vehicles or choose them from a dropdown. We make it easy to print one or a hunders addendums at a time.</p>
                        </div>
                      </div>
                      <div class="overview-wrapper">
                        <div class="hidden-sm overview-icon"><span class="glyphicon glyphicon-book"></span>
                        </div>
                        <div class="overview-content">
                          <h3 class="title">Customization</h3>
                          <p class="description">Watermarks, border colors, special fonts, you name it, we can make it happen.</p>
                        </div>
                      </div>
                      <div class="overview-wrapper">
                        <div class="hidden-sm overview-icon"><span class="glyphicon glyphicon-headphones"></span>
                        </div>
                        <div class="overview-content">
                          <h3 class="title">Personal Support</h3>
                          <p class="description">Don't worry about learning all the ins and outs of customization, we'll do it all for you... for free.</p>
                        </div>
                      </div>
                    </div>
                    <!-- End Block 3 -->
                  </div>
                </div>
                <!-- End Tab Pane 2 -->
                <!-- Tab Pane 3 -->
                <div class="tab-pane fade" id="overview-tab-3">
                  <div class="r1 row">
                    <!-- Block 1 -->
                    <div class="c1 col-sm-6 col-md-6">
                      <div class="img-slide">
                        <script src="//content.jwplatform.com/players/fzyhnJaP-X7R8yf1M.js"></script><br><br>
                        <script src="//content.jwplatform.com/players/I00yDUPC-X7R8yf1M.js"></script>
                      </div>
                    </div>
                    <!-- End Block 1 -->
                    <!-- Block 2 -->
                    <div class="c2 col-sm-6 col-md-6">
                      <div class="overview-content">
                        <h3 class="title">User-friendly Application<br><small class="subtitle">500+ dealerships</small></h3>
                        <p>We have been doing addendums since 2014 and have over 500 happy dealerships.</p>
                        <p>The videos to the left show what we do, and the problems we solve. Take a look and remember you can always call us to answer any specific questions.</p>
                      </div>
                    </div>
                    <!-- End Block 2 -->
                  </div>
                </div>
                <!-- End Tab Pane 3 -->
              </div>
              <!-- End Tab Panes -->
            </div>
          </div>
          <!-- End Section Content -->
        </div>
      </section>
      <!-- END OVERVIEW -->



      <!-- FEATURES -->
      <section class="features" id="features">
        <div class="container">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Features</h2>
            <h3 class="section-subtitle">NO CONTRACT, MONTH-TO-MONTH, AND FREE FOR 50 VEHICLES</h3>
          </div>
          <!-- End Section Header -->
          <!-- Section Content -->
          <div class="r1 row animateblock btt speed-1">
            <div class="c1 col-md-12" id="features-carousel-1">
              <!-- Block 1 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_genius"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">Vehicle Addendums</h3>
                  <p class="description">Print beautiful, full color or black and white, addendums in seconds for as little as $0.14 a piece. Add your dealer installed options to increase your profits!</p>
                </div>
              </div>
              <!-- End Block 1 -->
              <!-- Block 2 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_cloud-download"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">Automatic Web</h3>
                  <p class="description">The Automatic Web Plan updates your inventory every night with data from your website or syndication provider. No more manual data entry!</p>
                </div>
              </div>
              <!-- End Block 2 -->
              <!-- Block 3 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_cloud-download"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">Automatic DMS</h3>
                  <p class="description">The Automatic DMS Plan updates your inventory automatically with your DMS data we receive from DealerVault or direct from CDK. No manual data entry and your inventory shows up FAST!</p>
                </div>
              </div>
              <!-- End Block 3 -->
              <!-- Block 4 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_gift_alt"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">Vehicle Options</h3>
                  <p class="description">Apply options to a vehicle addendum based on preset defaults. Options can be manually added or automatically added based on model and/or bodystyle.</p>
                </div>
              </div>
              <!-- End Block 4 -->
              <!-- Block 5 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_document"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">Info Sheets</h3>
                  <p class="description">Easily print Pre-Owned Vehicle Information Sheets, with your logo and vehicle details, description, options, QR code, and optional PureCars integration.</p>
                </div>
              </div>
              <!-- End Block 5 -->
              <!-- Block 6 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_document"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">Buyers Guides</h3>
                  <p class="description">As-Is, Implied, or Warranty Buyers Guides in English and Spanish can be printed in seconds with the click of your mouse.</p>
                </div>
              </div>
              <!-- End Block 6 -->
              <!-- Block 7 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_globe-2"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">A SaaS Application</h3>
                  <p class="description">DealerAddendums is a web based solution, so there is no software to install, and you are always working on the latest version.</p>
                </div>
              </div>
              <!-- End Block 7 -->
              <!-- Block 8 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_error-oct_alt"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">NO CONTRACTS</h3>
                  <p class="description">There are never any contracts, set up or cancellation fees. Use the system for as long as it's a fit, cancel when it's not.</p>
                </div>
              </div>
              <!-- End Block 8 -->
            </div>
          </div>
          <!-- End Section Content -->
        </div>
      </section>
      <!-- END FEATURES -->

      <!-- STATS -->
      <section class="stats hidden-xs" id="numbers">
        <div class="bg-section">
          <!-- <img src="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-72x72.png" alt="Stats Section Background"> -->
        </div>
        <div class="container">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Realtime Numbers <a href="http://dealeraddendums.com/map" target="_blank">(Map)</a></h2>
            <h3 class="section-subtitle">HUNDREDS OF THOUSANDS PRINTED IN OVER 500 DEALERSHIPS!</h3>
          </div>
          <!-- End Section Header -->
          <!-- Section Content -->
          <div class="container">


          <div class="numbers-row row">

            <!-- NUMBERS - ITEM 1 -->
            <div class="col-md-3 col-sm-6">
              <div class="numbers-item">
                <h1 class="name"><?php echo $total_prints?></h1>
                <div class="section-subtitle">Addendums Printed</div>
              </div>
            </div>

            <!-- NUMBERS - ITEM 2 -->
            <div class="col-md-3 col-sm-6">
              <div class="numbers-item">
                <h1 class="name"><?php echo $todays;?></h1>
                <div class="section-subtitle">Printed Today</div>
              </div>
            </div>

            <!-- NUMBERS - ITEM 3 -->
            <div class="col-md-3 col-sm-6">
              <div class="numbers-item">
                <h1 class="name">33.1</span>Sec.</h1>
                <div class="section-subtitle">Average Print Time</div>
              </div>
            </div>

            <!-- NUMBERS - ITEM 4 -->
            <div class="col-md-3 col-sm-6">
              <div class="numbers-item">
                <h1 class="name"><?php echo $count_dealer['total']; ?> <span class="fa fa-coffee"></span></h1>
                <div class="section-subtitle">Number of Dealers</div>
              </div>
            </div>

          </div>

        </div>
          <!-- End Section Content -->
        </div>
      </section>
      <!-- END STATS -->

      <!-- EXAMPLES -->
      <section class="gallery" id="examples">
        <div class="container">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Example Addendums</h2>
            <h3 class="section-subtitle">Here are a few examples of "Real" Addendums in use.</h3>
          </div>
          <!-- End Section Header -->
          <!-- Section Content -->
          <div class="r1 row animateblock btt speed-1">
            <div class="c1 col-md-12" id="gallery-carousel-1">
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_1.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_1.jpg" alt="Screenshot 1">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_2.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_2.jpg" alt="Screenshot 2">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_3.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_3.jpg" alt="Screenshot 3">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_4.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_4.jpg" alt="Screenshot 4">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_5.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_5.jpg" alt="Screenshot 5">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_6.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_6.jpg" alt="Screenshot 6">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_7.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_7.jpg" alt="Screenshot 7">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_8.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_8.jpg" alt="Screenshot 8">
                </a>
              </div>
            </div>
          </div>
          <!-- End Section Content -->
          <!-- Modal -->
          <div class="blueimp-gallery blueimp-gallery-controls" id="blueimp-gallery">
            <div class="slides"></div>
            <a class="prev"><span class="fa fa-angle-left"></span></a>
            <a class="next"><span class="fa fa-angle-right"></span></a>
            <a class="close">×</a>
          </div>
          <!-- End Modal -->
        </div>
      </section>
      <!-- END GALLERY -->

      <!-- TESTIMONIALS -->
      <section class="testimonials" id="testimonials">
        <div class="container">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Testimonials</h2>
            <h3 class="section-subtitle">Here are just a few testimonials from some of our dealers. He have TONS more...</h3>
          </div>
          <!-- End Section Header -->
          <!-- Section Content -->
          <div class="r1 row animateblock btt speed-1">
            <div class="c1 col-md-12">
              <div class="carousel slide fade" id="testimonials-carousel-1" data-ride="carousel">
                <!-- Controls -->
                <div class="testimonials-controls">
                  <a class="arrow-left" data-slide="prev" href="#testimonials-carousel-1" role="button"><span class="fa fa-angle-left"></span></a>
                  <a class="arrow-right" data-slide="next" href="#testimonials-carousel-1" role="button"><span class="fa fa-angle-right"></span></a>
                </div>
                <!-- End Controls -->
                <!-- Wrapper for Slides -->
                <div class="carousel-inner" role="listbox">                  

                  <!-- Testimonial 1 -->
                  <div class="item active">
                    <div class="testimonials-photo">
                      <img src="images/testimonials/testimonial_1.jpg" alt="Testimonial 1">
                    </div>
                    <div class="testimonials-info">
                      <cite class="author" title="Source Title 1">Jason Falbo</cite>
                      <a class="link-company" href="http://www.finneganchryslerjeepdodge.com/" target="_blank">Finnigan CDJR</a>
                    </div>
                    <blockquote>
                      <p>Ok, we’re signed up. Placed order for labels as well. Try to hurry those over! You win, your product rocks..super easy to use and intuitive. Feel free to cut and paste that to whatever testimonials you have.</p>
                    </blockquote>
                  </div>
                  <!-- End Testimonial 1 -->                  
                  <!-- Testimonial 2 -->
                  <div class="item">
                    <div class="testimonials-photo">
                      <img src="images/testimonials/testimonial_0.jpg" alt="Testimonial 2">
                    </div>
                    <div class="testimonials-info">
                      <cite class="author" title="Source Title 2">Christine</cite>
                      <a class="link-company" href="https://www.mikemaroonehonda.com/" target="_blank">Townsend Honda</a>
                    </div>
                    <blockquote>
                      <p>Hi Allan, thank you so very much and for creating this website, I absolutely love it and using it, it makes my job a whole lot easier!</p>
                    </blockquote>
                  </div>
                  <!-- End Testimonial 2 -->
                  <!-- Testimonial 3 -->
                  <div class="item">
                    <div class="testimonials-photo">
                      <img src="images/testimonials/testimonial_3.jpg" alt="Testimonial 3">
                    </div>
                    <div class="testimonials-info">
                      <cite class="author" title="Source Title 3">Yazmin Garcia</cite>
                      <a class="link-company" href="https://www.crownbmw.com/" target="_blank">Crown BMW</a>
                    </div>
                    <blockquote>
                      <p>Dealer Addendums has truly made our addendum process so much easier! The efficiency and quick response from Allan is outstanding! Here at Crown BMW we give Dealer Addendums two thumbs up</p>
                    </blockquote>
                  </div>
                  <!-- End Testimonial 3 -->
                  <!-- Testimonial 4 -->
                  <div class="item">
                    <div class="testimonials-photo">
                      <img src="images/testimonials/testimonial_4.jpg" alt="Testimonial 3">
                    </div>
                    <div class="testimonials-info">
                      <cite class="author" title="Source Title 4">Phillip Taylor</cite>
                      <a class="link-company" href="http://www.stevensonauto.com/" target="_blank">Stevenson Automotive Group</a>
                    </div>
                    <blockquote>
                      <p>We got tired of wrestling with Reynolds' aniquated addendum system, now we have 8 rooftops using DealerAddendums. It couldn't be easier to use, or an easier decision to make!</p>
                    </blockquote>
                  </div>
                  <!-- End Testimonial 4 -->
                  <!-- Testimonial 5 -->
                  <div class="item">
                    <div class="testimonials-photo">
                      <img src="images/testimonials/testimonial_5.jpg" alt="Testimonial 3">
                    </div>
                    <div class="testimonials-info">
                      <cite class="author" title="Source Title 5">Ian Weir</cite>
                      <a class="link-company" href="https://www.richmond-bmw.com/" target="_blank">Richmond BMW</a>
                    </div>
                    <blockquote>
                      <p>DealerAddendums makes addendums quick and easy for my entire inventory. Customer service is outstanding and always helpful</p>
                    </blockquote>
                  </div>
                  <!-- End Testimonial 5 -->
                  <!-- Testimonial 6 -->
                  <div class="item">
                    <div class="testimonials-photo">
                      <img src="images/testimonials/testimonial_6.jpg" alt="Testimonial 3">
                    </div>
                    <div class="testimonials-info">
                      <cite class="author" title="Source Title 6">Amy</cite>
                      <a class="link-company" href="#" target="_blank">Spradley Kia Mazda</a>
                    </div>
                    <blockquote>
                      <p>Great service, great pricing, and anytime I need help, I am given remote support right away! Fantastic service!!</p>
                    </blockquote>
                  </div>
                  <!-- End Testimonial 6 -->

                </div>
                <!-- End Wrapper for Slides -->
              </div>
            </div>
          </div>
          <!-- End Section Content -->
        </div>
      </section>
      <!-- END TESTIMONIALS -->

      <!-- CLIENTS -->
      <section class="clients">
        <div class="container-fluid">
          <!-- Section Content -->
          <div class="r1 row"><div align="center"><h3>Trusted by these groups and many more!</h3></div>
            <div class="c1" id="clients-carousel-1">
              <div class="client-logo">
                <a href="http://www.lithia.com/" target="_blank"><img class="center-block" src="images/clients/client-1.png" alt="Lithia Motors"></a>
              </div>
              <div class="client-logo">
                <a href="https://www.lhmauto.com/" target="_blank"><img class="center-block" src="images/clients/client-2.png" alt="Larry H Miller Automotive"></a>
              </div>
              <div class="client-logo">
                <a href="https://www.asburyauto.com/" target="_blank"><img class="center-block" src="images/clients/client-3.png" alt="Asbury Automotive Group"></a>
              </div>
              <div class="client-logo">
                <a href="http://www.sonicautomotive.com/" target="_blank"><img class="center-block" src="images/clients/client-4.png" alt="Sonic Automotive"></a>
              </div>
              <div class="client-logo">
                <a href="http://www.berkshirehathawayautomotive.com/" target="_blank"><img class="center-block" src="images/clients/client-5.png" alt="Berkshire Hathaway Automotive"></a>
              </div>
              <div class="client-logo">
                <a href="https://www.kengarff.com/" target="_blank"><img class="center-block" src="images/clients/client-6.png" alt="Ken Garff Automotive Group "></a>
              </div>
            </div>
          </div>
          <!-- End Section Content -->
        </div>
      </section>
      <!-- END CLIENTS -->
 
      <!-- TEAM -->
      <section class="team" id="team">
        <div class="container">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Who</h2>
            <h3 class="section-subtitle">We are a team of dedicated entrepreneurs who love solving problems worth solving.</h3>
          </div>
          <!-- End Section Header -->
          <div class="r1 row animateblock btt speed-1">
            <div class="c1 col-md-12" id="team-carousel-1">
              <!-- Block 1 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_allan.jpg" alt="Member 1">
                </div>
                <div class="team-content">
                  <h3 class="name">Allan Tone<br><small class="function">CEO</small></h3>
                  <p class="description">A serial entrepreneur with several successful exits and a passion for helping small businesses with their problems. No answer?... Hiking or Skiing with Carol probably.</p>
                </div>
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="btn btn-md" id="button1" href="mailto:allan@dealeraddendums.com">Email Me </a> <br>&nbsp;
                </div>
              </div>
              <!-- End Block 1 -->
              <!-- Block 2 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_carol.jpg" alt="Carol Wetzel">
                </div>
                <div class="team-content">
                  <h3 class="name">Dr Carol Wetzel<br><small class="function">SVP Sales and Marketing</small></h3>
                  <p class="description">Also a serial entrepreneur. Got a problem worth solving? My day job is with Discovery Education where we are transforming education in America.</p>
                </div>
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="btn btn-md" id="button1" href="mailto:carol@dealeraddendums.com">Email Me </a> <br>&nbsp;
                </div>
              </div>
              <!-- End Block 2 -->
              <!-- Block 3 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_derek.jpg" alt="Derek Lewis">
                </div>
                <div class="team-content">
                  <h3 class="name">Derek Lewis<br><small class="function">Director Client Success</small></h3>
                  <p class="description">Based in LA, I make sure ALL our clients big and small are cared for with fanatical support and attention.</p>
                </div>
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="btn btn-md" id="button1" href="mailto:derek@dealeraddendums.com">Email Me </a> <br>&nbsp;
                </div>
              </div>
              <!-- End Block 3 -->
              <!-- Block 4 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_alex.jpg" alt="Member 4">
                </div>
                <div class="team-content">
                  <h3 class="name">Alex Suarez<br><small class="function">Sales and Marketing Executive</small></h3>
                  <p class="description">We have a KILLER application, don't you need it? When you are a hammer, everything looks like a nail, but really if you are still Excelling or Hand-writing addendums...shame on you.</p>
                </div>
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="btn btn-md" id="button1" href="mailto:alex@dealeraddendums.com">Email Me </a> <br>&nbsp;
                </div>
              </div>
              <!-- End Block 4 -->
              <!-- Block 5 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_amran.jpg" alt="Member 1">
                </div>
                <div class="team-content">
                  <h3 class="name">Amran Hossain<br><small class="function">Lead Programmer</small></h3>
                  <p class="description">Kick ass programmer. (PHP, Javascript, MySQL) I bring Allan's vision to life, live in Bangladesh and love to travel</p>
                </div>  
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="btn btn-md" id="button1" href="mailto:iam@shawon.info">Email Me </a> <br>&nbsp;
                </div>
              </div>
              <!-- End Block 5 -->
              
              
            </div>
          </div>
        </div>
      </section>
      <!-- END TEAM -->

      <!-- CONTACT -->
      <section class="contact" id="contact">
        <div class="container-fluid">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Communicate</h2>
            <h3 class="section-subtitle">Made with style in Salt Lake City, Utah</h3>
          </div>
          <!-- End Section Header -->
          <!-- Section Content -->
          <div class="r1 row">
            <div class="c1 col-md-12">
              <!-- Map accordion -->
              <div class="panel-group" id="contact-accordion-1">
                <div class="panel panel-default">
                  <h3 class="map-title">
                      <a data-toggle="collapse" data-parent="#contact-accordion-1" href="#contact-collapse-1" aria-expanded="true" aria-controls="contact-collapse-1"><span class="glyphicon glyphicon-map-marker"></span><br>Where</a>
                    </h3>
                  <div class="panel-collapse collapse in" id="contact-collapse-1">
                    <div class="panel-body">
                      <!-- Map -->
                      <div class="map-wrapper">
                        <!-- Here is where the map will be rendered -->
                        <div id="map-canvas"></div>
                        <!-- Address over the map -->
                        <address class="alert vcard">
                          <button class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                          <div class="org">
                            <strong>DealerAddendums Inc.</strong>
                          </div>
                          <div class="adr">
                            <span class="street-address">965 E. 900 S. #210</span>
                            <span class="locality">Salt Lake City</span>,
                            <span class="region">UT 84108</span> -
                            <span class="country-name">USA</span>
                          </div>
                          <div class="tel">
                            <span class="tel-1">+1 801-415-9435 Office</span>
                            <span class="tel-2">+1 800-231-7124 Free</span>
                            <span class="tel-2">+1 801-349-2538 Fax</span>
                          </div>
                          <div class="email">
                            <!-- Anti spam protection. Keep the codes: "&#x3a;" (= ":") / "&#x40;" (= "@") / "&#x2e;" (= ".") -->
                            <span class="email-contact"><a href="mailto&#x3a;support&#x40;dealeraddendums&#x2e;com" data-user="support" data-domain="dealeraddendums.com"></a></span>
                            <span class="email-support"><a href="mailto&#x3a;allan&#x40;dealeraddendums&#x2e;com" data-user="allan" data-domain="dealeraddendums.com"></a></span>
                          </div>
                        </address>
                        <!-- End Address over the map -->
                      </div>
                      <!-- End Map -->
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Map accordion -->
            </div>
          </div>
        </div>
        <!-- END CONTACT -->

        <!-- PRICING -->
        <section class="pricing col3" id="pricing">
          <div class="container">
            <!--- Section Header -->
            <div class="section-header">
              <h2 class="section-title">Start out on a FREE 30 day trial</h2>
              <h3 class="section-subtitle">Print 50 labels then choose to upgrade. Never a contract, always month to month.</h3>
            </div>
            <!--- End Section Header -->
            <!-- Section Content - Attached 3 Columns -->
            <div class="r1 row animateblock btt speed-1">
              <!--- Table 1 -->
              <div class="c1 col-sm-4 col-md-4">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="title">Trial/Manual Load</h3>
                  </div>
                  <div class="panel-body">
                    <!--- Price -->
                    <div class="plan-price">
                      <span class="price">$75</span>
                      <span class="per-month">/month</span>
                    </div>
                    <!--- Features -->
                    <ul class="list-unstyled plan-features">
                      <li>30 day trial gets 50 free prints</li>
                      <li>Print Addendums</li>
                      <li>Print Buyers Guides</li>
                      <li>Print Infosheets</li>
                      <li>Print QR Code Stickers</li>
                      <li>50 Free Labels</li>
                      <li>Single Vehicle Import</li>
                      <li>Excel Vehicle Import</li>
                      <li>Labels as Low as $.14 ea</li>
                      <li>4 Hour Email Support</li>
                    </ul>
                  </div>
                  <div class="panel-footer">
                    <a class="btn btn-block btn-lg btn-success signup" id="button2" href="#signup">Sign Up</a>
                  </div>
                </div>
              </div>
              <!--- End Table 1 -->
              <!-- Table 2 - Popular -->
              <div class="c2 col-sm-4 col-md-4 popular">
                <div class="panel panel-default panel-popular">
                  <div class="badge-popular"></div>
                  <div class="panel-heading">
                    <h3 class="title">Automatic - Web</h3>
                  </div>
                  <div class="panel-body">
                    <!--- Price -->
                    <div class="plan-price">
                      <span class="price">$125</span>
                      <span class="per-month">/month</span>
                    </div>
                    <!--- Features -->
                    <ul class="list-unstyled plan-features">
                      <li><orange>Multi-Rooftop Discounts</orange></li>
                      <li>Print Addendums</li>
                      <li>Print Buyers Guides</li>
                      <li>Print Infosheets</li>
                      <li>Print QR Code Stickers</li>
                      <li><orange>Automatic Inventory Import</orange></li>
                      <li>Single Vehicle Import</li>
                      <li>Excel Vehicle Import</li>
                      <li>Labels as Low as $.14 ea</li>
                      <li><orange>24/7 Customer Support</orange></li>
                    </ul>
                  </div>
                  <div class="panel-footer">
                    <a class="btn btn-block btn-lg btn-success signup" id="button2" href="#signup">Sign Up</a>
                  </div>
                </div>
              </div>
              <!--- End Table 2 -->
              <!--- Table 3 -->
              <div class="c3 col-sm-4 col-md-4">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="title">Automatic - DMS</h3>
                  </div>
                  <div class="panel-body">
                    <!--- Price -->
                    <div class="plan-price">
                      <span class="price">$175</span>
                      <span class="per-month">/month</span>
                    </div>
                    <!--- Features -->
                    <ul class="list-unstyled plan-features">
                      <li><orange>CDK | R&R | Autosoft</orange></li>
                      <li>Print Addendums</li>
                      <li>Print Buyers Guides</li>
                      <li>Print Infosheets</li>
                      <li>Print QR Code Stickers</li>
                      <li><orange>Direct DMS Import</orange></li>
                      <li>Single Vehicle Import</li>
                      <li>Excel Vehicle Import</li>
                      <li>Labels as Low as $.14 ea</li>
                      <li>24/7 Customer Support</li>
                    </ul>
                  </div>
                  <div class="panel-footer">
                    <a class="btn btn-block btn-lg btn-success signup" id="button2" href="#signup">Sign Up</a>
                  </div>
                </div>
              </div>
              <!--- End Table 3 -->
            </div>
            <!--- End Section Content -->
          </div>
        </section>
        <!-- END PRICING -->
s
        <!-- TRIAL SIGNUP -->
        <section class="team" id="signup">
          <div class="container-fluid">
              <!-- Section Header -->
              <div class="section-header">
                <h2 class="section-title">Sign Up Now</h2>
                <h3 class="section-subtitle">Sign up for a <orange>FREE ACCOUNT</orange>, then upgrade at any time ($75/Mo, $125/Mo, or $175/Mo)</h3>
              </div>
              	<!-- End Section Header -->
              	<div id="note1"></div>
              	<form id="ajax-contact-form" class="form" role="form">
		              <fieldset>
		                <div class="r1 row form-group">
		                  <div class="c1 col-md-12">
							           <label><h3><orange>Dealership Information</orange></h3></label>
									       <input class="form-control"  id="rcorners" type="text" name="dealer_name" value="" placeholder="Dealership Name *"/>
								      </div>
							      </div>
										
							      <div class="r2 row form-group">
								      <div class="c1 col-sm-4 col-md-4">
									      <select name="dealer_logo" class="form-control" >
  									  	<option value="" selected >Franchise Brand</option>
  									  	<option value="mylogo.jpg">Multiple/Custom</option>
  									  	<option value="Abarth.jpg">Abarth</option>
    										<option value="Acura.jpg">Acura</option>
    										<option value="Alfa-romeo.jpg">Alfa Romeo</option>
    										<option value="Aston-marton.jpg">Aston Marton</option>
    										<option value="Audi.jpg">Audi</option>
    										<option value="Bently.jpg<">Bently</option>
    										<option value="BMW.jpg">BMW</option>
    										<option value="Buick.jpg">Buick</option>
    										<option value="Cadillac.jpg">Cadillac</option>
    										<option value="Chevrolet.jpg">Chevrolet</option>
    										<option value="Chrysler.jpg">Chrysler</option>
    										<option value="Dodge.jpg">Dodge</option>
    										<option value="Ferrari.jpg">Ferrari</option>
    										<option value="Fiat.jpg">Fiat</option>
    										<option value="Fisker.jpg">Fisker</option>
    										<option value="Ford.jpg">Ford</option>
    										<option value="GMC.jpg">GMC</option>
    										<option value="Honda.jpg">Honda</option>
    										<option value="Hyundai.jpg">Hyundai</option>
    										<option value="Infinity.jpg">Infinity</option>
    										<option value="Jaguar.jpg">Jaguar</option> 
    										<option value="Jeep.jpg">Jeep</option>
    										<option value="Kia.jpg">Kia</option>
    										<option value="Lamborghini.jpg">Lamborghini</option>
    										<option value="Land-rover.jpg">Land Rover</option>
    										<option value="Lexus.jpg">Lexus</option>
    										<option value="Lincoln.jpg">Lincoln</option>
    										<option value="Lotus.jpg">Lotus</option>
    										<option value="Maserati.jpg">Maserati</option>
    										<option value="Maybach.jpg">Maybach</option>
    										<option value="Mazda.jpg">Mazda</option>
    										<option value="Mercedes-benz.jpg">Mercedes Benz</option>
    										<option value="Mercury.jpg">Mercury</option>
    										<option value="Mini.jpg">Mini</option>
    										<option value="Mitsubishi.jpg">Mitsubishi</option>
    										<option value="Nissan.jpg">Nissan</option>
    										<option value="Pontiac.jpg">Pontiac</option>
    										<option value="Porsche.jpg">Porsche</option>
    										<option value="Rolls-royce.jpg">Rolls Royce</option>
    										<option value="Rover.jpg">Rover</option>
    										<option value="Saab.jpg">Saab</option> 
    										<option value="Seat.jpg">Seat</option>
    										<option value="Smart.jpg">Smart</option>
    										<option value="Subaru.jpg">Subaru</option>
    										<option value="Suzuki.jpg">Suzuki</option>
    										<option value="Tesla.jpg">Tesla</option>
    										<option value="Toyota.jpg">Toyota</option>
    										<option value="Used.jpg">Used Only</option>
    										<option value="Volkswagon.jpg">Volskwagon</option>
    										<option value="Volvo">Volvo</option>
    									</select> 
    								</div>
								    <div class="c2 col-sm-4 col-md-4">
									    <input class="form-control" type="text" id="rcorners" name="dealer_group" value="" placeholder="Dealer Group"/>
								    </div>
								    <div class="c3 col-sm-4 col-md-4">
									    <input class="form-control" type="text" id="rcorners" name="dealer_phone" value="" placeholder="Dealership Phone"/>
								    </div>
							</div>

							      <div class="r3 row form-group">
		                  <div class="c1 col-md-12">
									    <input class="form-control" type="text" id="rcorners" name="address" value="" placeholder="Address *"/>
								    </div>
							</div>

							<div class="r4 row form-group">
								<div class="c1 col-sm-3 col-md-3">
									<input class="form-control" type="text" id="rcorners" name="city" value="" placeholder="City *"/>
								</div>
								<div class="c2 col-sm-3 col-md-3">
									<select name="state" id="form-location" class="form-control" >
										<option value="" selected >State/Province *</option>
										<option value="AL">Alabama</option>
										<option value="AK">Alaska</option>
										<option value="AZ">Arizona</option>
										<option value="AR">Arkansas</option>
										<option value="CA">California</option>
										<option value="CO">Colorado</option>
										<option value="CT">Connecticut</option>
										<option value="DE">Delaware</option>
										<option value="FL">Florida</option>
										<option value="GA">Georgia</option>
										<option value="HI">Hawaii</option>
										<option value="ID">Idaho</option>
										<option value="IL">Illinois</option>
										<option value="IN">Indiana</option>
										<option value="IA">Iowa</option>
										<option value="KS">Kansas</option>
										<option value="KY">Kentucky</option>
										<option value="LA">Louisiana</option>
										<option value="ME">Maine</option>
										<option value="MD">Maryland</option>
										<option value="MA">Massachusetts</option>
										<option value="MI">Michigan</option>
										<option value="MM">Minnesota</option>
										<option value="MS">Mississippi</option>
										<option value="MO">Missouri</option>
										<option value="MT">Montana</option>
										<option value="NE">Nebraska</option>
										<option value="NV">Nevada</option>
										<option value="NH">New Hampshire</option>
										<option value="NJ">New Jersey</option>
										<option value="NM">New Mexico</option>
										<option value="NY">New York</option>
										<option value="NC">North Carolina</option>
										<option value="ND">North Dakota</option>
										<option value="OH">Ohio</option>
										<option value="OK">Oklahoma</option>
										<option value="OR">Oregon</option>
										<option value="PA">Pennsylvania</option>
										<option value="RI">Rhode Island</option>
										<option value="SC">South Carolina</option>
										<option value="SD">South Dakota</option>
										<option value="TN">Tennessee</option>
										<option value="TX">Texas</option>
										<option value="UT">Utah</option>
										<option value="VT">Vermont</option>
										<option value="VA">Virginia</option>
										<option value="WA">Washington</option>
										<option value="WV">West Virginia</option>
										<option value="WI">Wisconsin</option>
										<option value="WY">Wyoming</option>
										<option value="">***Canada***</option>
										<option value="AB">AB - Alberta</option>
										<option value="BC">BC - British Columbia</option>
										<option value="MB">MB - Manitoba</option>
										<option value="NB">NB - New Brunswick</option>
										<option value="NL">NL - Newfoundland and Labrador</option>
										<option value="NT">NT - Northwest Territories</option>
										<option value="NS">NS - Nova Scotia</option>
										<option value="NU">NU - Nunavut</option>
										<option value="ON">ON - Ontario</option>
										<option value="PE">PE - Prince Edward Island</option>
										<option value="QC">QC - Quebec</option>
										<option value="SK">SK - Saskatchewan</option>
										<option value="YT">YT - Yukon</option>
									</select>
								</div>
								<div class="c3 col-sm-3 col-md-3">
									<input class="form-control" type="text" name="zip"  id="rcorners" value="" placeholder="Zip *"/>
								</div>
								<div class="c4 col-sm-3 col-md-3">
									<select name="country" id="form-location" class="form-control" >
										<option value=""></option><option value="AF">AFGHANISTAN</option><option value="AX">ALAND ISLANDS</option><option value="AL">ALBANIA</option><option value="DZ">ALGERIA</option><option value="AS">AMERICAN SAMOA</option><option value="AD">ANDORRA</option><option value="AO">ANGOLA</option><option value="AI">ANGUILLA</option><option value="AQ">ANTARCTICA</option><option value="AG">ANTIGUA / BARBUDA</option><option value="AR">ARGENTINA</option><option value="AM">ARMENIA</option><option value="AW">ARUBA</option><option value="AU">AUSTRALIA</option><option value="CX">AUSTRALIA (CHRISTMAS ISLANDS)</option><option value="CC">AUSTRALIA (COCOS KEELING ISLANDS)</option><option value="NF">AUSTRALIA (NORFOLK ISLANDS)</option><option value="AT">AUSTRIA</option><option value="AZ">AZERBAIJAN</option><option value="BS">BAHAMAS</option><option value="BH">BAHRAIN</option><option value="BD">BANGLADESH</option><option value="BB">BARBADOS</option><option value="BY">BELARUS</option><option value="BE">BELGIUM</option><option value="BZ">BELIZE</option><option value="BJ">BENIN</option><option value="BM">BERMUDA</option><option value="BT">BHUTAN</option><option value="BO">BOLIVIA</option><option value="XB">BONAIRE (NETHERLANDS ANTILLES)</option><option value="BA">BOSNIA / HERZEGOVINA</option><option value="BW">BOTSWANA</option><option value="BV">BOUVET ISLAND</option><option value="BR">BRAZIL</option><option value="IO">BRITISH INDIAN OCEAN TERRITORY</option><option value="BN">BRUNEI DARUSSALAM</option><option value="BG">BULGARIA</option><option value="BF">BURKINA FASO</option><option value="BI">BURUNDI</option><option value="KH">CAMBODIA</option><option value="CM">CAMEROON</option><option value="CA">CANADA</option><option value="IC">CANARY ISLANDS</option><option value="CV">CAPE VERDE</option><option value="KY">CAYMAN ISLANDS</option><option value="CF">CENTRAL AFRICAN REPUBLIC</option><option value="TD">CHAD</option><option value="CL">CHILE</option><option value="CN">CHINA</option><option value="CO">COLOMBIA</option><option value="KM">COMOROS</option><option value="CG">CONGO</option><option value="CD">CONGO, DEMOCRATIC REPUBLIC OF THE</option><option value="CK">COOK ISLANDS</option><option value="CR">COSTA RICA</option><option value="HR">CROATIA</option><option value="CU">CUBA</option><option value="XC">CURACAO</option><option value="CY">CYPRUS</option><option value="CZ">CZECH REPUBLIC</option><option value="DK">DENMARK</option><option value="DJ">DJIBOUTI</option><option value="DM">DOMINICA</option><option value="DO">DOMINICAN REPUBLIC</option><option value="TL">EAST TIMOR</option><option value="EC">ECUADOR</option><option value="EG">EGYPT</option><option value="SV">EL SALVADOR</option><option value="ER">ERITREA</option><option value="EE">ESTONIA</option><option value="ET">ETHIOPIA</option><option value="FK">FALKLAND ISLANDS (MALVINAS)</option><option value="FO">FAROE ISLANDS</option><option value="FJ">FIJI</option><option value="WF">FIJI (WALLIS / FUTUNA ISLANDS)</option><option value="FI">FINLAND</option><option value="FR">FRANCE</option><option value="GF">FRENCH GUIANA</option><option value="PF">FRENCH POLYNESIA</option><option value="TF">FRENCH SOUTHERN TERRITORIES</option><option value="GA">GABON</option><option value="GM">GAMBIA</option><option value="GE">GEORGIA</option><option value="DE">GERMANY</option><option value="GH">GHANA</option><option value="GI">GIBRALTAR</option><option value="GR">GREECE</option><option value="GL">GREENLAND</option><option value="GD">GRENADA</option><option value="GP">GUADELOUPE</option><option value="GU">GUAM</option><option value="GT">GUATEMALA</option><option value="GG">GUERNSEY</option><option value="GQ">GUINEA (EQUATORIAL GUINEA)</option><option value="GW">GUINEA BISSAU</option><option value="GN">GUINEA REPUBLIC (GUINEA)</option><option value="GY">GUYANA (BRITISH)</option><option value="HT">HAITI</option><option value="HM">HEARD ISLAND AND MCDONALD ISLANDS</option><option value="HN">HONDURAS</option><option value="HK">HONG KONG</option><option value="HU">HUNGARY</option><option value="IS">ICELAND</option><option value="IN">INDIA</option><option value="ID">INDONESIA</option><option value="IR">IRAN (ISLAMIC REPUBLIC OF)</option><option value="IQ">IRAQ</option><option value="IE">IRELAND, REPUBLIC OF (IRELAND)</option><option value="IL">ISRAEL</option><option value="IT">ITALY</option><option value="SM">ITALY (SAN MARINO)</option><option value="VA">ITALY (VATICAN CITY)</option><option value="CI">IVORY COAST</option><option value="JM">JAMAICA</option><option value="JP">JAPAN</option><option value="JE">JERSEY</option><option value="JO">JORDAN</option><option value="KZ">KAZAKHSTAN</option><option value="KE">KENYA</option><option value="KI">KIRIBATI</option><option value="KR">KOREA, REPUBLIC OF (KOREA SOUTH)</option><option value="KP">KOREA, THE D.P.R. OF (KOREA NORTH)</option><option value="KV">KOSOVO</option><option value="KW">KUWAIT</option><option value="KG">KYRGYZSTAN</option><option value="LA">LAOS</option><option value="LV">LATVIA</option><option value="LB">LEBANON</option><option value="LS">LESOTHO</option><option value="LR">LIBERIA</option><option value="LY">LIBYAN ARAB JAMAHIRIYA</option><option value="LI">LIECHTENSTEIN</option><option value="LT">LITHUANIA</option><option value="LU">LUXEMBOURG</option><option value="MO">MACAO</option><option value="MK">MACEDONIA, REPUBLIC OF (FYROM)</option><option value="MG">MADAGASCAR</option><option value="MW">MALAWI</option><option value="MY">MALAYSIA</option><option value="MV">MALDIVES</option><option value="ML">MALI</option><option value="MT">MALTA</option><option value="MH">MARSHALL ISLANDS</option><option value="MQ">MARTINIQUE</option><option value="MR">MAURITANIA</option><option value="MU">MAURITIUS</option><option value="YT">MAYOTTE</option><option value="MX">MEXICO</option><option value="FM">MICRONESIA, FEDERATED STATES OF</option><option value="MD">MOLDOVA, REPUBLIC OF</option><option value="MC">MONACO</option><option value="MN">MONGOLIA</option><option value="ME">MONTENEGRO</option><option value="MS">MONTSERRAT</option><option value="MA">MOROCCO</option><option value="MZ">MOZAMBIQUE</option><option value="MM">MYANMAR</option><option value="NA">NAMIBIA</option><option value="NR">NAURU, REPUBLIC OF</option><option value="NP">NEPAL</option><option value="NL">NETHERLANDS</option><option value="AN">NETHERLANDS ANTILLES</option><option value="XN">NEVIS</option><option value="NC">NEW CALEDONIA</option><option value="NZ">NEW ZEALAND</option><option value="NI">NICARAGUA</option><option value="NE">NIGER</option><option value="NG">NIGERIA</option><option value="NU">NIUE</option><option value="MP">NORTHERN MARIANA ISLANDS</option><option value="NO">NORWAY</option><option value="OM">OMAN</option><option value="PK">PAKISTAN</option><option value="PW">PALAU</option><option value="PS">PALESTINIAN TERRITORY, OCCUPIED</option><option value="PA">PANAMA</option><option value="PG">PAPUA NEW GUINEA</option><option value="PY">PARAGUAY</option><option value="PE">PERU</option><option value="PH">PHILIPPINES</option><option value="PN">PITCAIRN</option><option value="PL">POLAND</option><option value="PT">PORTUGAL</option><option value="PR">PUERTO RICO</option><option value="QA">QATAR</option><option value="RE">REUNION ISLANDS</option><option value="RO">ROMANIA</option><option value="RU">RUSSIAN FEDERATION (RUSSIA)</option><option value="RW">RWANDA</option><option value="PM">SAINT PIERRE AND MIQUELON</option><option value="WS">SAMOA</option><option value="ST">SAO TOME / PRINCIPE</option><option value="SA">SAUDI ARABIA</option><option value="SN">SENEGAL</option><option value="RS">SERBIA</option><option value="SC">SEYCHELLES</option><option value="SL">SIERRA LEONE</option><option value="SG">SINGAPORE</option><option value="SK">SLOVAKIA</option><option value="SI">SLOVENIA</option><option value="SB">SOLOMON ISLANDS</option><option value="SO">SOMALIA</option><option value="XS">Somaliland, Rep of (North Somalia)</option><option value="ZA">SOUTH AFRICA</option><option value="SH">SOUTH AFRICA (ST HELENA)</option><option value="GS">SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS</option><option value="SS">SOUTH SUDAN</option><option value="ES">SPAIN</option><option value="LK">SRI LANKA</option><option value="XY">ST BARTHELEMY</option><option value="XE">ST EUSTATIUS (NETHERLANDS ANTILLES)</option><option value="KN">ST KITTS</option><option value="LC">ST LUCIA</option><option value="XM">ST MAARTEN (NETHERLANDS ANTILLES)</option><option value="VC">ST VINCENT / GRENADINE</option><option value="SD">SUDAN</option><option value="SR">SURINAME (SURINAM)</option><option value="SJ">SVALBARD AND JAN MAYEN</option><option value="SZ">SWAZILAND</option><option value="SE">SWEDEN</option><option value="CH">SWITZERLAND</option><option value="SY">SYRIAN ARAB REPUBLIC</option><option value="TW">TAIWAN</option><option value="TJ">TAJIKISTAN</option><option value="TZ">TANZANIA, UNITED REPUBLIC OF</option><option value="TH">THAILAND</option><option value="TG">TOGO</option><option value="TK">TOKELAU</option><option value="TO">TONGA</option><option value="TT">TRINIDAD / TOBAGO</option><option value="TN">TUNISIA</option><option value="TR">TURKEY</option><option value="TM">TURKMENISTAN</option><option value="TC">TURKS / CAICOS ISLANDS</option><option value="TV">TUVALU</option><option value="UG">UGANDA</option><option value="UA">UKRAINE</option><option value="AE">UNITED ARAB EMIRATES</option><option value="GB">UNITED KINGDOM</option><option value="US" selected >UNITED STATES</option><option value="UM">UNITED STATES MINOR OUTLYING ISLANDS</option><option value="UY">URUGUAY</option><option value="UZ">UZBEKISTAN</option><option value="VU">VANUATU</option><option value="VE">VENEZUELA</option><option value="VN">VIETNAM</option><option value="VG">VIRGIN ISLANDS (BRITISH)</option><option value="VI">VIRGIN ISLANDS (USA)</option><option value="EH">WESTERN SAHARA</option><option value="YE">YEMEN</option><option value="ZM">ZAMBIA</option><option value="ZW">ZIMBABWE</option>
									</select>
								</div>
							</div>

							
							<div class="r5 row form-group"><label><h3><orange>User Information</orange></h3></label>
		                  		<div class="c1 col-md-12">
									<input class="form-control" type="text" id="rcorners" name="name" value="" placeholder="Your Name *"/>
								</div>
							</div>

							<div class="r6 row form-group">
								<div class="c1 col-sm-6 col-md-6">
									<input class="form-control" type="text" id="rcorners" name="email" value="" placeholder="Your Email *" />
								</div>
								<div class="c2 col-sm-6 col-md-6">
									<input class="form-control" type="text" id="rcorners" name="user_phone" value="" placeholder="Your Phone *"/>
								</div>
							</div>
							<div class="7 row form-group">
		                  		<div class="c1 col-sm-6 col-md-6">
									<select name="owner" id="form-location" class="form-control">
										<option value="" selected >Referred By *</option>
										<option value="Google">Google</option>
										<option value="Bing">Bing</option>
										<option value="Other Search">Other Search</option>
										<option value="Email">Email</option>
										<option value="Blog">Blog</option>
										<option value="A friend">A friend</option>
										<option value="Dealer Referral">Dealer Referral</option>
										<option value="1482180260">Derek Lewis</option>
										<option value="Alex">Alex Suarez</option>
										<option value="EZR">Your EZR Mobile Rep</option>
										<option value="AER Rep">Your AER Rep</option>
										<option value="Other">Other</option>
									</select>
								</div>
								<div class="c2 col-sm-6 col-md-6">
									<input class="form-control" type="text" name="repname" id="rcorners" value="" placeholder="Referal Details"/>
								</div>
							</div>
							<div class="form-inline">
								<label><h3>Username *</h3></label>
								<input class="form-control" type="text" name="username" id="rcorners" value=""  />
								<label><h3>Password *</h3></label>
								<input class="form-control" type="text" name="password" id="rcorners" value="" placeholder="At least 8 characters"  />
							</div>
							<hr>
							<div class="r7 row form-group">
                <div class="c1 col-sm-5 col-md-5">
                  <input type="submit" class="btn btn-block  btn-success btn-form marg-right5" id="button2" value="Free Sign Up" />
                </div>
                <div class="c2 col-sm-3 col-md-3">  
                  <input type="reset"  class="btn btn-block  btn-warning btn-form" value="Reset" />
                </div>
                <div class="c3 col-sm-2 col-md-2">
                  <button type="button" class="btn btn-block btn-default" id="button1" data-toggle="modal" data-target="#terms">
                    Terms of Use
                  </button>
                </div>
                <div class="c4 col-sm-2 col-md-2">
                  <button type="button" class="btn btn-block btn-default" id="button1" data-toggle="modal" data-target="#myModal">
                    Privacy Policy
                  </button>
                </div>
              <div id="note"></div>
              <div class="clear"></div>
                    </fieldset>
		            </form> 
              	</div>
              <!-- End Contact form -->
              <div class="alerts alerts-xs hidden-sm hidden-md hidden-lg alerts-contact"></div>
            </div> 
          </fieldset>         
        </section>
        <!-- End Section Content -->
      </dev>
      <!-- END SIGNUP -->

    </div>
    <!-- END MAIN -->


    <!-- FOOTER -->
    <footer class="footer">
      <div class="container">
        <div class="r1 row">
          <div class="c1 col-md-12">
            <div class="footer-logo"><img class="logo" src="images/websiteicon.png" width="105px" height="105px" alt="Logo"></div>
            <!-- Social Buttons -->
            <section class="social-icons">
              <h3 class="hide">Social Buttons</h3>
              <a class="btn-social facebook" data-toggle="tooltip" data-placement="top" title="Facebook" href="https://www.facebook.com/Addendums/" target="_blank"><span class="fa fa-facebook"></span></a>
              <a class="btn-social twitter" data-toggle="tooltip" data-placement="top" title="Twitter" href="https://twitter.com/dealeraddendums" target="_blank"><span class="fa fa-twitter"></span></a>
              <a class="btn-social youtube" data-toggle="tooltip" data-placement="top" title="Youtube" href="https://www.youtube.com/watch?v=HphjzVbLprQ" target="_blank"><span class="fa fa-youtube"></span></a>
              <a class="btn-social email" data-toggle="tooltip" data-placement="top" title="Email" href="mailto:support@dealeraddendums.com" target="_blank"><span class="fa fa-envelope"></span></a>
              
            </section>
            <!-- End Social Buttons -->
          </div>
        </div>
      </div>
      <div class="copyright">
        <div class="container">
          <div class="r1 row">
            <div class="c1 col-md-12">         
             &copy; 2014 - 2017 DealerAddendums Inc &nbsp;&nbsp; || &nbsp;&nbsp; Patent Pending &nbsp;&nbsp;||&nbsp;&nbsp; <?php echo RELEASE_VERSION; ?>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- END FOOTER -->

    <a class="go-top" href="#top"><span class="fa fa-chevron-up"></span></a>

  </div>
  <!-- END LAYOUT -->

  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script>
    window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')
  </script>

  <!-- Bootstrap -->
  <script src="js/bootstrap.min.js"></script>

  <!-- Stackable Menu -->
  <script src="js/vendor/stackable.js"></script>

  <!-- Retina Graphics -->
  <script src="js/vendor/retina.min.js"></script>

  <!-- Nicescroll Scrollbars -->
  <script src="js/vendor/jquery.nicescroll.min.js"></script>

  <!-- Circliful Circle Statistics -->
  <script src="js/vendor/circles.min.js"></script>

  <!-- Slick Carousel -->
  <script src="js/vendor/slick.min.js"></script>

  <!-- BlueImp Gallery Lightbox -->
  <script src="js/vendor/jquery.blueimp-gallery.min.js"></script>

  <!-- Mailchimp Ajax Subscription -->
  <script src="js/vendor/jquery.ajaxchimp.min.js"></script>

  <!-- Google Map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDR5LCzbUhJGUiv9LodJmcxroAeAmawtow"></script>
  <script src="js/google_map.min.js"></script>

  <!-- Placeholder Polyfill IE9 -->
  <script src="js/vendor/simple.min.js"></script>

  <!-- Default General Settings -->
  <script src="js/main.min.js"></script>

<script type="text/javascript"> 
  window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"a62da00697","applicationID":"17541974","transactionName":"M11QZEJSCBAEAkBZWAoXZ0JZHBEUElAbXVgXDx1kVV4WDwQVUR9eClxXSB5DDhM=","queueTime":0,"applicationTime":0,"atts":"HxpTEgpIGx4=","errorBeacon":"bam.nr-data.net","agent":""}</script>

  <!-- Intercom
  ================================= -->
  <script>
    window.Intercom("boot", {
        app_id: "7cda8fc1bfea25032a2ba95d5d862ecb86f8e976"
    });
    window.Intercom("update");
  </script>
  
  <!-- Google
  ================================= -->
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-46873878-1', 'auto');
    ga('send', 'pageview');
  </script>

  <!-- Print Notification
  ================================= -->
  <script>
    setInterval(function(){
      $.ajax({
          url: "map/get_values.php",
          type: "get",
        dataType: 'json',
        cache:false,
            success: function(html){
          if(html.addendums!=$("#addendums").val()){
            $("#addendums").val(html.addendums);
            $.ajax({
                url: "map/get_last_home.php",
                type: "get",
              dataType: 'json',
              cache:false,
                  success: function(myresponse){
                if(myresponse.id!=$("#last_data").val()){
                  var text1=myresponse.dealer+' just printed an addendum. <br>Click to see the addendum they printed for a:<br> <a href="app/assets/print/single-download.php?vid='+myresponse.id+'&printstart=1&viewpdf=yes" target="new"><strong>'+myresponse.make+' '+myresponse.model+'</strong></a>';
                  $('.bottom-right').notify({
                    message: { html: text1 },
                    fadeOut: { enabled: true, delay: 5000 },
                    type:'blackgloss'
                  }).show();
                  $("#new_link").val("app/assets/print/single-download.php?vid="+myresponse.id+"&printstart=1&viewpdf=yes");
                  $("#last_data").val(myresponse.id);           
                }
              }
            });
          }
        }
      });
    }, 10000);
    $( ".bottom-right" ).click(function() {
        window.open($("#new_link").val(), '_new');
    });
  </script>

  <!-- Order processing
	================================= -->
	<script type="text/javascript">
		$(document).ready(function(){	
			$("#ajax-contact-form").submit(function() {
				var str = $(this).serialize();		
				$.ajax({
					type: "POST",
					url: "contact_form/order_process.php",
					data: str,
					success: function(msg) {
						result = '';
						result1 = '';
						// Message Sent - Show the 'Thank You' message and hide the form
						if(msg == 'OK') {
							result1 = '<div class="notification_ok"><h3>Your Free account is now set up and you should receive a confirmation email in a minute or two. <br><br>Thank you!<br>Allan Tone - Founder & CEO</h3></div>';
							$("#fields").hide();
						} else {
							result = msg;
						}
						$('#note').html(result);
						$('#note1').html(result1);

					}
				});
				return false;
			});															
		});	
	</script>


	<!-- Open Modal
	================================= -->
	<script>
		$('#myModal').on('shown.bs.modal', function () {
		  	$('#myInput').focus()
		})
	</script>

  <!-- Modals - PRIVACY
    ================================= -->

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">DealerAddendums Inc.</h4>
              </div>
              <div class="modal-body">
            <h1>Privacy Policy</h1>
            <p>Last updated: August 09, 2017</p>
            <p>DealerAddendums Inc. ("us", "we", or "our") operates the http://dealeraddendums.com website (the "Service").</p>
            <p>This page informs you of our policies regarding the collection, use and disclosure of Personal and Business Information when you use our Service.</p>
            <p>We will not use or share your information with anyone except as described in this Privacy Policy.</p>
            <p><strong>We use your Personal Information for providing and improving the Service. By using the Service, you agree to the collection and use of information in accordance with this policy.</strong> Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our Terms of Use</p>
            <p><strong>Information Collection And Use</strong></p>
            <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to, your email address, name, phone number, postal address ("Personal Information").</p>
            <p>We collect this information for the purpose of providing the Service, identifying and communicating with you, responding to your requests/inquiries, servicing your purchase orders, and improving our services.</p>
            <p><strong>Log Data</strong></p>
            <p>We collect information that your browser sends whenever you visit our Service ("Log Data"). This Log Data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages and other statistics.</p>
            <p><strong>Cookies</strong></p>
            <p>Cookies are files with a small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and transferred to your device. We use cookies to collect information in order to improve our services for you.</p>
            <p>You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. The Help feature on most browsers provide information on how to accept cookies, disable cookies or to notify you when receiving a new cookie.</p>
            <p>If you do not accept cookies, you may not be able to use some features of our Service and we recommend that you leave them turned on.</p>
            <p><strong>Do Not Track Disclosure</strong></p>
            <p>We support Do Not Track ("DNT"). Do Not Track is a preference you can set in your web browser to inform websites that you do not want to be tracked.</p>
            <p>You can enable or disable Do Not Track by visiting the Preferences or Settings page of your web browser.</p>
            <p><strong>Service Providers</strong></p>
            <p>We may employ third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform Service-related services and/or to assist us in analyzing how our Service is used.</p>
            <p>These third parties have access to your Personal Information only to perform specific tasks on our behalf and are obligated not to disclose or use your information for any other purpose.</p>
            <p><strong>Compliance With Laws</strong></p>
            <p>We will disclose your Personal Information where required to do so by law or subpoena or if we believe that such action is necessary to comply with the law and the reasonable requests of law enforcement or to protect the security or integrity of our Service.</p>
            <p><strong>Security</strong></p>
            <p>The security of your Personal Information is important to us, and we strive to implement and maintain reasonable, commercially acceptable security procedures and practices appropriate to the nature of the information we store, in order to protect it from unauthorized access, destruction, use, modification, or disclosure.</p>
            <p>However, please be aware that no method of transmission over the internet, or method of electronic storage is 100% secure and we are unable to guarantee the absolute security of the Personal Information we have collected from you.</p>
            <p><strong>International Transfer</strong></p>
            <p>Your information, including Personal Information, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>
            <p>If you are located outside United States and choose to provide information to us, please note that we transfer the information, including Personal Information, to United States and process it there.</p>
            <p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>
            <p><strong>Links To Other Sites</strong></p>
            <p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.</p>
            <p>We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>
            <p><strong>Children's Privacy</strong></p>
            <p>Only persons age 18 or older have permission to access our Service. Our Service does not address anyone under the age of 13 ("Children").</p>
            <p>We do not knowingly collect personally identifiable information from children under 13. If you are a parent or guardian and you learn that your Children have provided us with Personal Information, please contact us. If we become aware that we have collected Personal Information from a children under age 13 without verification of parental consent, we take steps to remove that information from our servers.</p>
            <p><strong>Changes To This Privacy Policy</strong></p>
            <p>This Privacy Policy is effective as of June 02, 2015 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page.</p>
            <p>We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Your continued use of the Service after we post any modifications to the Privacy Policy on this page will constitute your acknowledgment of the modifications and your consent to abide and be bound by the modified Privacy Policy.</p>
            <p>If we make any material changes to this Privacy Policy, we will notify you either through the email address you have provided us, or by placing a prominent notice on our website.</p>
            <p><strong>Contact Us</strong></p>
            <p>If you have any questions about this Privacy Policy, please contact us.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
        </div>
    </div>



    <!-- Modals - Terms
    ================================= -->
    <div class="modal fade" id="terms" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">DealerAddendums Inc.</h4>
              </div>
            <div class="modal-body">
          <h1>Terms of Use ("Terms")</h1>
          <p>Last updated: August 09, 2017</p>
          <p>
          Please read these Terms of Use ("Terms", "Terms of Use") carefully before using the http://dealeraddendums.com website (the "Service") operated by DealerAddendums Inc. ("us", "we", or "our").<br><br>
          Your access to and use of the Service is conditioned upon your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who wish to access or use the Service.<br><br>
          <strong>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you do not have permission to access the Service.</strong><br><br>
          </p><p>
          <Strong>Accounts</Strong><br>
          All accounts with DealerAddendums Inc are month-to-month and there is no contract, or set up fee.
          <ul>
            <li>Free trial accounts are full featured accounts with manual data import and management and are limited to 50 vehicle prints.</li>
            <li>Manual Accounts are full featured accounts with manual data import and management and unlimited vehicle printing.</li>
            <li>Automatic DMS and Automatic Web accounts have all the Manual account features but add automatic inventory management through DMS or Web integration.</li></ul>
          </p><p>
          Manual and Automatic accounts are recurring subscription accounts (paid) and are billed on the monthly account creation anniversary. Bills are due on receipt, and are late at 37 days. Late accounts are automatically suspended until account is brought current.<br><br>
          Labels ordered through our system are shipped the next business day via USPS Priority Flat Rate and usually arrive in two days. The shipping and any taxes are included in the pricing and the cost of the ordered labels is added to your next invoice.<br><br>
          When you create an account, you guarantee that you are above the age of 18, and that the information you provide us is accurate, complete, and current at all times. Inaccurate, incomplete, or obsolete information may result in the immediate termination of your account on the Service.<br><br>
          You are responsible for maintaining the confidentiality of your account and password, including but not limited to the restriction of access to your computer and/or account. You agree to accept responsibility for any and all activities or actions that occur under your account and/or password, whether your password is with our Service or a third-party Single Sign On service. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.<br><br>
          You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trademark that is subject to any rights of another person or entity other than you, without appropriate authorization. You may not use as a username any name that is offensive, vulgar or obscene.
          We reserve the right to refuse service, terminate accounts, remove or edit content in our sole discretion.
          </p><p>
          <Strong>Communications</Strong><br>

          By creating an Account on our service, you agree to subscribe to newsletters, marketing or promotional materials and other information we may send. However, you may opt out of receiving any, or all, of these communications from us by following the unsubscribe link or instructions provided in any email we send.
          </p><p>
          <Strong>Content</Strong><br>

          Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material ("Content"). You are responsible for the Content that you post on or through the Service, including its legality, reliability, and appropriateness.<br><br>
          By posting Content on or through the Service, You represent and warrant that: (i) the Content is yours (you own it) and/or you have the right to use it and the right to grant us the rights and license as provided in these Terms, and (ii) that the posting of your Content on or through the Service does not violate the privacy rights, publicity rights, copyrights, contract rights or any other rights of any person or entity. We reserve the right to terminate the account of anyone found to be infringing on a copyright.<br><br>
          You retain any and all of your rights to any Content you submit, post or display on or through the Service and you are responsible for protecting those rights. We take no responsibility and assume no liability for Content you or any third party posts on or through the Service. However, by posting Content using the Service you grant us the right and license to use, modify, publicly perform, publicly display, reproduce, and distribute such Content on and through the Service.<br><br>
          DealerAddendums Inc has the right but not the obligation to monitor and edit all Content provided by users.<br><br>
          In addition, Content found on or through this Service are the property of DealerAddendums Inc or used with permission. You may not distribute, modify, transmit, reuse, download, repost, copy, or use said Content, whether in whole or in part, for commercial purposes or for personal gain, without express advance written permission from us.
          </p><p>
          <Strong>Links To Other Web Sites</Strong><br>

          Our Service may utilize third party web sites or services that are not owned or controlled by DealerAddendums Inc.<br><br>
          DealerAddendums Inc has no control over, and assumes no responsibility for the content, privacy policies, or practices of any third party web sites or services. We do not warrant the offerings of any of these entities/individuals or their websites.<br><br>
          You acknowledge and agree that DealerAddendums Inc shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such third party web sites or services.<br><br>
          We strongly advise you to read the terms and conditions and privacy policies of any third party web sites or services that you visit.
          </p><p>
          <Strong>Termination</Strong><br><br>

          <strong>DealerAddendums does NOT prorate the current invoice for terminated accounts.</strong><br><br>
          If you wish to terminate your account, you may simply log in and convert your account to the "Free" plan and pay any outstanding invoices. You can also send an email with your termination request to allan@dealeraddendums.com. <br><br>
          We may terminate or suspend your account and bar access to the Service immediately, without prior notice or liability, under our sole discretion, for any reason whatsoever and without limitation, including but not limited to a breach of the Terms.<br><br>
          All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.
          </p><p>
          <Strong>Indemnification</Strong><br>

          You agree to defend, indemnify and hold harmless DealerAddendums Inc and its licensee and licensors, and their employees, contractors, agents, officers and directors, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees), resulting from or arising out of a) your use and access of the Service, by you or any person using your account and password; b) a breach of these Terms, or c) Content posted on the Service.
          </p><p>
          <strong>Limitation Of Liability</strong><br>

          In no event shall DealerAddendums Inc, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.
          </p><p>
          <Strong>Disclaimer</Strong><br>

          Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and "AS AVAILABLE" basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance.<br><br>
          DealerAddendums Inc its subsidiaries, affiliates, and its licensors do not warrant that a) the Service will function uninterrupted, secure or available at any particular time or location; b) any errors or defects will be corrected; c) the Service is free of viruses or other harmful components; or d) the results of using the Service will meet your requirements.
          </p><p>
          <Strong>Exclusions</Strong><br>

          Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or limitation of liability for consequential or incidental damages, so the limitations above may not apply to you.
          </p><p>
          <Strong>Governing Law </Strong><br>

          These Terms shall be governed and construed in accordance with the laws of Utah, United States, without regard to its conflict of law provisions.<br><br>
          Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have had between us regarding the Service.
          </p><p>
          </Strong>Changes</Strong><br>

          We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material, we will provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.<br><br>
          By continuing to access or use our Service after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Service.
          </p><p>
          </Strong>Contact Us</strong><br>

          If you have any questions about these Terms, please contact us.
          </p>
            </div>
            <div class="modal-footer">
                <input type="hidden" id="addendums" value="" />
                <input type="hidden" value="" id="last_data" />
                <input type="hidden" value="" id="new_link" />
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
  </div>
<div class='notifications bottom-right'></div>

</body>
</html>
