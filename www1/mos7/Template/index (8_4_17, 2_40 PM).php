
<!DOCTYPE html>
<html lang="en">

<head>
  <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" CONTENT="New vehicle addendums, addendum software and addendum templates. Printing addendums has never been easier than with DealerAddendums.com">
  <meta name="keywords" CONTENT="dealer addendums, automotive addendum, auto dealer stickers, car dealership labels, addendum templates, window stickers, addendum labels, addendum software, buyers guides, information sheets,  VIN Decoder, CDK, 3PA">
  <meta name="copyright" CONTENT="Copyright © 2017 DealerAddendums.com. All Rights Reserved.">
  <meta name="author" CONTENT="Allan Tone">
  <meta name="language" CONTENT="English">

  <meta name="robots" content="index,follow">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-title" content="Addendum System for Car Dealers | DealerAddendums Inc.">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="theme-color" content="#3c7cc4">
  <title>Addendum System for Car Dealers | DealerAddendums Inc.</title>

  <!-- Facebook -->
  <meta property="og:locale" content="en_US">
  <meta property="og:url" content="http://www.dealeraddendums.com">
  <meta property="og:title" content="Addendum System for Car Dealers | DealerAddendums Inc.">
  <meta property="og:site_name" content="DealerAddendums Inc">
  <meta property="og:description" content=New vehicle addendums, addendum software and addendum templates. Printing addendums has never been easier than with DealerAddendums.com">
  <meta property="og:image" content="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-72x72.png">
  <meta property="og:image:type" content="image/png">
  <meta property="og:image:width" content="80">
  <meta property="og:image:height" content="80">
  <meta property="og:type" content="website">

  <!-- Fav and Touch Icons -->
  <meta name="msapplication-TileColor" content="#d9534f">
  <meta name="msapplication-TileImage" content="images/favicons/mstile-144x144.png">
  <meta name="msapplication-config" content="images/favicons/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <link rel="apple-touch-icon" sizes="57x57" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="152x152" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="images/favicons/manifest.json">
  <link rel="shortcut icon" href="http://d14bqw9lzcvxs9.cloudfront.net/icons/favicon.ico">

  <!-- Bootstrap -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!--=== FONT ICONS ===-->
    <!-- Font Awesome -->
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- IonIcons -->
    <link href="assets/ionicons/css/ionicons.min.css" rel="stylesheet">

    <!-- Elegant Icons -->
    <link href="assets/elegant-icons/css/elegant-icons.min.css" rel="stylesheet">
    <!--[if lte IE 7]><script src="assets/elegant-icons/js/elegant-icons-lte-ie7.min.js"></script><![endif]-->

  <!--=== VENDORS ===-->
    <!-- Slick Carousel -->
    <link href="css/vendor/slick.min.css" rel="stylesheet">
    <link href="css/vendor/slick-theme.min.css" rel="stylesheet">

    <!-- Blueimp Gallery Lightbox -->
    <link href="css/vendor/blueimp-gallery.min.css" rel="stylesheet">

  <!-- Custom -->
  <link href="css/main.min.css" rel="stylesheet">
  <link href="css/style.min.css" rel="stylesheet">
  <link href="css/animation.min.css" rel="stylesheet">
  <link href="css/responsive.min.css" rel="stylesheet">
  <link href="css/typography.min.css" rel="stylesheet">
  <link href="css/colors/default.min.css" rel="stylesheet">
  <link href="css/colors/orange.css" rel="stylesheet">

  <!--Inline styles  -->
  <style>
    orange{
      color: orange;
      font-weight: bold;
    }
    .stats {
  position: relative;
  z-index: 0;
  padding: 50px 0 64px;
  background: url(http://d14bqw9lzcvxs9.cloudfront.net/dealership_blurred_dark.jpg) no-repeat center center;
  background-attachment: fixed;
  background-size: cover;
}
  </style>

  <?php 
      //INCLUDE FILES 
      include("../../../../version/version.php");
      
      // CONNECT TO DATABASE 
      include("../../../app/assets/config/db.php");
      $count_sql=mysqli_query($db,"select*from count_table where id=1");
      $count=mysqli_fetch_array($count_sql);
      // COUNTER CALCULATIONS   
      // DEALER COUNT
      $cu_dealer=mysqli_query($db,"SELECT count(*) as total from dealer_dim where ACTIVE='Yes'");
      $count_dealer=mysqli_fetch_array($cu_dealer);
      
      // TODAYS PRINT COUNT
      $dateta=date("Y-m-d");
      $todays=$count['print_today'];

      // TOTAL PRINT COUNT
      // CURRENT VEHICLES
      //$pr_addendum=mysqli_query($db,"SELECT count(*) as total from dealer_inventory where PRINT_FLAG='1'");
      $printed_addendum=$count['printed'];
      // OLD VEHICLES
      //$pr_addendum_old=mysqli_query($db,"SELECT count(*) as total from z_dealer_inventory_inactive where PRINT_FLAG='1'");
      $printed_addendum_old=$count['old_addendum'];
      // TOTAL VENICLES
      $total_prints = number_format($printed_addendum + $printed_addendum_old + 460000);
    ?>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!--[if gt IE 8]>
    <link href="css/ie9.min.css" rel="stylesheet">
  <![endif]-->
</head>

<body class="btn-circle sld-transition-2" id="top" data-spy="scroll" data-target=".navbar">

  <!-- Preloader -->
  <div class="preloader">
    <div class="status"></div>
  </div>
  <!-- End Preloader -->
          
  <!-- Modal -->
  <div class="modal fade modal-full" id="modal" tabindex="-1" role="dialog" aria-labelledby="title-modal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
      </div>
    </div>
  </div>
  <!-- End Modal -->


  <!-- LAYOUT -->
  <div class="layout">

    <!-- HEADER -->
    <header>

      <!-- SLIDESHOW -->
      <section class="slideshow">
        <div class="container-fluid">
          <div class="r1 row">
            <div class="c1 col-md-12">
              <div class="carousel slide" id="slideshow-carousel-1" data-ride="carousel">

                <!-- Wrapper for Slides -->
                <div class="carousel-inner" role="listbox">

                  <!-- Slide 3 -->
                  <div class="item item-3 slide-image active">
                    <img class="img-responsive bg-item parallax" data-speed="1" src="http://d14bqw9lzcvxs9.cloudfront.net/dealership_blurred_dark.jpg" alt="DealerAddendums Inc.">
                    <div class="carousel-caption">
                      <div class="c4 col-sm-5 col-md-5">
                        <img class="img-responsive center-block" src="http://d14bqw9lzcvxs9.cloudfront.net/addendums/<?php echo rand(1, 10); ?>.png" alt="Image">
                      </div>
                      <div class="c5 col-xs-12 col-sm-7 col-md-7">
                        <h2 class="title">Create, Manage and Print Addendums Online.
                        <img class="img-responsive center-block" src="http://d14bqw9lzcvxs9.cloudfront.net/horizontal_stacked_website.png"></h2>
                        <a class="btn btn-lg btn-danger btn-download" href="#signup">Sign up for a free trial</a>
                      </div>
                    </div>
                  </div>
                  <!-- End Slide 3 -->

                </div>
                <!-- End Wrapper for Slides -->

              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- END SLIDESHOW -->

      <!-- NAVBAR -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and Toggle -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href=""><img class="logo" src="images/logo.png" alt="Logo"></a>
          </div>
          <!-- End Brand and Toggle -->
          <!-- Menu -->
          <div class="collapse navbar-collapse" id="navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active" data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#top">Home</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#overview">Overview</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#features">Features</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1"> 
                <a href="#numbers">Our Numbers</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#examples">Examples</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#testimonials">Testimonials</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#pricing">Pricing</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#team">Team</a>
              </li>
               <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#contact">Contact Us</a>
              </li>
              <li data-toggle="collapse" data-target="#navbar-collapse-1">
                <a href="#signup">Free Trial</a>
              </li>
              <li class="dropdown li-more">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">+</a>
                <ul class="dropdown-menu" id="overflow"></ul>
              </li>
            </ul>
          </div>
          <!-- End Menu -->
        </div>
      </nav>
      <!-- END NAVBAR -->

    </header>
    <!-- END HEADER -->


    <!-- MAIN -->
    <div class="main">

      <!-- OVERVIEW -->
      <section class="overview" id="overview">
        <div class="container">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Overview</h2>
             <h3 class="section-subtitle">DealerAddendums.com is an online subscription service for new and used vehicle dealers. The system allows dealers to print New Vehicle Addendums, Buyers Guides, and Info Sheets in seconds.</h3>
          </div>
          <!-- End Section Header -->
          <!-- Section Content -->
          <div class="r1 row">
            <div class="c1 col-md-12">
              <!-- Nav Tabs -->
              <ul class="nav nav-justified nav-tabs" role="tablist">
                <li>
                  <a class="btn-lg" data-toggle="tab" href="#overview-tab-2">Customize</a>
                </li>
                <li class="active">
                  <a class="btn-lg" data-toggle="tab" href="#overview-tab-1">Create in seconds</a>
                </li>
                <li>
                  <a class="btn-lg" data-toggle="tab" href="#overview-tab-3">Explainer Video</a>
                </li>
              </ul>
              <!-- End Nav Tabs -->
              <!-- Tab Panes -->
              <div class="tab-content">
                <!-- Tab Pane 1 -->
                <div class="tab-pane fade in active" id="overview-tab-1">
                  <div class="r1 row">
                    <!-- Block 1 -->
                    <div class="c1 col-sm-6 col-md-6 col-sm-push-6">
                      <div class="img-perspective">
                        <img class="img-responsive center-block img-1" src="images/overview/overview_left_3.png" alt="Image">
                        <img class="img-responsive center-block img-2" src="images/overview/overview_left_2.png" alt="Image">
                        <img class="img-responsive center-block img-3" src="images/overview/overview_left_1.png" alt="Image">
                      </div>
                    </div>
                    <!-- End Block 1 -->
                    <!-- Block 2 -->
                    <div class="c2 col-sm-6 col-md-6 col-sm-pull-6">
                      <div class="overview-wrapper">
                        <div class="overview-content">
                          <h3 class="title">Create Addendums<br><small class="subtitle">Addendums in Seconds</small></h3>
                          <p><orange>1. Select Vehicle(s) :</orange> Select your vehicle, or vehicles. You can search by stock, VIN or model.</p>
                          <p><orange>2. Add Options :</orange> Use the Default options to automatically add options based on Model or Bodystyle, or choose from a dropdown list of common options</p>
                          <p><orange>3. Print :</orange> Print one at a time or use the batch function to print up to 50 at a time. Print using ANY laser printer.</p>
                          
                        </div>
                      </div>
                    </div>
                    <!-- End Block 2 -->
                  </div>
                </div>
                <!-- End Tab Pane 1 -->
                <!-- Tab Pane 2 -->
                <div class="tab-pane fade" id="overview-tab-2">
                  <div class="r1 row">
                    <!-- Block 1 -->
                    <div class="c1 col-sm-4 col-md-4 col-sm-push-4 animateblock size-img speed-2">
                      <div class="img-wireframe">
                        <img class="img-responsive center-block img-4" src="images/overview/overview_center_1.png" alt="Image">
                        <img class="img-responsive center-block img-5" src="images/overview/overview_center_2.png" alt="Image">
                      </div>
                    </div>
                    <!-- End Block 1 -->
                    <!-- Block 2 -->
                    <div class="c2 col-sm-4 col-md-4 col-sm-pull-4 animateblock ltr-2 speed-3">
                      <div class="overview-wrapper">
                        <div class="hidden-sm overview-icon"><span class="ion-social-html5"></span>
                        </div>
                        <div class="overview-content">
                          <h3 class="title">Dealership Logo</h3>
                          <p class="description">Upload your dealership, or group logo in fill high resolution color.</p>
                        </div>
                      </div>
                      <div class="overview-wrapper">
                        <div class="hidden-sm overview-icon"><span class="glyphicon glyphicon-cog"></span>
                        </div>
                        <div class="overview-content">
                          <h3 class="title">Inventory data</h3>
                          <p class="description">We can pull from your website, or DMS and you choose what specific vehicle info to print.</p>
                        </div>
                      </div>
                      <div class="overview-wrapper">
                        <div class="hidden-sm overview-icon"><span class="glyphicon glyphicon-eye-open"></span>
                        </div>
                        <div class="overview-content">
                          <h3 class="title">Custom Footer</h3>
                          <p class="description">Choose from EPA, QR Code, VIN Barcode, product marketing, or upload your own image.</p>
                        </div>
                      </div>
                    </div>
                    <!-- End Block 2 -->
                    <!-- Block 3 -->
                    <div class="c3 col-sm-4 col-md-4 animateblock rtl-2 speed-3">
                      <div class="overview-wrapper">
                        <div class="hidden-sm overview-icon"><span class="ion-social-buffer"></span>
                        </div>
                        <div class="overview-content">
                          <h3 class="title">Vehicle Options</h3>
                          <p class="description">Enter your options once and have them auto applied to specific vehicles or choose them from a dropdown. We make it easy to print one or a hunders addendums at a time.</p>
                        </div>
                      </div>
                      <div class="overview-wrapper">
                        <div class="hidden-sm overview-icon"><span class="glyphicon glyphicon-book"></span>
                        </div>
                        <div class="overview-content">
                          <h3 class="title">Customization</h3>
                          <p class="description">Watermarks, border colors, special fonts, you name it, we can make it happen.</p>
                        </div>
                      </div>
                      <div class="overview-wrapper">
                        <div class="hidden-sm overview-icon"><span class="glyphicon glyphicon-headphones"></span>
                        </div>
                        <div class="overview-content">
                          <h3 class="title">Personal Support</h3>
                          <p class="description">Don't worry about learning all the ins and outs of customization, we'll do it all for you... for free.</p>
                        </div>
                      </div>
                    </div>
                    <!-- End Block 3 -->
                  </div>
                </div>
                <!-- End Tab Pane 2 -->
                <!-- Tab Pane 3 -->
                <div class="tab-pane fade" id="overview-tab-3">
                  <div class="r1 row">
                    <!-- Block 1 -->
                    <div class="c1 col-sm-6 col-md-6">
                      <div class="img-slide">
                        <script src="//content.jwplatform.com/players/fzyhnJaP-X7R8yf1M.js"></script><br><br>
                        <script src="//content.jwplatform.com/players/I00yDUPC-X7R8yf1M.js"></script>
                      </div>
                    </div>
                    <!-- End Block 1 -->
                    <!-- Block 2 -->
                    <div class="c2 col-sm-6 col-md-6">
                      <div class="overview-content">
                        <h3 class="title">User-friendly Application<br><small class="subtitle">500+ dealerships</small></h3>
                        <p>We have been doing addendums since 2014 and have over 500 happy dealerships.</p>
                        <p>The videos to the left show what we do, and the problems we solve. Take a look and remember you can always call us to answer any specific questions.</p>
                      </div>
                    </div>
                    <!-- End Block 2 -->
                  </div>
                </div>
                <!-- End Tab Pane 3 -->
              </div>
              <!-- End Tab Panes -->
            </div>
          </div>
          <!-- End Section Content -->
        </div>
      </section>
      <!-- END OVERVIEW -->



      <!-- FEATURES -->
      <section class="features" id="features">
        <div class="container">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Features</h2>
            <h3 class="section-subtitle">NO CONTRACT, MONTH-TO-MONTH, AND FREE FOR 50 VEHICLES</h3>
          </div>
          <!-- End Section Header -->
          <!-- Section Content -->
          <div class="r1 row animateblock btt speed-1">
            <div class="c1 col-md-12" id="features-carousel-1">
              <!-- Block 1 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_genius"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">Vehicle Addendums</h3>
                  <p class="description">Print beautiful, full color or black and white, addendums in seconds for as little as $0.14 a piece. Add your dealer installed options to increase your profits!</p>
                </div>
              </div>
              <!-- End Block 1 -->
              <!-- Block 2 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_cloud-download"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">Automatic Web</h3>
                  <p class="description">The Automatic Web Plan updates your inventory every night with data from your website or syndication provider. No more manual data entry!</p>
                </div>
              </div>
              <!-- End Block 2 -->
              <!-- Block 3 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_cloud-download"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">Automatic DMS</h3>
                  <p class="description">The Automatic DMS Plan updates your inventory automatically with your DMS data we receive from DealerVault or direct from CDK. No manual data entry and your inventory shows up FAST!</p>
                </div>
              </div>
              <!-- End Block 3 -->
              <!-- Block 4 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_gift_alt"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">Vehicle Options</h3>
                  <p class="description">Apply options to a vehicle addendum based on preset defaults. Options can be manually added or automatically added based on model and/or bodystyle.</p>
                </div>
              </div>
              <!-- End Block 4 -->
              <!-- Block 5 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_document"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">Info Sheets</h3>
                  <p class="description">Easily print Pre-Owned Vehicle Information Sheets, with your logo and vehicle details, description, options, QR code, and optional PureCars integration.</p>
                </div>
              </div>
              <!-- End Block 5 -->
              <!-- Block 6 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_document"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">Buyers Guides</h3>
                  <p class="description">As-Is, Implied, or Warranty Buyers Guides in English and Spanish can be printed in seconds with the click of your mouse.</p>
                </div>
              </div>
              <!-- End Block 6 -->
              <!-- Block 7 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_globe-2"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">A SaaS Application</h3>
                  <p class="description">DealerAddendums is a web based solution, so there is no software to install, and you are always working on the latest version.</p>
                </div>
              </div>
              <!-- End Block 7 -->
              <!-- Block 8 -->
              <div class="features-wrapper">
                <div class="features-icon"><span class="icon_error-oct_alt"></span>
                </div>
                <div class="features-content">
                  <h3 class="title">NO CONTRACTS</h3>
                  <p class="description">There are never any contracts, set up or cancellation fees. Use the system for as long as it's a fit, cancel when it's not.</p>
                </div>
              </div>
              <!-- End Block 8 -->
            </div>
          </div>
          <!-- End Section Content -->
        </div>
      </section>
      <!-- END FEATURES -->

      <!-- STATS -->
      <section class="stats hidden-xs" id="numbers">
        <div class="bg-section">
          <!-- <img src="http://d14bqw9lzcvxs9.cloudfront.net/icons/apple-touch-icon-72x72.png" alt="Stats Section Background"> -->
        </div>
        <div class="container">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Realtime Numbers <a href="http://dealeraddendums.com/map" target="_blank">(Map)</a></h2>
            <h3 class="section-subtitle">HUNDREDS OF THOUSANDS PRINTED IN OVER 500 DEALERSHIPS!</h3>
          </div>
          <!-- End Section Header -->
          <!-- Section Content -->
          <div class="container">


          <div class="numbers-row row">

            <!-- NUMBERS - ITEM 1 -->
            <div class="col-md-3 col-sm-6">
              <div class="numbers-item">
                <div class="numbers-item-counter"><span class="counter-up"><?php echo $total_prints?></span></div>
                <div class="numbers-item-caption">Addendums Printed</div>
              </div>
            </div>

            <!-- NUMBERS - ITEM 2 -->
            <div class="col-md-3 col-sm-6">
              <div class="numbers-item">
                <div class="numbers-item-counter"><span class="counter-up"><?php echo $todays;?></span></div>
                <div class="numbers-item-caption">Addendums Printed Today</div>
              </div>
            </div>

            <!-- NUMBERS - ITEM 3 -->
            <div class="col-md-3 col-sm-6">
              <div class="numbers-item">
                <div class="numbers-item-counter"><span class="counter-up">33.1</span>Sec.</div>
                <div class="numbers-item-caption">Average Print Time</div>
              </div>
            </div>

            <!-- NUMBERS - ITEM 4 -->
            <div class="col-md-3 col-sm-6">
              <div class="numbers-item">
                <div class="numbers-item-counter"><span class="counter-up"><?php echo $count_dealer['total']; ?></span> <span class="fa fa-coffee"></span></div>
                <div class="numbers-item-caption">Number of Dealers</div>
              </div>
            </div>

          </div>

        </div>
          <!-- End Section Content -->
        </div>
      </section>
      <!-- END STATS -->

      <!-- EXAMPLES -->
      <section class="gallery" id="examples">
        <div class="container">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Examples</h2>
            <h3 class="section-subtitle">Lorem ipsum per fames turpis tempor elementum felis venenatis, odio vivamus sagittis massa platea</h3>
          </div>
          <!-- End Section Header -->
          <!-- Section Content -->
          <div class="r1 row animateblock btt speed-1">
            <div class="c1 col-md-12" id="gallery-carousel-1">
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_1.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_1.jpg" alt="Screenshot 1">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_2.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_2.jpg" alt="Screenshot 2">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_3.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_3.jpg" alt="Screenshot 3">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_4.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_4.jpg" alt="Screenshot 4">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_1.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_1.jpg" alt="Screenshot 5">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_2.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_2.jpg" alt="Screenshot 6">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_3.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_3.jpg" alt="Screenshot 7">
                </a>
              </div>
              <div class="thumb-overlay">
                <a class="link-modal" data-gallery href="images/gallery/img_4.jpg">
                  <img class="img-responsive thumb" src="images/gallery/img_4.jpg" alt="Screenshot 8">
                </a>
              </div>
            </div>
          </div>
          <!-- End Section Content -->
          <!-- Modal -->
          <div class="blueimp-gallery blueimp-gallery-controls" id="blueimp-gallery">
            <div class="slides"></div>
            <a class="prev"><span class="fa fa-angle-left"></span></a>
            <a class="next"><span class="fa fa-angle-right"></span></a>
            <a class="close">×</a>
          </div>
          <!-- End Modal -->
        </div>
      </section>
      <!-- END GALLERY -->

      <!-- TESTIMONIALS -->
      <section class="testimonials" id="testimonials">
        <div class="container">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Testimonials</h2>
            <h3 class="section-subtitle">Lorem ipsum per fames turpis tempor elementum felis venenatis, odio vivamus sagittis massa platea</h3>
          </div>
          <!-- End Section Header -->
          <!-- Section Content -->
          <div class="r1 row animateblock btt speed-1">
            <div class="c1 col-md-12">
              <div class="carousel slide fade" id="testimonials-carousel-1" data-ride="carousel">
                <!-- Controls -->
                <div class="testimonials-controls">
                  <a class="arrow-left" data-slide="prev" href="#testimonials-carousel-1" role="button"><span class="fa fa-angle-left"></span></a>
                  <a class="arrow-right" data-slide="next" href="#testimonials-carousel-1" role="button"><span class="fa fa-angle-right"></span></a>
                </div>
                <!-- End Controls -->
                <!-- Wrapper for Slides -->
                <div class="carousel-inner" role="listbox">
                  <!-- Testimonial 1 -->
                  <div class="item active">
                    <div class="testimonials-photo">
                      <img src="images/testimonials/testimonial_1.jpg" alt="Testimonial 1">
                    </div>
                    <div class="testimonials-info">
                      <cite class="author" title="Source Title 1">Jack Smith</cite>
                      <a class="link-company" href="#" target="_blank">Company Example 1</a>
                    </div>
                    <blockquote>
                      <p>Lorem ipsum feugiat ad habitasse fermentum a venenatis interdum cubilia urna, consequat cubilia euismod a purus mattis facilisis pulvinar et, ornare egestas vehicula feugiat nostra eget nostra elit habitant.</p>
                    </blockquote>
                  </div>
                  <!-- End Testimonial 1 -->
                  <!-- Testimonial 1 -->
                  <div class="item">
                    <div class="testimonials-photo">
                      <img src="images/testimonials/testimonial_2.jpg" alt="Testimonial 2">
                    </div>
                    <div class="testimonials-info">
                      <cite class="author" title="Source Title 2">Janice Smith</cite>
                      <a class="link-company" href="#" target="_blank">Company Example 2</a>
                    </div>
                    <blockquote>
                      <p>Nam dapibus sit est sem ut enim inceptos diam, tincidunt porttitor platea eleifend fringilla malesuada elit litora enim, aliquet curabitur ultricies quisque vulputate luctus fusce ultricies odio conubia egestas imperdiet donec nec dictum justo.</p>
                    </blockquote>
                  </div>
                  <!-- End Testimonial 2 -->
                  <!-- Testimonial 2 -->
                  <div class="item">
                    <div class="testimonials-photo">
                      <img src="images/testimonials/testimonial_3.jpg" alt="Testimonial 3">
                    </div>
                    <div class="testimonials-info">
                      <cite class="author" title="Source Title 3">James Smith</cite>
                      <a class="link-company" href="#" target="_blank">Company Example 3</a>
                    </div>
                    <blockquote>
                      <p>Cubilia phasellus massa diam sed tincidunt elit etiam posuere habitasse netus molestie, congue at nam semper ultrices laoreet sapien eleifend libero nec odio felis, donec neque himenaeos risus a convallis pulvinar posuere odio feugiat.</p>
                    </blockquote>
                  </div>
                  <!-- End Testimonial 3 -->
                </div>
                <!-- End Wrapper for Slides -->
              </div>
            </div>
          </div>
          <!-- End Section Content -->
        </div>
      </section>
      <!-- END TESTIMONIALS -->

      <!-- CLIENTS -->
      <section class="clients">
        <div class="container-fluid">
          <!-- Section Content -->
          <div class="r1 row">
            <div class="c1" id="clients-carousel-1">
              <div class="client-logo">
                <a href="#" target="_blank"><img class="center-block" src="images/clients/client-1.png" alt="Client 1"></a>
              </div>
              <div class="client-logo">
                <a href="#" target="_blank"><img class="center-block" src="images/clients/client-2.png" alt="Client 2"></a>
              </div>
              <div class="client-logo">
                <a href="#" target="_blank"><img class="center-block" src="images/clients/client-3.png" alt="Client 3"></a>
              </div>
              <div class="client-logo">
                <a href="#" target="_blank"><img class="center-block" src="images/clients/client-4.png" alt="Client 4"></a>
              </div>
              <div class="client-logo">
                <a href="#" target="_blank"><img class="center-block" src="images/clients/client-5.png" alt="Client 5"></a>
              </div>
              <div class="client-logo">
                <a href="#" target="_blank"><img class="center-block" src="images/clients/client-6.png" alt="Client 6"></a>
              </div>
              <div class="client-logo">
                <a href="#" target="_blank"><img class="center-block" src="images/clients/client-1.png" alt="Client 7"></a>
              </div>
              <div class="client-logo">
                <a href="#" target="_blank"><img class="center-block" src="images/clients/client-2.png" alt="Client 8"></a>
              </div>
              <div class="client-logo">
                <a href="#" target="_blank"><img class="center-block" src="images/clients/client-3.png" alt="Client 9"></a>
              </div>
              <div class="client-logo">
                <a href="#" target="_blank"><img class="center-block" src="images/clients/client-4.png" alt="Client 10"></a>
              </div>
              <div class="client-logo">
                <a href="#" target="_blank"><img class="center-block" src="images/clients/client-5.png" alt="Client 11"></a>
              </div>
              <div class="client-logo">
                <a href="#" target="_blank"><img class="center-block" src="images/clients/client-6.png" alt="Client 12"></a>
              </div>
            </div>
          </div>
          <!-- End Section Content -->
        </div>
      </section>
      <!-- END CLIENTS -->

      <!-- PRICING -->
      <section class="pricing col3" id="pricing">
        <div class="container">
          <!--- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Choose</h2>
            <h3 class="section-subtitle">Lorem ipsum per fames turpis tempor elementum felis venenatis, odio vivamus sagittis massa platea</h3>
          </div>
          <!--- End Section Header -->
          <!-- Section Content - Attached 3 Columns -->
          <div class="r1 row animateblock btt speed-1">
            <!--- Table 1 -->
            <div class="c1 col-sm-4 col-md-4">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="title">Trial/Manual Load</h3>
                </div>
                <div class="panel-body">
                  <!--- Price -->
                  <div class="plan-price">
                    <span class="price">$75<sup>.00</sup></span>
                    <span class="per-month">/month</span>
                  </div>
                  <!--- Features -->
                  <ul class="list-unstyled plan-features">
                    <li>Lorem ipsum ligula facilisis duis</li>
                    <li>Turpis viverra amet bibendum</li>
                    <li>Convallis eleifend ullamcorper</li>
                    <li>Ultricies at et mauris bibendum</li>
                    <li>Non felis ligula per proin nam</li>
                  </ul>
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-danger signup" href="#signup">Sign Up</a>
                </div>
              </div>
            </div>
            <!--- End Table 1 -->
            <!-- Table 2 - Popular -->
            <div class="c2 col-sm-4 col-md-4 popular">
              <div class="panel panel-default panel-popular">
                <div class="badge-popular"></div>
                <div class="panel-heading">
                  <h3 class="title">Automatic - Web</h3>
                </div>
                <div class="panel-body">
                  <!--- Price -->
                  <div class="plan-price">
                    <span class="price">$125<sup>.00</sup></span>
                    <span class="per-month">/month</span>
                  </div>
                  <!--- Features -->
                  <ul class="list-unstyled plan-features">
                    <li>Lorem ipsum ligula facilisis duis</li>
                    <li>Turpis viverra amet bibendum</li>
                    <li>Convallis eleifend ullamcorper</li>
                    <li>Ultricies at et mauris bibendum</li>
                    <li>Non felis ligula per proin nam</li>
                  </ul>
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-danger signup" href="#signup">Sign Up</a>
                </div>
              </div>
            </div>
            <!--- End Table 2 -->
            <!--- Table 3 -->
            <div class="c3 col-sm-4 col-md-4">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="title">Automatic - DMS</h3>
                </div>
                <div class="panel-body">
                  <!--- Price -->
                  <div class="plan-price">
                    <span class="price">$175<sup>.00</sup></span>
                    <span class="per-month">/month</span>
                  </div>
                  <!--- Features -->
                  <ul class="list-unstyled plan-features">
                    <li>Lorem ipsum ligula facilisis duis</li>
                    <li>Turpis viverra amet bibendum</li>
                    <li>Convallis eleifend ullamcorper</li>
                    <li>Ultricies at et mauris bibendum</li>
                    <li>Non felis ligula per proin nam</li>
                  </ul>
                </div>
                <div class="panel-footer">
                  <a class="btn btn-block btn-lg btn-danger signup" href="#signup">Sign Up</a>
                </div>
              </div>
            </div>
            <!--- End Table 3 -->
          </div>
          <!--- End Section Content -->
        </div>
      </section>
      <!-- END PRICING -->

     
      <!-- TEAM -->
      <section class="team" id="team">
        <div class="container">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Who</h2>
            <h3 class="section-subtitle">Lorem ipsum per fames turpis tempor elementum felis venenatis, odio vivamus sagittis massa platea</h3>
          </div>
          <!-- End Section Header -->
          <div class="r1 row animateblock btt speed-1">
            <div class="c1 col-md-12" id="team-carousel-1">
              <!-- Block 1 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_1.jpg" alt="Member 1">
                </div>
                <div class="team-content">
                  <h3 class="name">John Smith<br><small class="function">CEO</small></h3>
                  <p class="description">Lorem ipsum consectetur ultrices ac facilisis est neque fames, eget vel libero egestas viverra lacinia.</p>
                </div>
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="team-social linkedin" data-toggle="tooltip" data-placement="bottom" title="Linkedin" href="#" target="_blank"><span class="fa fa-linkedin"></span></a>
                  <a class="team-social facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook" href="#" target="_blank"><span class="fa fa-facebook"></span></a>
                  <a class="team-social google-plus" data-toggle="tooltip" data-placement="bottom" title="Google+" href="#" target="_blank"><span class="fa fa-google-plus"></span></a>
                  <a class="team-social twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter" href="#" target="_blank"><span class="fa fa-twitter"></span></a>
                  <a class="team-social github" data-toggle="tooltip" data-placement="bottom" title="Github" href="#" target="_blank"><span class="fa fa-github"></span></a>
                </div>
              </div>
              <!-- End Block 1 -->
              <!-- Block 2 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_2.jpg" alt="Member 2">
                </div>
                <div class="team-content">
                  <h3 class="name">Jaqueline Smith<br><small class="function">Web Designer</small></h3>
                  <p class="description">Lorem ipsum consectetur ultrices ac facilisis est neque fames, eget vel libero egestas viverra lacinia.</p>
                </div>
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="team-social linkedin" data-toggle="tooltip" data-placement="bottom" title="Linkedin" href="#" target="_blank"><span class="fa fa-linkedin"></span></a>
                  <a class="team-social facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook" href="#" target="_blank"><span class="fa fa-facebook"></span></a>
                  <a class="team-social google-plus" data-toggle="tooltip" data-placement="bottom" title="Google+" href="#" target="_blank"><span class="fa fa-google-plus"></span></a>
                  <a class="team-social twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter" href="#" target="_blank"><span class="fa fa-twitter"></span></a>
                  <a class="team-social github" data-toggle="tooltip" data-placement="bottom" title="Github" href="#" target="_blank"><span class="fa fa-github"></span></a>
                </div>
              </div>
              <!-- End Block 2 -->
              <!-- Block 3 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_3.jpg" alt="Member 3">
                </div>
                <div class="team-content">
                  <h3 class="name">Joseph Smith<br><small class="function">Art Director</small></h3>
                  <p class="description">Lorem ipsum consectetur ultrices ac facilisis est neque fames, eget vel libero egestas viverra lacinia.</p>
                </div>
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="team-social linkedin" data-toggle="tooltip" data-placement="bottom" title="Linkedin" href="#" target="_blank"><span class="fa fa-linkedin"></span></a>
                  <a class="team-social facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook" href="#" target="_blank"><span class="fa fa-facebook"></span></a>
                  <a class="team-social google-plus" data-toggle="tooltip" data-placement="bottom" title="Google+" href="#" target="_blank"><span class="fa fa-google-plus"></span></a>
                  <a class="team-social twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter" href="#" target="_blank"><span class="fa fa-twitter"></span></a>
                  <a class="team-social github" data-toggle="tooltip" data-placement="bottom" title="Github" href="#" target="_blank"><span class="fa fa-github"></span></a>
                </div>
              </div>
              <!-- End Block 3 -->
              <!-- Block 4 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_4.jpg" alt="Member 4">
                </div>
                <div class="team-content">
                  <h3 class="name">Jennifer Smith<br><small class="function">Developer</small></h3>
                  <p class="description">Lorem ipsum consectetur ultrices ac facilisis est neque fames, eget vel libero egestas viverra lacinia.</p>
                </div>
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="team-social linkedin" data-toggle="tooltip" data-placement="bottom" title="Linkedin" href="#" target="_blank"><span class="fa fa-linkedin"></span></a>
                  <a class="team-social facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook" href="#" target="_blank"><span class="fa fa-facebook"></span></a>
                  <a class="team-social google-plus" data-toggle="tooltip" data-placement="bottom" title="Google+" href="#" target="_blank"><span class="fa fa-google-plus"></span></a>
                  <a class="team-social twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter" href="#" target="_blank"><span class="fa fa-twitter"></span></a>
                  <a class="team-social github" data-toggle="tooltip" data-placement="bottom" title="Github" href="#" target="_blank"><span class="fa fa-github"></span></a>
                </div>
              </div>
              <!-- End Block 4 -->
              <!-- Block 5 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_1.jpg" alt="Member 1">
                </div>
                <div class="team-content">
                  <h3 class="name">John Smith<br><small class="function">CEO</small></h3>
                  <p class="description">Lorem ipsum consectetur ultrices ac facilisis est neque fames, eget vel libero egestas viverra lacinia.</p>
                </div>
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="team-social linkedin" data-toggle="tooltip" data-placement="bottom" title="Linkedin" href="#" target="_blank"><span class="fa fa-linkedin"></span></a>
                  <a class="team-social facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook" href="#" target="_blank"><span class="fa fa-facebook"></span></a>
                  <a class="team-social google-plus" data-toggle="tooltip" data-placement="bottom" title="Google+" href="#" target="_blank"><span class="fa fa-google-plus"></span></a>
                  <a class="team-social twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter" href="#" target="_blank"><span class="fa fa-twitter"></span></a>
                  <a class="team-social github" data-toggle="tooltip" data-placement="bottom" title="Github" href="#" target="_blank"><span class="fa fa-github"></span></a>
                </div>
              </div>
              <!-- End Block 5 -->
              <!-- Block 6 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_2.jpg" alt="Member 2">
                </div>
                <div class="team-content">
                  <h3 class="name">Jaqueline Smith<br><small class="function">Web Designer</small></h3>
                  <p class="description">Lorem ipsum consectetur ultrices ac facilisis est neque fames, eget vel libero egestas viverra lacinia.</p>
                </div>
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="team-social linkedin" data-toggle="tooltip" data-placement="bottom" title="Linkedin" href="#" target="_blank"><span class="fa fa-linkedin"></span></a>
                  <a class="team-social facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook" href="#" target="_blank"><span class="fa fa-facebook"></span></a>
                  <a class="team-social google-plus" data-toggle="tooltip" data-placement="bottom" title="Google+" href="#" target="_blank"><span class="fa fa-google-plus"></span></a>
                  <a class="team-social twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter" href="#" target="_blank"><span class="fa fa-twitter"></span></a>
                  <a class="team-social github" data-toggle="tooltip" data-placement="bottom" title="Github" href="#" target="_blank"><span class="fa fa-github"></span></a>
                </div>
              </div>
              <!-- End Block 6 -->
              <!-- Block 7 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_3.jpg" alt="Member 3">
                </div>
                <div class="team-content">
                  <h3 class="name">Joseph Smith<br><small class="function">Art Director</small></h3>
                  <p class="description">Lorem ipsum consectetur ultrices ac facilisis est neque fames, eget vel libero egestas viverra lacinia.</p>
                </div>
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="team-social linkedin" data-toggle="tooltip" data-placement="bottom" title="Linkedin" href="#" target="_blank"><span class="fa fa-linkedin"></span></a>
                  <a class="team-social facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook" href="#" target="_blank"><span class="fa fa-facebook"></span></a>
                  <a class="team-social google-plus" data-toggle="tooltip" data-placement="bottom" title="Google+" href="#" target="_blank"><span class="fa fa-google-plus"></span></a>
                  <a class="team-social twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter" href="#" target="_blank"><span class="fa fa-twitter"></span></a>
                  <a class="team-social github" data-toggle="tooltip" data-placement="bottom" title="Github" href="#" target="_blank"><span class="fa fa-github"></span></a>
                </div>
              </div>
              <!-- End Block 7 -->
              <!-- Block 8 -->
              <div class="team-wrapper">
                <!-- Photo -->
                <div class="team-photo">
                  <img src="images/team/team_4.jpg" alt="Member 4">
                </div>
                <div class="team-content">
                  <h3 class="name">Jennifer Smith<br><small class="function">Developer</small></h3>
                  <p class="description">Lorem ipsum consectetur ultrices ac facilisis est neque fames, eget vel libero egestas viverra lacinia.</p>
                </div>
                <!-- Social buttons -->
                <div class="team-media">
                  <a class="team-social linkedin" data-toggle="tooltip" data-placement="bottom" title="Linkedin" href="#" target="_blank"><span class="fa fa-linkedin"></span></a>
                  <a class="team-social facebook" data-toggle="tooltip" data-placement="bottom" title="Facebook" href="#" target="_blank"><span class="fa fa-facebook"></span></a>
                  <a class="team-social google-plus" data-toggle="tooltip" data-placement="bottom" title="Google+" href="#" target="_blank"><span class="fa fa-google-plus"></span></a>
                  <a class="team-social twitter" data-toggle="tooltip" data-placement="bottom" title="Twitter" href="#" target="_blank"><span class="fa fa-twitter"></span></a>
                  <a class="team-social github" data-toggle="tooltip" data-placement="bottom" title="Github" href="#" target="_blank"><span class="fa fa-github"></span></a>
                </div>
              </div>
              <!-- End Block 8 -->
            </div>
          </div>
        </div>
      </section>
      <!-- END TEAM -->

      <!-- SIGNIP -->
      <section class="contact" id="contact">
        <div class="container-fluid">
          <!-- Section Header -->
          <div class="section-header">
            <h2 class="section-title">Communicate</h2>
            <h3 class="section-subtitle">Lorem ipsum per fames turpis tempor elementum felis venenatis, odio vivamus sagittis massa platea</h3>
          </div>
          <!-- End Section Header -->
          <!-- Section Content -->
          <div class="r1 row">
            <div class="c1 col-md-12">
              <!-- Map accordion -->
              <div class="panel-group" id="contact-accordion-1">
                <div class="panel panel-default">
                  <h3 class="map-title">
                      <a data-toggle="collapse" data-parent="#contact-accordion-1" href="#contact-collapse-1" aria-expanded="true" aria-controls="contact-collapse-1"><span class="glyphicon glyphicon-map-marker"></span><br>Where</a>
                    </h3>
                  <div class="panel-collapse collapse in" id="contact-collapse-1">
                    <div class="panel-body">
                      <!-- Map -->
                      <div class="map-wrapper">
                        <!-- Here is where the map will be rendered -->
                        <div id="map-canvas"></div>
                        <!-- Address over the map -->
                        <address class="alert vcard">
                          <button class="close" data-dismiss="alert" type="button"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                          </button>
                          <div class="org">
                            <strong>Your Company</strong>
                          </div>
                          <div class="adr">
                            <span class="street-address">76 Ninth Avenue - 7th floor</span>
                            <span class="locality">New York</span>,
                            <span class="region">NY 10011</span> -
                            <span class="country-name">USA</span>
                          </div>
                          <div class="tel">
                            <span class="tel-1">+1 212-565-0000</span>
                            <span class="tel-2">+1 212-565-0001</span>
                          </div>
                          <div class="email">
                            <!-- Anti spam protection. Keep the codes: "&#x3a;" (= ":") / "&#x40;" (= "@") / "&#x2e;" (= ".") -->
                            <span class="email-contact"><a href="mailto&#x3a;contact&#x40;yourcompany&#x2e;com" data-user="contact" data-domain="yourcompany.com"></a></span>
                            <span class="email-support"><a href="mailto&#x3a;support&#x40;yourcompany&#x2e;com" data-user="support" data-domain="yourcompany.com"></a></span>
                          </div>
                        </address>
                        <!-- End Address over the map -->
                      </div>
                      <!-- End Map -->
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Map accordion -->
            </div>
          </div>
        </div>
        <div class="container" id="signup">
          <div class="r1 row">
            <div class="c1 col-md-10 col-md-offset-1">
              <div class="alerts hidden-xs alerts-contact"></div>
              <!-- signup form -->
              <!-- Section Header -->
              <div class="section-header">
                <h2 class="section-title">Sign Up Now</h2>
                <h3 class="section-subtitle">Lorem ipsum per fames turpis tempor elementum felis venenatis, odio vivamus sagittis massa platea</h3>
              </div>
              <!-- End Section Header -->
              <div class="form-horizontal contact-form" role="form">
                <fieldset>
                  <legend>Contact</legend>
                  <div class="r1 row form-group">
                    <div class="c1 col-sm-6 col-md-6">
                      <input class="form-control" id="fname" name="fname" type="text" placeholder="First Name *" required>
                    </div>
                    <div class="c2 col-sm-6 col-md-6">
                      <input class="form-control" id="lname" name="lname" type="text" placeholder="Last Name *" required>
                    </div>
                  </div>
                  <div class="r2 row form-group">
                    <div class="c1 col-sm-6 col-md-6">
                      <input class="form-control" id="email" name="email" type="email" placeholder="Email *" required>
                    </div>
                    <div class="c2 col-sm-6 col-md-6">
                      <input class="form-control" id="phone" name="phone" type="tel" placeholder="Phone">
                    </div>
                  </div>
                  <div class="r3 row form-group">
                    <div class="c1 col-sm-6 col-md-6">
                      <input class="form-control" id="company" name="company" type="text" placeholder="Company">
                    </div>
                    <div class="c2 col-sm-6 col-md-6">
                      <input class="form-control" id="website" name="website" type="url" placeholder="Website">
                    </div>
                  </div>
                  <div class="r4 row form-group">
                    <div class="c1 col-md-12">
                      <input class="form-control" id="subject" name="subject" type="text" placeholder="Subject *" required>
                    </div>
                  </div>
                  <div class="r5 row form-group">
                    <div class="c1 col-md-12">
                      <textarea class="form-control" id="message" name="message" rows="10" placeholder="Message *" required></textarea>
                    </div>
                  </div>
                  <div class="r6 row form-group">
                    <div class="c1 col-md-12">
                      <button class="btn btn-lg btn-danger submit-form-contact" type="submit">Send Message</button>
                    </div>
                  </div>
                </fieldset>
              </div>
              <!-- End Contact form -->
              <div class="alerts alerts-xs hidden-sm hidden-md hidden-lg alerts-contact"></div>
            </div>
          </div>
        </div>
        <!-- End Section Content -->
      </section>
      <!-- END CONTACT -->

    </div>
    <!-- END MAIN -->


    <!-- FOOTER -->
    <footer class="footer">
      <div class="container">
        <div class="r1 row">
          <div class="c1 col-md-12">
            <div class="footer-logo"><img class="logo" src="images/websiteicon.png" width="105px" height="105px" alt="Logo"></div>
            <!-- Social Buttons -->
            <section class="social-icons">
              <h3 class="hide">Social Buttons</h3>
              <a class="btn-social facebook" data-toggle="tooltip" data-placement="top" title="Facebook" href="#" target="_blank"><span class="fa fa-facebook"></span></a>
              <a class="btn-social twitter" data-toggle="tooltip" data-placement="top" title="Twitter" href="#" target="_blank"><span class="fa fa-twitter"></span></a>
              <a class="btn-social google-plus" data-toggle="tooltip" data-placement="top" title="Google+" href="#" target="_blank"><span class="fa fa-google-plus"></span></a>
              <a class="btn-social youtube" data-toggle="tooltip" data-placement="top" title="Youtube" href="#" target="_blank"><span class="fa fa-youtube"></span></a>
              <a class="btn-social instagram" data-toggle="tooltip" data-placement="top" title="Instagram" href="#" target="_blank"><span class="fa fa-instagram"></span></a>
              <a class="btn-social email" data-toggle="tooltip" data-placement="top" title="Email" href="#" target="_blank"><span class="fa fa-envelope"></span></a>
              <a class="btn-social rss" data-toggle="tooltip" data-placement="top" title="RSS" href="#" target="_blank"><span class="fa fa-rss"></span></a>
            </section>
            <!-- End Social Buttons -->
          </div>
        </div>
      </div>
      <div class="copyright">
        <div class="container">
          <div class="r1 row">
            <div class="c1 col-md-12">         
            &copy; 2014 - 2017 DealerAddendums Inc ** Patent Pending ** <?php echo RELEASE_VERSION; ?>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- END FOOTER -->

    <a class="go-top" href="#top"><span class="fa fa-chevron-up"></span></a>

  </div>
  <!-- END LAYOUT -->

  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script>
    window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')
  </script>

  <!-- Bootstrap -->
  <script src="js/bootstrap.min.js"></script>

  <!-- Stackable Menu -->
  <script src="js/vendor/stackable.js"></script>

  <!-- Retina Graphics -->
  <script src="js/vendor/retina.min.js"></script>

  <!-- Nicescroll Scrollbars -->
  <script src="js/vendor/jquery.nicescroll.min.js"></script>

  <!-- Circliful Circle Statistics -->
  <script src="js/vendor/circles.min.js"></script>

  <!-- Slick Carousel -->
  <script src="js/vendor/slick.min.js"></script>

  <!-- BlueImp Gallery Lightbox -->
  <script src="js/vendor/jquery.blueimp-gallery.min.js"></script>

  <!-- Mailchimp Ajax Subscription -->
  <script src="js/vendor/jquery.ajaxchimp.min.js"></script>

  <!-- Google Map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDR5LCzbUhJGUiv9LodJmcxroAeAmawtow"></script>
  <script src="js/google_map.min.js"></script>

  <!-- Placeholder Polyfill IE9 -->
  <script src="js/vendor/simple.min.js"></script>

  <!-- Default General Settings -->
  <script src="js/main.min.js"></script>

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"beacon":"bam.nr-data.net","licenseKey":"a62da00697","applicationID":"17541974","transactionName":"M11QZEJSCBAEAkBZWAoXZ0JZHBEUElAbXVgXDx1kVV4WDwQVUR9eClxXSB5DDhM=","queueTime":0,"applicationTime":0,"atts":"HxpTEgpIGx4=","errorBeacon":"bam.nr-data.net","agent":""}</script></body>

</html>
